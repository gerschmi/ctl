#ifndef CTL_ABSTRACTDETECTOR_H
#define CTL_ABSTRACTDETECTOR_H

#include "systemcomponent.h"
#include "mat/matrix_utils.h"   // for definition of mat::Location
#include "img/singleviewdata.h" // for definition of SingleViewData::Dimensions
#include "models/abstractdatamodel.h"
#include "models/xydataseries.h"

#include <QJsonArray>
#include <QSize>
#include <QVariantMap>

/*
 * NOTE: This is header only.
 */

namespace CTL {

class GenericDetector;

/*!
 * \class AbstractDetector
 *
 * \brief Base class for detector components.
 *
 * This is the base class for all detector components.
 * Detectors are always considered to be composed of multiple flat panel elements, called modules.
 * The arrangement of all modules is described by a vector of ModuleLocation objects, one for each
 * of the modules. The ModuleLocation object must contain the position of the module in world
 * coordinates as well as a rotation matrix that represents the transformation from the module's
 * coordinate system to the CT-system (i.e. the coordinate system of the detector as a whole). In
 * addition to the arrangement, detector modules are characterized by their number of pixels
 * (`channels` x `rows`) and the corresponding dimensions of an individual pixel (`width` x
 * `height`).
 *
 * Custom detector types can be implemented by creating a sub-class of AbstractDetector.
 * Such sub-classes need to implement the method moduleLocations() that should extract the location
 * of all flat panel modules in the detector based on the specific parametrization chosen for the
 * sub-class.
 *
 * When creating a sub-class of AbstractDetector, make sure to register the new component in the
 * enumeration using the #CTL_TYPE_ID(newIndex) macro. It is required to specify a value
 * for \a newIndex that is not already in use. This can be easily achieved by use of values starting
 * from GenericComponent::UserType, as these are reserved for user-defined types.
 *
 * To enable de-/serialization of objects of the new sub-class, reimplement the toVariant() and
 * fromVariant() methods. These should take care of all newly introduced information of the
 * sub-class. Additionally, call the macro #DECLARE_SERIALIZABLE_TYPE(YourNewClassName) within the
 * .cpp file of your new class (substitute "YourNewClassName" with the actual class name). Objects
 * of the new class can then be de-/serialized with any of the serializer classes (see also
 * AbstractSerializer).
 */
class AbstractDetector : public SystemComponent
{
    CTL_TYPE_ID(100)
    DECLARE_ELEMENTAL_TYPE

public:
    typedef mat::Location ModuleLocation;
    enum SaturationModelType { Extinction, PhotonCount, Intensity, Undefined };

    // abstract interface
    public:virtual QVector<ModuleLocation> moduleLocations() const = 0;

public:
    AbstractDetector(const QSize& nbPixelPerModule,
                     const QSizeF& pixelSize,
                     const QString& name);

    // virtual methods
    QString info() const override;
    void fromVariant(const QVariant& variant) override; // de-serialization
    QVariant toVariant() const override; // serialization

    // setter methods
    void setSaturationModel(AbstractDataModel* model, SaturationModelType type);
    void setSaturationModel(std::unique_ptr<AbstractDataModel> model, SaturationModelType type);
    void setSpectralResponseModel(AbstractDataModel* model);
    void setSpectralResponseModel(std::unique_ptr<AbstractDataModel> model);

    // getter methods
    uint nbDetectorModules() const;
    const QSize& nbPixelPerModule() const;
    Q_DECL_DEPRECATED_X("Within the CTL 'dimension' is use for a count of something, use pixelSize() instead.")
    const QSizeF& pixelDimensions() const;
    const QSizeF& pixelSize() const;
    ModuleLocation moduleLocation(uint module) const;
    const AbstractDataModel* saturationModel() const;
    const AbstractDataModel* spectralResponseModel() const;
    XYDataSeries inputResponse(float from, float to, uint nbSamples) const;
    XYDataSeries spectralResponse(float from, float to, uint nbSamples) const;
    SaturationModelType saturationModelType() const;
    double skewAngle() const;

    // other methods
    bool hasSaturationModel() const;
    bool hasSpectralResponseModel() const;
    QSizeF moduleDimensions() const;
    SingleViewData::Dimensions viewDimensions() const;

    // converter
    std::unique_ptr<GenericDetector> toGeneric() const;

    ~AbstractDetector() override = default;

protected:
    AbstractDetector() = default;
    AbstractDetector(const QString& name);

    AbstractDetector(const AbstractDetector&) = default;
    AbstractDetector(AbstractDetector&&) = default;
    AbstractDetector& operator=(const AbstractDetector&) = default;
    AbstractDetector& operator=(AbstractDetector&&) = default;

    QSize _nbPixelPerModule; //!< Number of pixels in each detector module.
    QSizeF _pixelSize;       //!< Size of individual pixels (in mm).
    double _skewAngle = 0.0; //!< obliqueness angle (specifies non-orthogonality of pixels)

    AbstractDataModelPtr _spectralResponseModel; //!< Data model for spectral detector response.
    AbstractDataModelPtr _saturationModel; //!< Data model for saturation of measured values.
    SaturationModelType _saturationModelType = Undefined; //!< States whether saturation model refers to intensity or extinction values.
};

/*!
 * Returns the number of detector modules.
 *
 * Same as moduleLocations().size().
 */
inline uint AbstractDetector::nbDetectorModules() const { return moduleLocations().count(); }

/*!
 * Returns the number of pixels in an individual module as QSize. Dimensions are specified as
 * detector `channels` x `rows`.
 */
inline const QSize& AbstractDetector::nbPixelPerModule() const { return _nbPixelPerModule; }

/*!
 * This function has been deprecated. Use pixelSize() instead.
 */
inline const QSizeF& AbstractDetector::pixelDimensions() const { return _pixelSize; }

/*!
 * Returns the size of an individual pixel as QSizeF. The pixel size specified as `width` mm x
 * `height` mm (or *x*-spacing and *y*-spacing, respectively).
 *
 * Pixel sizes always refer to the pixel's rectangular (i.e. unskewed) shape.
 */
inline const QSizeF& AbstractDetector::pixelSize() const { return _pixelSize; }

/*!
 * Returns the (physical) dimensions of an individual detector module as QSizeF. Dimensions are
 * specified as `width` x `height`.
 */
inline QSizeF AbstractDetector::moduleDimensions() const
{
    return { _nbPixelPerModule.width()*_pixelSize.width(),
             _nbPixelPerModule.height()*_pixelSize.height() };
}

/*!
 * Returns the dimensions of a single view that would be acquired by this instance. This contains
 * number of channels (per module), number of rows (per module) and number of modules.
 */
inline SingleViewData::Dimensions AbstractDetector::viewDimensions() const
{
    SingleViewData::Dimensions ret;
    ret.nbChannels = _nbPixelPerModule.width();
    ret.nbRows     = _nbPixelPerModule.height();
    ret.nbModules  = nbDetectorModules();
    return ret;
}

/*!
 * Returns the location of module \a module. Same as moduleLocations().at(module).
 *
 * Using this method is typically very inefficient, as it always requires computation of all module
 * locations. In case you need multiple calls to this method, consider storing a local copy of the
 * entire set of locations (using moduleLocations()) and querying individual module locations from
 * that local copy using QVector::at().
 *
 * \sa moduleLocations()
 */
inline AbstractDetector::ModuleLocation AbstractDetector::moduleLocation(uint module) const
{
    Q_ASSERT(module < nbDetectorModules());
    return moduleLocations().at(module);
}

/*!
 * Returns a pointer to the saturation model of this instance.
 */
inline const AbstractDataModel* AbstractDetector::saturationModel() const
{
    return _saturationModel.get();
}

/*!
 * Returns the type of the saturation model, i.e. whether it refers to extinction or intensity
 * values.
 */
inline AbstractDetector::SaturationModelType AbstractDetector::saturationModelType() const
{
    return _saturationModelType;
}

/*!
 * Returns a pointer to the spectral response model of this instance.
 */
inline const AbstractDataModel* AbstractDetector::spectralResponseModel() const
{
    return _spectralResponseModel.get();
}

inline double AbstractDetector::skewAngle() const { return _skewAngle; }

/*!
 * Returns a sampled series of the saturation model of this instance.
 *
 * Returns an empty XYDataSeries if this instance does not have a saturation model set.
 */
inline XYDataSeries AbstractDetector::inputResponse(float from, float to, uint nbSamples) const
{
    if(!_saturationModel)
        return XYDataSeries();
    return XYDataSeries::sampledFromModel(*_saturationModel, from, to, nbSamples);
}

/*!
 * Returns a sampled series of the spectral response model of this instance.
 *
 * Returns an empty XYDataSeries if this instance does not have a spectral response model set.
 */
inline XYDataSeries AbstractDetector::spectralResponse(float from, float to, uint nbSamples) const
{
    if(!_spectralResponseModel)
        return XYDataSeries();
    return XYDataSeries::sampledFromModel(*_spectralResponseModel, from, to, nbSamples);
}

/*!
 * Returns true if this instance has a saturation model.
 */
inline bool AbstractDetector::hasSaturationModel() const
{
    return static_cast<bool>(_saturationModel);
}

/*!
 * Returns true if this instance has a saturation model.
 */
inline bool AbstractDetector::hasSpectralResponseModel() const
{
    return static_cast<bool>(_spectralResponseModel);
}

/*!
 * Constructs an empty object named \a name.
 */
inline AbstractDetector::AbstractDetector(const QString &name)
    : SystemComponent(name)
{
}

/*!
 * Constructs an AbstractDetector object with name \a name which contains modules that have
 * \a nbPixelPerModule pixels (`channels` x `rows`) with physical dimensions of \a pixelSize
 * (`width` mm x `height` mm).
 */
inline AbstractDetector::AbstractDetector(const QSize& nbPixelPerModule,
                                          const QSizeF& pixelSize,
                                          const QString& name)
    : SystemComponent(name)
    , _nbPixelPerModule(nbPixelPerModule)
    , _pixelSize(pixelSize)
{
}

/*!
 * Returns a formatted string with information about the object.
 *
 * In addition to the information from the base class, the info string contains the following
 * details: \li Nb. of detector modules \li Nb. of pixels per module \li Pixel size.
 */
inline QString AbstractDetector::info() const
{
    QString ret(SystemComponent::info());

    // clang-format off
    ret +=
       typeInfoString(typeid(this)) +
       "\tNb. of modules: "            + QString::number(nbDetectorModules()) + "\n"
       "\tNb. of pixels per module: "  + QString::number(_nbPixelPerModule.width()) + " x " +
                                         QString::number(_nbPixelPerModule.height()) + "\n"
       "\tPixel size: "                + QString::number(_pixelSize.width()) + " mm x " +
                                         QString::number(_pixelSize.height()) + " mm\n";

    ret += (this->type() == AbstractDetector::Type) ? QLatin1String("}\n") : QLatin1String("");
    // clang-format on

    return ret;
}

// Use SerializationInterface::fromVariant() documentation.
inline void AbstractDetector::fromVariant(const QVariant& variant)
{
    SystemComponent::fromVariant(variant);

    QVariantMap varMap = variant.toMap();

    auto nbPixels = varMap.value(QStringLiteral("pixel per module")).toMap();
    _nbPixelPerModule.setWidth(nbPixels.value(QStringLiteral("channels")).toInt());
    _nbPixelPerModule.setHeight(nbPixels.value(QStringLiteral("rows")).toInt());

    auto pixelDim = varMap.value(QStringLiteral("pixel size")).toMap();
    _pixelSize.setWidth(pixelDim.value(QStringLiteral("width")).toDouble());
    _pixelSize.setHeight(pixelDim.value(QStringLiteral("height")).toDouble());

    QVariant saturationModel = varMap.value(QStringLiteral("saturation model"));
    _saturationModel.reset(SerializationHelper::parseDataModel(saturationModel));

    int satModTypeVal = varMap.value(QStringLiteral("saturation model type")).toMap().value(QStringLiteral("enum value")).toInt();
    _saturationModelType = SaturationModelType(satModTypeVal);
}

// Use SerializationInterface::toVariant() documentation.
inline QVariant AbstractDetector::toVariant() const
{
    QVariantMap ret = SystemComponent::toVariant().toMap();

    QVariantMap nbPixels;
    nbPixels.insert(QStringLiteral("channels"),_nbPixelPerModule.width());
    nbPixels.insert(QStringLiteral("rows"), _nbPixelPerModule.height());

    QVariantMap pixelDim;
    pixelDim.insert(QStringLiteral("width"),_pixelSize.width());
    pixelDim.insert(QStringLiteral("height"), _pixelSize.height());

    QVariant saturationModel = hasSaturationModel() ? _saturationModel->toVariant()
                                                    : QVariant();

    QVariantMap satModelType;
    satModelType.insert(QStringLiteral("enum value"), _saturationModelType);
    satModelType.insert(QStringLiteral("meaning"), "0: Extinction, 1: Photon count, 2: Intensity, 3: Undefined");

    ret.insert(QStringLiteral("pixel per module"), nbPixels);
    ret.insert(QStringLiteral("pixel size"), pixelDim);
    ret.insert(QStringLiteral("saturation model"), saturationModel);
    ret.insert(QStringLiteral("saturation model type"), satModelType);

    return ret;
}

/*!
 * Sets the saturation model to \a model. The argument \a type must specify whether the passed model
 * refers to extinction values or intensities.
 */
inline void AbstractDetector::setSaturationModel(AbstractDataModel* model,
                                                 AbstractDetector::SaturationModelType type)
{
    _saturationModel.reset(model);
    _saturationModelType = type;
}

/*!
 * Sets the saturation model to \a model. The argument \a type must specify whether the passed model
 * refers to extinction values or intensities.
 */
inline void AbstractDetector::setSaturationModel(std::unique_ptr<AbstractDataModel> model,
                                                 SaturationModelType type)
{
    _saturationModel = std::move(model);
    _saturationModelType = type;
}

/*!
 * Sets the spectral response model to \a model. The model shall contain multiplicative factors
 * describing the sensitivity of the detector to specific photon energies.
 */
inline void AbstractDetector::setSpectralResponseModel(AbstractDataModel* model)
{
    _spectralResponseModel.reset(model);
}

/*!
 * Sets the spectral response model to \a model. The model shall contain multiplicative factors
 * describing the sensitivity of the detector to specific photon energies.
 */
inline void AbstractDetector::setSpectralResponseModel(std::unique_ptr<AbstractDataModel> model)
{
    _spectralResponseModel = std::move(model);
}

/*!
 * \fn virtual int AbstractDetector::genericType() const
 *
 * Returns the type id of the AbstractDetector class.
 *
 * This method can be used to determine whether the base type of an object is AbstractDetector.
 */

/*!
 * \fn virtual QVector<ModuleLocation> AbstractDetector::moduleLocations() const = 0
 *
 * Returns the location (i.e. position and rotation) of all individual detector modules with respect
 * to the (physical) center of the detector. These locations are considered in addition to the
 * global positioning of the detector (managed by AbstractGantry).
 *
 * Implement this method in derived classes to compute the locations of individual modules based on
 * the specific parametrization of that particular sub-class.
 */

/*!
 * \typedef AbstractDetector::ModuleLocation
 *
 * Synonym for mat::Location.
 */

/*!
 * \enum AbstractDetector::SaturationModelType
 *
 * Specification of whether the saturation model of this instance relates to intensity or extinction
 * values.
 */

} // namespace CTL

/*! \file */
///@{
/*!
 * \fn std::unique_ptr<AbstractDetector> CTL::makeDetector(ConstructorArguments&&... arguments)
 * \relates AbstractDetector
 *
 * Factory method to construct an object of any sub-class of AbstractDetector and wrap the object in
 * an std::unique_ptr<AbstractDetector>.
 *
 * This is similar to the more general method GenericComponent::makeComponent() with the difference
 * that it returns a unique pointer to the AbstractDetector base type instead of GenericComponent.
 *
 * \sa GenericComponent::makeComponent().
 */
///@}

#endif // CTL_ABSTRACTDETECTOR_H
