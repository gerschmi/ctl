#include "abstractgantry.h"
#include "genericgantry.h"

namespace CTL {

std::unique_ptr<GenericGantry> AbstractGantry::toGeneric() const
{
    return GenericGantry::fromOther(*this);
}

} // namespace CTL
