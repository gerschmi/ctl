#include "datamodels1d.h"

#include "mat/pi.h"

#include <QDebug>
#include <cmath>
#include <stdexcept>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(SaturatedLinearModel)
DECLARE_SERIALIZABLE_TYPE(SaturatedSplineModel)
DECLARE_SERIALIZABLE_TYPE(StepFunctionModel)
DECLARE_SERIALIZABLE_TYPE(ConstantModel)
DECLARE_SERIALIZABLE_TYPE(RectFunctionModel)
DECLARE_SERIALIZABLE_TYPE(IdentityModel)
DECLARE_SERIALIZABLE_TYPE(GaussianModel1D)

// ### SaturatedLinearModel ###

/*!
 * Constructs a SaturatedLinearModel with (lower/upper) saturation levels at \a lowerCap
 * and \a upperCap.
 */
SaturatedLinearModel::SaturatedLinearModel(float lowerCap, float upperCap)
    : _a(lowerCap)
    , _b(upperCap)
{
    setName(QStringLiteral("SaturatedLinearModel"));
}

/*!
 * \brief Returns the value from the model at \a position.
 *
 * The resulting value is described by the follwoing relation:
 * \f$
 *  f(x)=\begin{cases}
 *  a & x<a\\
 *  x & a\leq x\leq b\\
 *  b & x>b
 *  \end{cases}
 *  \f$
 */
float SaturatedLinearModel::valueAt(float position) const
{
    if(position < _a)
        return _a;
    else if(position > _b)
        return _b;
    else
        return position;
}

// base class documentation
float SaturatedLinearModel::binIntegral(float position, float binWidth) const
{
    static const auto linearPart = [] (float from, float to)
    {
        if(to<from) return 0.0f;

        return 0.5f * (std::pow(to, 2.0f) - std::pow(from, 2.0f));
    };
    static const auto constPart = [] (float from, float to, float ampl)
    {
        return ampl * (to - from);
    };

    const auto from = position - 0.5f * binWidth;
    const auto to   = position + 0.5f * binWidth;

    if(to < from) return 0.0f;                    // negative bin width
    if(to < _a) return constPart(from, to, _a);   // purely in lower saturation area
    if(from > _b) return constPart(from, to, _b); // purely in upper saturation area

    // mixed integral
    const auto from_linPart = std::max(from, _a);
    const auto to_linPart   = std::min(to, _b);

    return constPart(from, from_linPart, _a)
            + linearPart(from_linPart, to_linPart)
            + constPart(to_linPart, to, _b);
}

// Re-use documentation of base class
AbstractIntegrableDataModel* SaturatedLinearModel::clone() const
{
    return new SaturatedLinearModel(*this);
}

/*!
 * Returns the parameters of this instance as QVariant.
 *
 * This returns a QVariantMap with two key-value-pairs: ("a",\a a), ("b",\a b), where \a a and \a b
 * denote the lower and upper saturation level, respectively.
 */
QVariant SaturatedLinearModel::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("a"), _a);
    ret.insert(QStringLiteral("b"), _b);

    return ret;
}

/*!
 * Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Parameters can be passed by either of the following two options:
 *
 * 1. As a QVariantMap with two key-value-pairs: ("a", \a a), ("b", \a b). In this case, \a a and
 * \a b denote the lower and upper saturation level.
 * 2. As a QVariantList: In this case, the list must contain two floating point values sorted in
 * the following order: \a a, \a b.
 *
 * Example:
 * \code
 * SaturatedLinearModel linearModel;
 * // option 1
 * QVariantMap parameterMap;
 * parameterMap.insert("a", 1.0f);
 * parameterMap.insert("b", 5.0f);
 * linearModel.setParameter(parameterMap);
 *
 * // option 1 - alternative way
 * linearModel.setParameter(QVariantMap{ {"a", 1.0f},
 *                                       {"b", 5.0f} });
 *
 * // option 2
 * linearModel.setParameter(QVariantList{1.0f, 5.0f});
 * \endcode
 */
void SaturatedLinearModel::setParameter(const QVariant& parameter)
{
    if(parameter.canConvert(QMetaType::QVariantMap))
        setParFromMap(parameter.toMap());
    else if(parameter.canConvert(QMetaType::QVariantList))
        setParFromList(parameter.toList());
    else
        qWarning() << "SaturatedLinearModel::setParameter: Could not set parameters! "
                      "reason: incompatible variant passed";
}

/*!
 * Helper method for setParameter().
 */
void SaturatedLinearModel::setParFromList(const QVariantList& list)
{
    if(list.size()<2)
    {
        qWarning() << "SaturatedLinearModel::setParameter: Could not set parameters! "
                      "reason: contained QVariantList has too few entries (required: 2 float)";
        return;
    }
    _a = list.at(0).toFloat();
    _b = list.at(1).toFloat();
}

/*!
 * Helper method for setParameter().
 */
void SaturatedLinearModel::setParFromMap(const QVariantMap& map)
{
    _a = map.value(QStringLiteral("a"), 0.0f).toFloat();
    _b = map.value(QStringLiteral("b"), FLT_MAX).toFloat();
}

// ### SaturatedSplineModel ###

/*!
 * Constructs a SaturatedSplineModel with (lower/upper) saturation levels at \a lowerCap
 * and \a upperCap. Softening margins around the saturation levels are set to \a softLower and
 * \a softUpper, respectively.
 */
SaturatedSplineModel::SaturatedSplineModel(float lowerCap, float upperCap,
                                           float softLower, float softUpper)
    : _a(lowerCap)
    , _b(upperCap)
    , _softA(softLower)
    , _softB(softUpper)
{
    setName(QStringLiteral("SaturatedSplineModel"));
}

/*!
 * Constructs a SaturatedSplineModel with (lower/upper) saturation levels at \a lowerCap
 * and \a upperCap and equally sized softening margins \f$ s_{a}=s_{b}=softening \f$.
 */
SaturatedSplineModel::SaturatedSplineModel(float lowerCap, float upperCap, float softening)
    : SaturatedSplineModel(lowerCap, upperCap, softening, softening)
{
}

// Re-use documentation of base class
AbstractDataModel* SaturatedSplineModel::clone() const
{
    return new SaturatedSplineModel(*this);
}

/*!
 * Returns the value from the model at \a position.
 *
 * \f$
 *  f(x)=\begin{cases}
 *  a & x<a-s_{a}\\
 *  S_{1}(x) & a-s_{a}\leq x<a+s_{a}\\
 *  x & a+s_{a}\leq x<b-s_{b}\\
 *  S_{2}(x) & b-s_{b}\leq x<b+s_{b}\\
 *  b & x\geq b
 *  \end{cases}
 * \f$
 *
 * With:
 *  \f$
 *  \begin{align*}
 *  S_{1}(x) & =\frac{1}{4s_{a}}x^{2}-\frac{a-s_{a}}{2s_{a}}x+\frac{(a+s_{a})^{2}}{4s_{a}}\\
 *  S_{2}(x) & =-\frac{1}{4s_{b}}x^{2}+\frac{b+s_{b}}{2s_{b}}x+\frac{(b-s_{b})^{2}}{4s_{b}}
 *  \end{align*}
 * \f$
 */
float SaturatedSplineModel::valueAt(float position) const
{
    float spl1Start = _a - _softA;
    float spl1End   = _a + _softA;
    float spl2Start = _b - _softB;
    float spl2End   = _b + _softB;

    if(position < spl1Start)        // lower saturation
        return _a;
    else if(position < spl1End)     // lower spline regime
        return spline1(position);
    else if(position < spl2Start)   // linear regime
        return position;
    else if(position < spl2End)     // upper spline regime
        return spline2(position);
    else                            // upper saturation
        return _b;
}

/*!
 * Returns the parameters of this instance as QVariant.
 *
 * This returns a QVariantMap with four key-value-pairs: ("a", \a a), ("b", \a b),
 * ("softA", \f$ s_a \f$), ("softB", \f$ s_b \f$). Here, \a a and \a b denote the lower and upper
 * saturation level, respectively, and \f$ s_a \f$ and \f$ s_b \f$ are the softening margins of
 * the transitions to/from the linear regime.
 */
QVariant SaturatedSplineModel::parameter() const
{
    QVariantMap map;
    map.insert(QStringLiteral("a"), _a);
    map.insert(QStringLiteral("b"), _b);
    map.insert(QStringLiteral("softA"), _softA);
    map.insert(QStringLiteral("softB"), _softB);

    return map;
}

/*!
 * Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Parameters can be passed by either of the following two options:
 *
 * 1. As a QVariantMap with four key-value-pairs: ("a", \a a), ("b", \a b), ("softA", \f$ s_a \f$),
 * ("softB", \f$ s_b \f$). In this case, \a a and \a b denote the lower and upper saturation level
 * and \f$ s_a \f$ and \f$ s_b \f$ specify the softening margins of the transitions to/from the
 * linear regime.
 * 2. As a QVariantList: In this case, the list must contain four floating point values sorted in
 * the following order: \a a, \a b, \f$ s_a \f$, \f$ s_b \f$.
 *
 * Example:
 * \code
 * SaturatedSplineModel splineModel;
 * // option 1
 * QVariantMap parameterMap;
 * parameterMap.insert("a", 1.0f);
 * parameterMap.insert("b", 5.0f);
 * parameterMap.insert("softA", 0.2f);
 * parameterMap.insert("softB", 0.5f);
 * splineModel.setParameter(parameterMap);
 *
 * // option 1 - alternative way
 * splineModel.setParameter(QVariantMap{ {"a", 1.0f},
 *                                       {"b", 5.0f},
 *                                       {"softA", 0.2f},
 *                                       {"softB", 0.5f} });
 *
 * // option 2
 * splineModel.setParameter(QVariantList{1.0f, 5.0f, 0.2f, 0.5f});
 * \endcode
 */
void SaturatedSplineModel::setParameter(const QVariant& parameter)
{
    if(parameter.canConvert(QMetaType::QVariantMap))
        setParFromMap(parameter.toMap());
    else if(parameter.canConvert(QMetaType::QVariantList))
        setParFromList(parameter.toList());
    else
        qWarning() << "SaturatedSplineModel::setParameter: Could not set parameters! "
                      "reason: incompatible variant passed";
}

/*!
 * Helper method for setParameter().
 */
void SaturatedSplineModel::setParFromList(const QVariantList& list)
{
    if(list.size()<4)
    {
        qWarning() << "SaturatedSplineModel::setParameter: Could not set parameters! "
                      "reason: contained QVariantList has too few entries (required: 4 float)";
        return;
    }
    _a = list.at(0).toFloat();
    _b = list.at(1).toFloat();
    _softA = list.at(2).toFloat();
    _softB = list.at(3).toFloat();
}

/*!
 * Helper method for setParameter().
 */
void SaturatedSplineModel::setParFromMap(const QVariantMap& map)
{
    _a = map.value(QStringLiteral("a"), 0.0f).toFloat();
    _b = map.value(QStringLiteral("b"), FLT_MAX).toFloat();
    _softA = map.value(QStringLiteral("softA"), 0.0f).toFloat();
    _softB = map.value(QStringLiteral("softB"), 0.0f).toFloat();
}

/*!
 * Helper method for valueAt().
 */
float SaturatedSplineModel::spline1(float x) const
{
    return 1.0f/(4.0f*_softA)*(x*x) - (_a-_softA)/(2.0f*_softA)*x +
            (_a+_softA)*(_a+_softA)/(4.0f*_softA);
}

/*!
 * Helper method for valueAt().
 */
float SaturatedSplineModel::spline2(float x) const
{
    return -1.0f/(4.0f*_softB)*(x*x) + (_b+_softB)/(2.0f*_softB)*(x) -
            (_b-_softB)*(_b-_softB)/(4.0f*_softB);
}

/*!
 * \brief Creates a data model representing a step (Heaviside) function.
 *
 * The created step function has its step position at \a threshold. If
 * \a stepDirection = LeftIsZero, values below \a threshold are mapped to zero and all input larger
 * that \a threshold is mapped to \a amplitude. Behaves the other way round for
 * \a stepDirection = RightIsZero.
 */
StepFunctionModel::StepFunctionModel(float threshold, float amplitude, StepDirection stepDirection)
    : _threshold(threshold)
    , _amplitude(amplitude)
    , _stepDirection(stepDirection)
{
    setName(QStringLiteral("StepFunctionModel"));
}

/*!
 * \brief Returns the value from the model at \a position.
 *
 * The resulting value is described by the follwoing relation:
 *
 * For step direction LeftIsZero (see StepFunctionModel()):
 * \f$
 *  f(x)=\begin{cases}
 *  0 & x<threshold\\
 *  amplitude & x\geq threshold
 *  \end{cases}
 *  \f$
 */
float StepFunctionModel::valueAt(float position) const
{
    if(position < _threshold)
        return _amplitude * float(_stepDirection == RightIsZero);

    return _amplitude * float(_stepDirection == LeftIsZero);
}

// base class documentation
float StepFunctionModel::binIntegral(float position, float binWidth) const
{
    const auto binStart = position - 0.5f * binWidth;
    const auto binEnd = position + 0.5f * binWidth;

    if(binEnd < _threshold || binStart > _threshold) // bin fully before or behind step location
        return binWidth * valueAt(position);
    else // bin overlaps threshold position (needs computation of both contributions (one will be 0)
        return (_threshold - binStart) * valueAt(binStart) + (binEnd - _threshold) * valueAt(binEnd);
}

// base class documentation
AbstractIntegrableDataModel* StepFunctionModel::clone() const
{
    return new StepFunctionModel(*this);
}

/*!
 * Returns the parameters of this instance as QVariant.
 *
 * This returns a QVariantMap with three key-value-pairs, representing threshold, amplitude, and
 * which side of the step is zero.
 */
QVariant StepFunctionModel::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("threshold"), _threshold);
    ret.insert(QStringLiteral("amplitude"), _amplitude);
    ret.insert(QStringLiteral("left is zero"), bool(_stepDirection));

    return ret;
}

/*!
 * Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Parameters can be passed by either of the following two options:
 *
 * 1. As a QVariantMap with three key-value-pairs: ("Threshold", \a threshold),
 * ("Amplitude", \a amplitude), and ("Left is zero", \a stepDirection)
 * 2. As a QVariantList: In this case, the list must contain two floating point values and one
 * boolean sorted in the following order: \a threshold, \a amplitude, \a stepDirection.
 */
void StepFunctionModel::setParameter(const QVariant& parameter)
{
    if(parameter.canConvert(QMetaType::QVariantMap))
    {
        auto parMap = parameter.toMap();
        _threshold = parMap.value(QStringLiteral("threshold")).toFloat();
        _amplitude = parMap.value(QStringLiteral("amplitude")).toFloat();
        _stepDirection = StepDirection(parMap.value(QStringLiteral("left is zero")).toBool());
    }
    else if(parameter.canConvert(QMetaType::QVariantList))
    {
        auto parList = parameter.toList();
        if(parList.size() < 3)
        {
            qWarning() << "StepFunctionModel::setParameter: Could not set parameters! "
                          "reason: contained QVariantList has too few entries (required: 2 float, 1 bool)";
            return;
        }
        _threshold = parList.at(0).toFloat();
        _amplitude = parList.at(1).toFloat();
        _stepDirection = StepDirection(parList.at(2).toBool());
    }
    else
        qWarning() << "StepFunctionModel::setParameter: Could not set parameters! "
                      "reason: incompatible variant passed";
}

/*!
 * \brief Constructs a constant model that always returns the value \a amplitude.
 */
ConstantModel::ConstantModel(float amplitude)
    : _amplitude(amplitude)
{
    setName(QStringLiteral("ConstantModel"));
}

/*!
 * \brief Always returns the constant amplitude of this model.
 *
 * The amplitude is specified during construction or by means of setParameter().
 */
float ConstantModel::valueAt(float) const { return _amplitude; }

// base class documentation
AbstractIntegrableDataModel* ConstantModel::clone() const { return new ConstantModel(*this); }

// base class documentation
float ConstantModel::binIntegral(float, float binWidth) const
{
    return binWidth * _amplitude;
}

/*!
 * Returns the parameters of this instance as QVariant.
 *
 * This returns a QVariantMap with one key-value-pair, representing amplitude.
 */
QVariant ConstantModel::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("amplitude"), _amplitude);

    return ret;
}

/*!
 * Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Parameters can be passed by either of the following two options:
 *
 * 1. As a QVariantMap with one key-value-pair: ("amplitude", \a amplitude).
 * 2. As a QVariantList: In this case, the list must contain one floating point value, i.e. \a amplitude.
 */
void ConstantModel::setParameter(const QVariant& parameter)
{
    if(parameter.canConvert(QMetaType::QVariantMap))
    {
        auto parMap = parameter.toMap();
        _amplitude = parMap.value(QStringLiteral("amplitude")).toFloat();
    }
    else if(parameter.canConvert(QMetaType::QVariantList))
    {
        auto parList = parameter.toList();
        if(parList.size() < 1)
        {
            qWarning() << "ConstantModel::setParameter: Could not set parameters! "
                          "reason: contained QVariantList has too few entries (required: 1 float)";
            return;
        }
        _amplitude = parList.at(0).toFloat();
    }
    else
        qWarning() << "ConstantModel::setParameter: Could not set parameters! "
                      "reason: incompatible variant passed";
}

/*!
 * \brief Constructs a RectFunctionModel with a rectangle of height \a amplitude covering the
 * interval [\a rectBegin, \a rectEnd].
 *
 * This creates a model representing a rect function specified by \a rectBegin, \a rectEnd, and
 * \a amplitude.
 *
 * \sa valueAt()
 */
RectFunctionModel::RectFunctionModel(float rectBegin, float rectEnd, float amplitude)
    : _rectBegin(rectBegin)
    , _rectEnd(rectEnd)
    , _amplitude(amplitude)
{
    setName(QStringLiteral("RectFunctionModel"));
}

/*!
 * \brief Returns the value from the model at \a position.
 *
 * With the defining parameters of the rectanlge according to the constructor (see
 * RectFunctionModel()), the resulting value \f$ f(x) \f$ is described by the follwoing relation:
 *
 * \f$
 *  f(x)=\begin{cases}
 *  0 & x<rectBegin\\
 *  amplitude & rectBegin \leq x < rectEnd\\
 *  0 & x \geq rectEnd
 *  \end{cases}
 *  \f$
 */
float RectFunctionModel::valueAt(float position) const
{
    if(position < _rectBegin)
        return 0.0f;
    if(position < _rectEnd)
        return _amplitude;

    return 0.0f;
}

// base class documentation
AbstractDataModel* RectFunctionModel::clone() const
{
    return new RectFunctionModel(*this);
}

/*!
 * Returns the parameters of this instance as QVariant.
 *
 * This returns a QVariantMap with three key-value-pairs, representing start and end of the rect-interval
 * as well as the amplitude.
 */
QVariant RectFunctionModel::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("rect begin"), _rectBegin);
    ret.insert(QStringLiteral("rect end"), _rectEnd);
    ret.insert(QStringLiteral("amplitude"), _amplitude);

    return ret;
}

/*!
 * Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Parameters can be passed by either of the following two options:
 *
 * 1. As a QVariantMap with three key-value-pairs: ("Rect begin", \a rectBegin),
 * ("Rect end", \a rectEnd), and ("Amplitude", \a amplitude)
 * 2. As a QVariantList: In this case, the list must contain three floating point values sorted in
 * the following order: \a rectBegin, \a rectEnd, \a amplitude.
 */
void RectFunctionModel::setParameter(const QVariant& parameter)
{
    if(parameter.canConvert(QMetaType::QVariantMap))
    {
        auto parMap = parameter.toMap();
        _rectBegin = parMap.value(QStringLiteral("rect begin")).toFloat();
        _rectEnd = parMap.value(QStringLiteral("rect end")).toFloat();
        _amplitude = parMap.value(QStringLiteral("amplitude")).toFloat();
    }
    else if(parameter.canConvert(QMetaType::QVariantList))
    {
        auto parList = parameter.toList();
        if(parList.size() < 3)
        {
            qWarning() << "RectFunctionModel::setParameter: Could not set parameters! "
                          "reason: contained QVariantList has too few entries (required: 3 float)";
            return;
        }
        _rectBegin = parList.at(0).toFloat();
        _rectEnd = parList.at(1).toFloat();
        _amplitude = parList.at(2).toFloat();
    }
    else
        qWarning() << "RectFunctionModel::setParameter: Could not set parameters! "
                      "reason: incompatible variant passed";
}

/*!
 * \brief Always returns the input value (i.e. \a position).
 *
 * This represents the identity function \f$ f(x) = x \f$.
 */
float IdentityModel::valueAt(float position) const
{
    return position;
}

/*!
 * \brief \copybrief AbstractIntegrableDataModel::binIntegral
 *
 * Returns
 * \f$ \frac{b^2-a^2}{2}\f$, with the abbreviations:
 *
 * \f$ \begin{align*}
 * a &= position - 0.5\cdot binWidth \\
 * b &= position + 0.5\cdot binWidth.
 *     \end{align*}
 * \f$
 */
float IdentityModel::binIntegral(float position, float binWidth) const
{
    const auto a = position - 0.5f *binWidth;
    const auto b = position + 0.5f *binWidth;

    return 0.5f * (b*b - a*a);
}

// base class documentation
AbstractIntegrableDataModel* IdentityModel::clone() const
{
    return new IdentityModel(*this);
}

IdentityModel::IdentityModel()
{
    setName(QStringLiteral("IdentityModel"));
}

/*!
 * \brief Constructs a GaussianModel1D representing a one-dimensional Gaussian curve.
 *
 * This creates a model representing a one-dimensional Gaussian function specified by \a amplitude,
 * \a mean, and \a std.
 *
 * \a std must be strictly positive, i.e. \a std > 0; throws an std::domain_error for invalid input.
 *
 * \sa valueAt()
 */
GaussianModel1D::GaussianModel1D(float amplitude, float mean, float std)
    : _ampl(amplitude)
    , _mean(mean)
    , _std(std)
{
    if(std <= 0.0f)
        throw std::domain_error("GaussianModel1D: Invalid value for standard deviation "
                                "(requires: std > 0.0).");

    setName(QStringLiteral("GaussianModel1D"));
}

/*!
 * \brief Returns the value from the model at \a position.
 *
 * With the defining parameters of the Gaussian curve according to the constructor (see
 * GaussianModel1D()):
 *
 * - \a amplitude: \f$ A \f$
 * - \a mean: \f$ \mu \f$
 * - \a std: \f$ \sigma \f$
 *
 * the resulting value is given by follwoing relation:
 *
 * \f$
 * f(x)= \frac{A}{\sqrt{2\pi \sigma^2}} \cdot  \exp\left[-\frac{1}{2 \sigma^2} \cdot (x-\mu)^2\right]
 * \f$
 */
float GaussianModel1D::valueAt(float position) const
{
    const auto var    = std::pow(_std, 2.0f);
    const auto expon  = -0.5f * std::pow(position - _mean, 2.0f) / var;
    const auto normal = 1.0f / std::sqrt(2.0f * float(PI) * var);

    return _ampl * normal * std::exp(expon);
}

// base class documentation
float GaussianModel1D::binIntegral(float position, float binWidth) const
{
    static const auto transfErf = [] (float x)
    {
        return 0.5f * (1.0f + std::erf(x / std::sqrt(2.0f)));
    };

    const auto from = position - 0.5f * binWidth;
    const auto to   = position + 0.5f * binWidth;

    return transfErf((to - _mean) / _std) - transfErf((from - _mean) / _std);
}

// base class documentation
AbstractIntegrableDataModel* GaussianModel1D::clone() const
{
    return new GaussianModel1D(*this);
}

// base class documentation
QVariant GaussianModel1D::toVariant() const
{
    QVariantMap ret = AbstractDataModel::toVariant().toMap();

    ret.insert(QStringLiteral("#"), "GaussianModel1D");

    return ret;
}

// base class documentation
QVariant GaussianModel1D::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("amplitude"), _ampl);
    ret.insert(QStringLiteral("mean"), _mean);
    ret.insert(QStringLiteral("std"), _std);

    return ret;
}

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * The value passed to \a parameter must be a QVariantMap containing one or multiple of the
 * following (key, value)-pairs:
 *
 * -("amplitude", [\c float] \a amplitude),
 * -("mean", [\c float] \a mean),
 * -("std", [\c float] \a std).
 *
 * \a std must be strictly positive, i.e. \a std > 0; otherwise, a warning is generated and the
 * parameter will be set to 1.0.
 *
 * Example: setting mean to 3 and amplitude to 2.5
 * \code
 * auto gaussian = std::make_shared<GaussianModel1D>();
 * gaussian->setParameter(QVariantMap{ { "mean", 3.0f },
 *                                     { "amplitude", 2.5f } } );
 * \endcode
 */
void GaussianModel1D::setParameter(const QVariant& parameter)
{
    const auto setSaveStd = [this] (float std)
    {
        if(std <= 0.0f)
        {
            qWarning() << "GaussianModel1D: Trying to set invalid standard deviation value in x "
                          "(requested: " + QString::number(std) + "; requires: std > 0.0). "
                          "Value will be ignored and set to default (std = 1.0).";
            std = 1.0f;
        }

        _std = std;
    };

    const auto varMap = parameter.toMap();

    if(varMap.contains(QStringLiteral("amplitude")))
        _ampl  = varMap.value(QStringLiteral("amplitude")).toFloat();
    if(varMap.contains(QStringLiteral("mean")))
        _mean = varMap.value(QStringLiteral("mean")).toFloat();
    if(varMap.contains(QStringLiteral("std")))
        setSaveStd(varMap.value(QStringLiteral("std")).toFloat());
}


} // namespace CTL
