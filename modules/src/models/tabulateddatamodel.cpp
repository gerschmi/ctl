#include "tabulateddatamodel.h"
#include "pointseriesbase.h"
#include <QDebug>
#include <stdexcept>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(TabulatedDataModel)

/*!
 * Constructs an empty TabulatedDataModel.
 */
TabulatedDataModel::TabulatedDataModel()
{
    setName(QStringLiteral("TabulatedDataModel"));
}
/*!
 * Constructs a TabulatedDataModel with lookup values given by \a dataSeries.
 *
 * If \a dataSeries contains multiple entries with same x value, only the last one will be found in
 * the lookup table after construction.
 *
 * Example:
 * \code
 *  XYDataSeries dataSeries;
 *  dataSeries.append(1.2f,  3.4f);
 *  dataSeries.append(4.5f,  6.7f);
 *  dataSeries.append(8.9f, 10.11f);
 *
 *  auto table = TabulatedDataModel(dataSeries);
 *  qInfo() << table.lookupTable();
 *  // output: QMap((1.2, 3.4)(4.5, 6.7)(8.9, 10.11))
 *
 *
 *  // double entry
 *  dataSeries.append(4.5f, 42.0f);      // now 'dataSeries' has two entries with a key (x value) of 4.5
 *                                       // its value (42.0) will replace the old one (i.e. 13.37)
 *
 *  // re-construct the model
 *  table = TabulatedDataModel(dataSeries);
 *  qInfo() << table.lookupTable();
 *  // output: QMap((1.2, 3.4)(4.5, 42)(8.9, 10.11))
 *  //         Warning: Tabulated data already contains a value with the key 4.5 ; the previous table entry will be replaced.
 * \endcode
 *
 * \sa setData(const PointSeriesBase&).
 */
TabulatedDataModel::TabulatedDataModel(const PointSeriesBase& dataSeries)
    : TabulatedDataModel()
{
    setData(dataSeries);
}

/*!
 * Constructs a TabulatedDataModel with lookup values given by \a table.
 *
 * Example:
 * \code
 *  QMap<float, float> values;
 *  values[5.0f]  = 1.0f;
 *  values[10.0f] = 3.0f;
 *  values[15.0f] = 10.0f;
 *  values[20.0f] = 7.7f;
 *
 *  auto table = TabulatedDataModel(std::move(values));
 * \endcode
 */
TabulatedDataModel::TabulatedDataModel(QMap<float, float> table)
    : _data(std::move(table))
{
    setName(QStringLiteral("TabulatedDataModel"));
}

/*!
 * Constructs a TabulatedDataModel with lookup values given by \a keys and \a values.
 *
 * Data with the same key will be overwritten and the entry occuring last in the vector will remain
 * in the resulting tabulated data.
 *
 * Both vectors need to have the same length; throws an exception otherwise.
 *
 * Example:
 * \code
 *  auto keys   = QVector<float>{ 2.1f, 4.2f, 13.9f, 7.8f };
 *  auto values = QVector<float>{ 42.0f, 13.37f, 42.0f, 13.37f };
 *
 *  auto table = TabulatedDataModel(keys, values);
 *  qInfo() << table.lookupTable();
 *  // output: QMap((2.1, 42)(4.2, 13.37)(7.8, 13.37)(13.9, 42))
 *
 *
 *  // double entry
 *  keys.append(4.2f);      // now we have two entries of 4.2 in the 'keys' vector
 *  values.append(66.6f);   // this value will replace the old one (i.e. 13.37)
 *
 *  // re-construct the model
 *  table = TabulatedDataModel(keys, values);
 *  qInfo() << table.lookupTable();
 *  // output: QMap((2.1, 42)(4.2, 66.6)(7.8, 13.37)(13.9, 42))
 *
 *
 *  // differing length of keys and values
 *  keys.append(10.0f);
 *  try {
 *      table = TabulatedDataModel(keys, values);
 *  } catch (std::domain_error& err) {
 *      qInfo() << err.what();
 *  }
 *  // output: TabulatedDataModel::setData(): keys and values have different size.
 * \endcode
 */
TabulatedDataModel::TabulatedDataModel(const QVector<float>& keys, const QVector<float>& values)
{
    setData(keys, values);
    setName(QStringLiteral("TabulatedDataModel"));
}

/*!
 * Returns a constant reference to the lookup table stored in this instance
 */
const QMap<float, float>& TabulatedDataModel::lookupTable() const { return _data; }

/*!
 * \brief Removes all values from the table.
 */
void TabulatedDataModel::clearData() { _data.clear(); }

/*!
 * Sets the lookup table of this instance to the values in \a dataSeries. This replaces any previous
 * data.
 *
 * \a dataSeries should not contain multiple entries with the same x value; if it does contain
 * such values, any reoccuring value replaces the previous one. Ultimately, only the key-value-pair
 * corresponding to the entry appearing last in \a dataSeries (i.e. from all of those with same x)
 * will be in the table after this call.
 *
 * Example:
 * \code
 *  TabulatedDataModel table;   // an empty table model
 *
 *  // create an XryTube object, set the tube voltage to 115 kV and sample its spectrum
 *  XrayTube tube;
 *  tube.setTubeVoltage(115.0f);
 *  const auto sampledSpectrum = tube.spectrum(10);     // sample 10 values
 *
 *  // set the data for 'table' to the points of the sampled spectrum
 *  table.setData(sampledSpectrum);
 *
 *  // print table
 *  for(auto it = table.lookupTable().cbegin(); it != table.lookupTable().cend(); ++it)
 *      qInfo() << it.key() << it.value();
 *
 *  // output: 5.75 6.47231e-05
 *  //         17.25 0.047528
 *  //         28.75 0.193677
 *  //         40.25 0.2194
 *  //         51.75 0.193015
 *  //         63.25 0.186251
 *  //         74.75 0.0760962
 *  //         86.25 0.0484104
 *  //         97.75 0.0269858
 *  //         109.25 0.00857234
 * \endcode
 */
void TabulatedDataModel::setData(const PointSeriesBase& dataSeries)
{
    clearData();

    for(const auto pt : dataSeries.data())
    {
        if(_data.contains(pt.x()))
            qWarning() << "Tabulated data already contains a value with the key" << pt.x() << ";"
                       << "the previous table entry will be replaced.";
        insertDataPoint(pt.x(), pt.y());
    }
}

/*!
 * Sets the lookup table of this instance to \a table.
 */
void TabulatedDataModel::setData(QMap<float, float> table) { _data = std::move(table); }

/*!
 * Sets the lookup table of this instance to the values given by \a keys and \a values.
 *
 * Data with the same key will be overwritten and the entry occuring last in the vector will remain
 * in the resulting tabulated data.
 *
 * Both vectors need to have the same length; throws an exception otherwise.
 */
void TabulatedDataModel::setData(const QVector<float>& keys, const QVector<float>& values)
{
    Q_ASSERT(keys.size() == values.size());
    if(keys.size() != values.size())
        throw std::domain_error("TabulatedDataModel::setData(): keys and values have different size.");

    _data.clear();
    for(int k = 0, length = keys.length(); k < length; ++k)
        _data.insert(keys.at(k), values.at(k));
}

/*!
 * Inserts the (\a key, \a value) pair into the lookup table of this instance.
 *
 * If an entry with the same key already exists, it will be overwritten.
 */
void TabulatedDataModel::insertDataPoint(float key, float value) { _data.insert(key, value); }

/*!
 * \brief Puts all data in this instance into a QVariantList and returns it.
 *
 * The returned QVariantList contains all data points in the table of this instance - these data
 * points are the individual entries in the list. Each of these entries itself is a QVariantList
 * with two elements, the (key, value)-pair for the table entry.
 */
QVariantList TabulatedDataModel::dataAsVariantList() const
{
    QVariantList list;

    auto i = _data.constBegin();
    while(i != _data.constEnd())
    {
        list.append(QVariant(QVariantList{ i.key(), i.value() }));
        ++i;
    }

    return list;
}

/*!
 * \brief Sets the data of this instance based on values in \a list.
 *
 * The passed QVariantList \a list must contain all data points that shall be in the table -
 * these data points are individual entries in \a list. Each of these entries itself must be a
 * QVariantList with two elements, the (key, value)-pair for the table entry. Entries with less than
 * two elements will be ignored.
 *
 * Note that this method is private, but we illustrate an example anyways.
 * Example: set the data points (1, 3) and (3, 7) to the table.
 * \code
 *  auto table = std::make_shared<TabulatedDataModel>();
 *  table->setDataFromVariantList(QVariantList{ QVariantList{1.0, 3.0},
 *                                              QVariantList{3.0, 7.0} });
 * \endcode
 */
void TabulatedDataModel::setDataFromVariantList(const QVariantList& list)
{
    _data.clear();
    for(const auto& var : list)
    {
        const auto dataPoint = var.toList();
        if(dataPoint.size() < 2)
            continue;
        _data.insert(dataPoint.at(0).toFloat(), dataPoint.at(1).toFloat());
    }
}

/*!
 * Returns the integral of the tabulated data over the interval
 * \f$ \left[position-\frac{binWidth}{2},\,position+\frac{binWidth}{2}\right] \f$.
 *
 * This method uses trapezoid integration. The following figure provides an example of the procedure.
 *
 * \image html TabulatedDataModel.svg "Example of integration of lookup table data in a TabulatedDataModel."
 *
 * The table contains 10 data points (star symbols). Integration is shown for three different cases.
 * Interval 1: The integration bin covers multiple tabulated values. In this case, the result is a
 * sum over all partial intervals. If one (or both) integration border(s) is outside the range of
 * tabulated values, the requested point is extrapolated linearly to zero (see left side of
 * Interval 1). Interval 2: Integration bin lays between two tabulated values. Interval 3: Interval
 * is fully outside the range of tabulated data. This bin integral returns zero.
 */
float TabulatedDataModel::binIntegral(float position, float binWidth) const
{
    const auto from = position - 0.5f * binWidth;
    const auto to = position + 0.5f * binWidth;

    auto ret = 0.0f;

    const auto lowerEndPosition = _data.lowerBound(from);
    const auto upperEndPosition = _data.lowerBound(to);

    // check if integration interval is fully outside tabulated data --> return zero
    if(lowerEndPosition == _data.end() || to < _data.begin().key())
        return 0.0f;

    // integration interval lies fully within two tabulated values --> return value * binWidth
    if(lowerEndPosition == upperEndPosition && upperEndPosition != _data.begin())
        return valueAt(position) * binWidth;

    // if function reaches this point, multiple segments need to be integrated
    // compute contribution of lower end
    const auto lowerEndValue = valueAt(from);
    const auto lowerEndContr = 0.5f * (lowerEndValue + lowerEndPosition.value()) *
                                      (lowerEndPosition.key() - from);

    ret += lowerEndContr;

    qDebug() << "lowerEndContr: Integral from " << lowerEndValue << "(at: " << from << ") to "
             << lowerEndPosition.value() << "(at: " << lowerEndPosition.key()
             << ") = " << lowerEndContr;

    if(upperEndPosition == _data.begin())
        return ret;

    // compute contributions of all 'full segments'
    auto currentPosition = lowerEndPosition;
    while(currentPosition != upperEndPosition)
    {
        if(std::next(currentPosition) == _data.end())
            break;
        if(std::next(currentPosition).key() > to)
            break;
        float width = std::next(currentPosition).key() - currentPosition.key();
        float height = 0.5f * (std::next(currentPosition).value() + currentPosition.value());

        qDebug() << "contribution for " << currentPosition.key() << "to" << std::next(currentPosition).key()
                 << ": " << width * height;

        ret += width * height;
        ++currentPosition;
    }

    if(qFuzzyCompare(currentPosition.key(), to))
        return ret;

    // compute contribution of upper end
    const auto lastSample = std::prev(upperEndPosition);
    const auto upperEndValue = valueAt(to);
    const auto upperEndContr = 0.5f * (lastSample.value() + upperEndValue) * (to - lastSample.key());

    ret += upperEndContr;

    qDebug() << "upperEndContr: Integral from " << std::prev(upperEndPosition).value()
             << "(at: " << std::prev(upperEndPosition).key() << ") to " << upperEndValue << "(at: " << to
             << ") = " << upperEndContr;

    return ret;
}

// base class documentation
AbstractIntegrableDataModel* TabulatedDataModel::clone() const { return new TabulatedDataModel(*this); }

/*!
 * Returns a linearly interpolated value at position \a pos based on the data in the lookup table.
 *
 * Returns zero if \a pos is outside the range of the available tabulated data.
 */
float TabulatedDataModel::valueAt(float pos) const
{
    // check if data contains an entry for 'pos'
    if(_data.contains(pos))
        return _data.value(pos);

    // find position of next tabulated entry in data with key > pos
    const auto nextValidDataPt = _data.lowerBound(pos);

    // check if value is outside of tabulated data --> return 0
    if(nextValidDataPt == _data.begin() || nextValidDataPt == _data.end())
        return 0.0f;

    // now it is assured that pos is contained in range of keys of data
    const auto weight = (nextValidDataPt.key() - pos) /
                        (nextValidDataPt.key() - std::prev(nextValidDataPt).key());
    const auto contribLower = std::prev(nextValidDataPt).value() * weight;
    const auto contribUpper = (nextValidDataPt).value() * (1.0f - weight);

    return contribLower + contribUpper;
}

// base class documentation
QVariant TabulatedDataModel::parameter() const
{
    auto variant = AbstractIntegrableDataModel::parameter().toMap();
    variant.insert(QStringLiteral("data"), dataAsVariantList());

    return variant;
}

/*!
 * \brief Sets the data of this instance based on values in \a parameter.
 *
 * The passed \a parameter must be a QVariantMap that contains an entry "data" which is a
 * QVariantList that contains all data points that shall be put into the table
 *
 * See setDataFromVariantList() for more details.
 *
 * Example: set the data points (1, 3) and (3, 7) to the table.
 * \code
 *  auto table = std::make_shared<TabulatedDataModel>();
 *  const auto dataList = QVariantList{ QVariantList{1.0, 3.0},
 *                                      QVariantList{3.0, 7.0} };
 *  table->setParameter(QVariantMap{ {"data", dataList} });
 * \endcode
 *
 * Note that it will typically be much simpler to clear the table (see clearData()) and insert data
 * points individually (see insertDataPoint()) or use a setData() method.
 */
void TabulatedDataModel::setParameter(const QVariant& parameter)
{
    AbstractIntegrableDataModel::setParameter(parameter);
    if(parameter.toMap().contains(QStringLiteral("data")))
        setDataFromVariantList(parameter.toMap().value(QStringLiteral("data")).toList());
}

} // namespace CTL
