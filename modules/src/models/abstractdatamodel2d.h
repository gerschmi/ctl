#ifndef CTL_ABSTRACTDATAMODEL2D_H
#define CTL_ABSTRACTDATAMODEL2D_H

#include "io/serializationinterface.h"

#include <memory>

namespace CTL {

template<typename>
class Chunk2D;
template<typename>
class Range;

typedef Range<float> SamplingRange;

/*!
 * \class AbstractDataModel2D
 * \brief The AbstractDataModel2D class is the base class for two-dimensional data models.
 *
 * A two-dimensional data model represents data on a 2D grid, i.e. given a tuple of x and y
 * coordinates, it provides a well-defined value.
 * Sub-classes must implement the method to sample a value at a given position (valueAt()), as well
 * as a clone() method.
 *
 * Data from the model can also be sampled in a structured pattern using sampleChunk(), which
 * samples values on an equidistantly-spaced 2D grid and returns data as a Chunk2D.
 *
 * Models can have parameters specifying their specific properties. Parameters can be set by passing
 * a QVariant that contains all necessary information. Re-implement the setParameter() method to
 * parse the QVariant into your required format (i.e. extract relevant information) within
 * sub-classes of AbstractDataModel2D.
 *
 * To enable de-/serialization of sub-class objects, also reimplement the parameter() method such
 * that it stores all parameters of the model in a QVariant. The resulting QVariant should be fully
 * compatible with setParameter() in the sense that setParameter(parameter()) does not change the
 * objects properties. Additionally, call the macro #DECLARE_SERIALIZABLE_TYPE(YourNewClassName)
 * within the .cpp file of your new class (substitute "YourNewClassName" with the actual class name).
 * Objects of the new class can then be de-/serialized with any of the serializer classes (see also
 * AbstractSerializer).
 *
 * Alternatively, the de-/serialization interface methods toVariant() and fromVariant() can be
 * reimplemented directly. This might be required in some specific situations (usually when
 * polymorphic class members are in use). For the majority of cases, using the parameter() /
 * setParameter() approach should be sufficient and is recommended.
 */
class AbstractDataModel2D : public SerializationInterface
{
    CTL_TYPE_ID(1000)

    public:virtual float valueAt(float x, float y) const = 0;
    public:virtual AbstractDataModel2D* clone() const = 0;

public:
    virtual QVariant parameter() const;
    virtual void setParameter(const QVariant& parameter);

    void setParameter(const QString& name, const QVariant& value);

    // SerializationInterface interface
    void fromVariant(const QVariant &variant) override;
    QVariant toVariant() const override;

    QString name() const { return _name; }
    void setName(const QString& name) { _name = name; }

    Chunk2D<float> sampleChunk(const SamplingRange& xRange, const SamplingRange& yRange,
                               uint nbSamplesX, uint nbSamplesY) const;

    ~AbstractDataModel2D() override = default;

private:
    QString _name;

protected:
    AbstractDataModel2D() = default;
    AbstractDataModel2D(const AbstractDataModel2D&) = default;
    AbstractDataModel2D(AbstractDataModel2D&&) = default;
    AbstractDataModel2D& operator=(const AbstractDataModel2D&) = default;
    AbstractDataModel2D& operator=(AbstractDataModel2D&&) = default;
};

// Data model operators
std::shared_ptr<AbstractDataModel2D> operator+(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs);
std::shared_ptr<AbstractDataModel2D> operator-(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs);
std::shared_ptr<AbstractDataModel2D> operator*(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs);
std::shared_ptr<AbstractDataModel2D> operator/(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs);
std::shared_ptr<AbstractDataModel2D>& operator+=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs);
std::shared_ptr<AbstractDataModel2D>& operator-=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs);
std::shared_ptr<AbstractDataModel2D>& operator*=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs);
std::shared_ptr<AbstractDataModel2D>& operator/=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs);

} // namespace CTL

/*! \file */
///@{

/*!
 * \fn std::shared_ptr<AbstractDataModel2D> CTL::operator+(std::shared_ptr<AbstractDataModel2D> lhs,
 *                                                         std::shared_ptr<AbstractDataModel2D> rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Creates a data model that represents the sum of \a lhs and \a rhs in the sense that, when
 * requesting a value from the resulting model at position (x, y), the returned value will be the
 * sum of the individually sampled values of each model, i.e.:
 * \code lhs->valueAt(x,y) + rhs->valueAt(x,y)\endcode.
 *
 * Note that you will no longer be able to modify the parameters of any of the two input models
 * based on the returned 'sum'-model.
 */

/*!
 * \fn std::shared_ptr<AbstractDataModel2D> CTL::operator-(std::shared_ptr<AbstractDataModel2D> lhs,
 *                                                         std::shared_ptr<AbstractDataModel2D> rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Creates a data model that represents the difference of \a lhs and \a rhs in the sense that, when
 * requesting a value from the resulting model at position (x, y), the returned value will be the
 * difference of the individually sampled values of each model, i.e.:
 * \code lhs->valueAt(x,y) - rhs->valueAt(x,y)\endcode.
 *
 * Note that you will no longer be able to modify the parameters of any of the two input models
 * based on the returned 'difference'-model.
 */

/*!
 * \fn std::shared_ptr<AbstractDataModel2D> CTL::operator*(std::shared_ptr<AbstractDataModel2D> lhs,
 *                                                         std::shared_ptr<AbstractDataModel2D> rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Creates a data model that represents the product of \a lhs and \a rhs in the sense that, when
 * requesting a value from the resulting model at position (x, y), the returned value will be the
 * product of the individually sampled values of each model, i.e.:
 * \code lhs->valueAt(x,y) * rhs->valueAt(x,y)\endcode.
 *
 * Note that you will no longer be able to modify the parameters of any of the two input models
 * based on the returned 'product'-model.
 */

/*!
 * \fn std::shared_ptr<AbstractDataModel2D> CTL::operator/(std::shared_ptr<AbstractDataModel2D> lhs,
 *                                                         std::shared_ptr<AbstractDataModel2D> rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Creates a data model that represents the quotient of \a lhs and \a rhs in the sense that, when
 * requesting a value from the resulting model at position (x, y), the returned value will be the
 * quotient of the individually sampled values of each model, i.e.:
 * \code lhs->valueAt(x,y) / rhs->valueAt(x,y)\endcode.
 *
 * Note that you will no longer be able to modify the parameters of any of the two input models
 * based on the returned 'quotient'-model.
 */

/*!
 * \fn std::shared_ptr<AbstractDataModel2D>& CTL::operator+=(std::shared_ptr<AbstractDataModel2D>& lhs,
 *                                                           const std::shared_ptr<AbstractDataModel2D>& rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Replaces \a lhs by a 'sum'-model of \a lhs + \a rhs.
 *
 * See CTL::operator+(std::shared_ptr<AbstractDataModel2D>,
 *                    std::shared_ptr<AbstractDataModel2D>) for more details.
 */

/*!
 * \fn std::shared_ptr<AbstractDataModel2D>& CTL::operator-=(std::shared_ptr<AbstractDataModel2D>& lhs,
 *                                                           const std::shared_ptr<AbstractDataModel2D>& rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Replaces \a lhs by a 'difference'-model of \a lhs - \a rhs.
 *
 * See CTL::operator-(std::shared_ptr<AbstractDataModel2D>,
 *                    std::shared_ptr<AbstractDataModel2D>) for more details.
 */

/*!
 * \fn std::shared_ptr<AbstractDataModel2D>& CTL::operator*=(std::shared_ptr<AbstractDataModel2D>& lhs,
 *                                                           const std::shared_ptr<AbstractDataModel2D>& rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Replaces \a lhs by a 'product'-model of \a lhs * \a rhs.
 *
 * See CTL::operator*(std::shared_ptr<AbstractDataModel2D>,
 *                    std::shared_ptr<AbstractDataModel2D>) for more details.
 */

/*!
 * \fn std::shared_ptr<AbstractDataModel2D>& CTL::operator/=(std::shared_ptr<AbstractDataModel2D>& lhs,
 *                                                           const std::shared_ptr<AbstractDataModel2D>& rhs)
 * \relates CTL::AbstractDataModel2D
 *
 * Replaces \a lhs by a 'quotient'-model of \a lhs / \a rhs.
 *
 * See CTL::operator/(std::shared_ptr<AbstractDataModel2D>,
 *                    std::shared_ptr<AbstractDataModel2D>) for more details.
 */

///@}

#endif // CTL_ABSTRACT2DDATAMODEL2D_H
