#ifndef CTL_DATAMODELS2D_H
#define CTL_DATAMODELS2D_H

#include "abstractdatamodel2d.h"

#include <memory>
#include <QRectF>

namespace CTL {

/*!
 * \class ConstantModel2D
 * \brief The ConstantModel2D class represents a model that always returns a fixed value.
 *
 * This model maps all input to the same output value specified in the constructor.
 */
class ConstantModel2D : public AbstractDataModel2D
{
    CTL_TYPE_ID(1001)

    // AbstractDataModel2D interface
    public: float valueAt(float x, float y) const override;
    public: AbstractDataModel2D* clone() const override;

public:
    explicit ConstantModel2D(float amplitude = 1.0f);

    QVariant parameter() const override;
    void setParameter(const QVariant &parameter) override;
    QVariant toVariant() const override;

    using AbstractDataModel2D::setParameter;

private:
    float _amplitude;
};

/*!
 * \class GaussianModel2D
 * \brief The GaussianModel2D class represents a two-dimensional Gaussian curve.
 *
 * This model maps input values according to a two-dimensional Gaussian function.
 */
class GaussianModel2D : public AbstractDataModel2D
{
    CTL_TYPE_ID(1002)

    // AbstractDataModel2D interface
    public:float valueAt(float x, float y) const override;
    public:AbstractDataModel2D* clone() const override;

public:
    explicit GaussianModel2D(float amplitude = 1.0f, float mean = 0.0f, float std = 1.0f,
                             float correlation = 0.0f);
    GaussianModel2D(float amplitude,
                    float meanX, float meanY,
                    float stdX, float stdY, float correlation = 0.0f);

    // SerializationInterface interface
    QVariant toVariant() const override;
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    using AbstractDataModel2D::setParameter;

private:
    float _ampl = 1.0f;
    float _meanX = 0.0f;
    float _meanY = 0.0f;
    float _stdX = 1.0f;
    float _stdY = 1.0f;
    float _corr = 0.0f;

    void setSaveCorr(float corr);
    void setSaveStdX(float std);
    void setSaveStdY(float std);
};

/*!
 * \class RectModel2D
 * \brief The RectModel2D class represents a two-dimensional rectangular (rect) function.
 *
 * This model maps input values according to a rect function in both dimensions. The start and end
 * points of the rectangle, as well as its height can be specified in the constructor of using
 * setParameter().
 */
class RectModel2D : public AbstractDataModel2D
{
    CTL_TYPE_ID(1003)

    // AbstractDataModel2D interface
    public: float valueAt(float x, float y) const override;
    public: AbstractDataModel2D* clone() const override;

public:
    explicit RectModel2D(float amplitude = 1.0f, float xFrom = -0.5f, float xTo = 0.5f,
                                                 float yFrom = -0.5f, float yTo = 0.5f);
    RectModel2D(float amplitude, const SamplingRange& xRange, const SamplingRange& yRange);
    RectModel2D(float amplitude, const QRectF& rect);

    QVariant parameter() const override;
    void setParameter(const QVariant &parameter) override;
    QVariant toVariant() const override;

    using AbstractDataModel2D::setParameter;

private:
    float _amplitude;
    QRectF _rect;
};

/*!
 * \class SeparableProductModel
 * \brief The SeparableProductModel class represents the product of individual (1D) models in each
 * dimension.
 *
 * This model maps input in each dimension according to the corresponding model and returns the
 * product of both values. This can be used to construct models that have no inter-dependency
 * between both dimensions (i.e. they are separable).
 */
class SeparableProductModel : public AbstractDataModel2D
{
    CTL_TYPE_ID(1100)

    // AbstractDataModel2D interface
    public: float valueAt(float x, float y) const override;
    public: AbstractDataModel2D* clone() const override;

public:
    SeparableProductModel(std::shared_ptr<AbstractDataModel> xModel,
                          std::shared_ptr<AbstractDataModel> yModel);

    AbstractDataModel& xModel();
    AbstractDataModel& yModel();

    QVariant parameter() const override;
    void setParameter(const QVariant &parameter) override;
    QVariant toVariant() const override;


private:
    SeparableProductModel() = default;

    std::shared_ptr<AbstractDataModel> _xModel;
    std::shared_ptr<AbstractDataModel> _yModel;
};


} // namespace CTL

#endif // CTL_DATAMODELS2D_H
