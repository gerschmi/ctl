#ifndef CTL_INTERVALDATASERIES_H
#define CTL_INTERVALDATASERIES_H

#include "pointseriesbase.h"
#include "abstractdatamodel.h"
#include "processing/range.h"

namespace CTL {

/*!
 * \class IntervalDataSeries
 *
 * \brief The IntervalDataSeries class is a container for a series of values sampled in equidistant
 * steps (i.e. intervals with equal bin width).
 *
 * This class is a specialization of PointSeriesBase to manage equidistantly-sampled data points.
 * Although data is managed as a series of *x* and *y* pairs internally, only *y*-values might be
 * manipulated, in order to ensure that the actual sampling points (i.e. *x*-values) are kept on the
 * equidistant 'grid'.
 *
 * There are two main ways of creating an IntervalDataSeries object:
 * 1. Sample data from an AbstractIntegrableDataModel subclass, and
 * 2. Individually add (consecutive) values to the series.
 *
 * For 1., the sampledFromModel() factory method can be used. These take a data model and return a
 * series of values sampled within the specified interval. You can choose whether the sampled values
 * shall be data integrated over the entire bin or mean values within the individual intervals.
 *
 * Example:
 * \code
 * // create a step function (steps from 0 to 1 at x=0)
 * auto dataModel = StepFunctionModel();
 *
 * // sample the interval [-5, 5] with 5 values, i.e. bin width is 2.0
 * auto series = IntervalDataSeries::sampledFromModel(dataModel, -5.0f, 5.0f, 5);
 *
 * // print values
 * for(const auto& pt : series.data())
 *     qInfo() << pt.x() << pt.y();
 *
 * // output: (bin center, value)-pairs
 * // -4 0
 * // -2 0
 * // 0 1
 * // 2 2
 * // 4 2
 * // Note that the sampled values represent bin integrals!
 * \endcode
 *
 * When manually filling a series (method 2.), you need to decide on the distance between sampling
 * points (i.e. bin width) used in the series to create an IntervalDataSeries using its constructor.
 * Besides the bin width, you might also set the beginning of the first interval (i.e. lower edge of
 * the first bin) and specify the type of data, your sampled values will represent (i.e. either bin
 * integrals of mean values). Note that all these properties may not be changed afterwards. After
 * creating the series object, data points can be appended to the series with append(). The sampling
 * point (i.e. *x*-value or bin position) is determined automatically, based on the bin width and
 * the specified beginning of the first bin.
 *
 * IntervalDataSeries offers the possibility to compute both the sum as well as the integral over
 * all values in the series; see sum() and integral() for details. Further convenience provided
 * includes normalization of the values in the series by the integral (see normalizeByIntegral()),
 * computation of the centroid ('center-of-mass', see centroid()), and the possibility to clamp the
 * sampled series to a specific range of *x*-values (see clampToRange()).
 */
class IntervalDataSeries : public PointSeriesBase
{
public:
    enum ValueType { BinIntegral,
                     MeanValue, };

    //IntervalDataSeries() = default;
    explicit IntervalDataSeries(float binWidth, float firstBinStart = 0.0f,
                                ValueType valueType = BinIntegral);
    IntervalDataSeries(float binWidth, float firstBinStart, const std::vector<float>& values,
                       ValueType valueType = BinIntegral);

    // factory methods
    static IntervalDataSeries sampledFromModel(const AbstractIntegrableDataModel& dataModel,
                                               float from, float to, uint nbSamples,
                                               ValueType samples = BinIntegral);
    static IntervalDataSeries sampledFromModel(std::shared_ptr<AbstractIntegrableDataModel> dataModel,
                                               float from, float to, uint nbSamples,
                                               ValueType samples = BinIntegral);

    // data manipulation
    void append(float value);
    void append(const std::vector<float>& values);
    void clampToRange(const SamplingRange& range);
    void normalizeByIntegral();
    using PointSeriesBase::normalizeByMaxAbsVal;
    using PointSeriesBase::normalizeByMaxVal;
    using PointSeriesBase::scaleValues;
    void setValue(uint sampleNb, float value);
    void setValues(const std::vector<float>& values);

    // other methods
    SamplingRange binBounds(uint binNb) const;
    float binWidth() const;
    float centroid() const;
    float integral() const;
    float integral(const std::vector<float>& weights) const;
    IntervalDataSeries normalizedByIntegral() const;
    SamplingRange samplingRange() const; // overloads PointSeriesBase::samplingRange()
    ValueType valueType() const;

private:
    float _binWidth;
    float _firstBinStart;
    ValueType _valueType;

    std::vector<float> samplingPointsForIndices(uint startIdx, uint endIdx) const;
};

}
#endif // CTL_INTERVALDATASERIES_H
