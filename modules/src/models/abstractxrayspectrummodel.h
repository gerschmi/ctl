#ifndef CTL_ABSTRACTXRAYSPECTRUMMODEL_H
#define CTL_ABSTRACTXRAYSPECTRUMMODEL_H

#include "abstractdatamodel.h"

namespace CTL {

/*!
 * \brief The AbstractXraySpectrumModel class is the abstract base class for data models
 * representing X-ray spectra.
 *
 * This class serves as base class for all data models that hold X-ray spectra. Values sampled
 * from such models represent the (relative) photon count emitted at the energy the value has been
 * sampled from - e.g., valueAt(45.0f) would return the contribution at 45 keV. Note that these are
 * differential quantities. The bin integral values (see binIntegral()) return actual photon counts
 * within the given energy range of the bin. Sub-classes must provide an implementation of
 * binIntegral(); in addition to the plain valueAt() method.
 *
 * This class introduces an additional \c float member that is used to store the energy that the
 * data model currently describes. This allows for having a single data model that can provide
 * spectral data for differing device settings (such as varying tube voltage). The energy
 * parameter can be set thorugh setParameter().
 *
 * For details on de-/serializability of sub-classes, please refer to the documentation of
 * AbstractIntegrableDataModel.
 */
class AbstractXraySpectrumModel : public AbstractIntegrableDataModel
{
public:
    public:virtual AbstractXraySpectrumModel* clone() const override = 0;

    void setParameter(const QVariant& parameter) override;
    QVariant parameter() const override;

protected:
    float _energy = 0.0f; //!< Control parameter of device setting (usually tube voltage).
};

} // namespace CTL

#endif // CTL_ABSTRACTXRAYSPECTRUMMODEL_H
