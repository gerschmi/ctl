#include "datamodels2d.h"

#include "mat/pi.h"
#include "models/abstractdatamodel.h"
#include "processing/range.h"

#include <cmath>
#include <QDebug>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(ConstantModel2D)
DECLARE_SERIALIZABLE_TYPE(GaussianModel2D)
DECLARE_SERIALIZABLE_TYPE(RectModel2D)
DECLARE_SERIALIZABLE_TYPE(SeparableProductModel)

// ###############
// ConstantModel2D

/*!
 * \brief Constructs a ConstantModel2D; a model returning a constant value independent of the input.
 *
 * The created model will always return the value \a amplitude.
 */
ConstantModel2D::ConstantModel2D(float amplitude)
    : _amplitude(amplitude)
{
    setName(QStringLiteral("ConstantModel2D"));
}

/*!
 * \brief Always returns the constant specified for this instance.
 *
 * Note that the constant may be set either during construction of via setParameter().
 */
float ConstantModel2D::valueAt(float, float) const { return _amplitude; }

// base class documentation
AbstractDataModel2D* ConstantModel2D::clone() const { return new ConstantModel2D(*this); }

// base class documentation
QVariant ConstantModel2D::parameter() const { return QVariantMap{{{"amplitude", _amplitude}}}; }

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * The value passed to \a parameter must be a QVariantMap containing the following
 * (key, value)-pair:
 *
 * -("amplitude", [\c float] \a amplitude).
 */
void ConstantModel2D::setParameter(const QVariant& parameter)
{
    const auto parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("amplitude")))
        _amplitude = parMap.value(QStringLiteral("amplitude")).toFloat();
}

// base class documentation
QVariant ConstantModel2D::toVariant() const
{
    QVariantMap ret = AbstractDataModel2D::toVariant().toMap();

    ret.insert(QStringLiteral("#"), "ConstantModel2D");

    return ret;
}


// ###############
// GaussianModel2D

/*!
 * \brief Constructs a GaussianModel2D; a model representing a two-dimensional Gaussian curve.
 *
 * This creates a model representing a two-dimensional Gaussian function specified by \a amplitude,
 * \a meanX, \a meanY, \a stdX, \a stdY, and \a correlation.
 *
 * Note that the input to both standard deviation parameters (\a stdX, \a stdY) must be strictly
 * positive, i.e. \a stdX > 0, \a stdY > 0; and \a correlation must be between (excluding the
 * borders) -1 and 1, i.e. -1.0 < \a correlation < 1.0.
 *
 * Throws an std::domain_error for invalid input.
 *
 * \sa valueAt()
 */
GaussianModel2D::GaussianModel2D(float amplitude,
                                 float meanX, float meanY,
                                 float stdX, float stdY, float correlation)
    : _ampl(amplitude)
    , _meanX(meanX)
    , _meanY(meanY)
    , _stdX(stdX)
    , _stdY(stdY)
    , _corr(correlation)
{

    if(stdX <= 0.0f || stdY <= 0.0f)
        throw std::domain_error("GaussianModel2D: Invalid value for standard deviation "
                                "(requires: std > 0.0).");

    if(correlation <= -1.0f || correlation >= 1.0f)
        throw std::domain_error("GaussianModel2D: Invalid correlation value "
                                "(requires: -1.0 < correlation < 1.0).");

    setName(QStringLiteral("GaussianModel2D"));
}

/*!
 * \brief Constructs a GaussianModel2D; a model representing a two-dimensional Gaussian curve.
 *
 * This creates a model representing a two-dimensional Gaussian function specified by \a amplitude,
 * \a mean, \a std, and \a correlation.
 *
 * The passed parameters \a mean and \a std will be uses for both dimensions. Same as
 * GaussianModel2D(amplitude, mean, mean, std, std, correlation).
 *
 * Note that the input to \a std must be strictly positive, i.e. \a std > 0; and \a correlation must
 * be between (excluding the borders) -1 and 1, i.e. -1.0 < \a correlation < 1.0.
 *
 * Throws an std::domain_error for invalid input.
 *
 * \sa valueAt()
 */
GaussianModel2D::GaussianModel2D(float amplitude, float mean, float std, float correlation)
    : GaussianModel2D(amplitude, mean, mean, std, std, correlation)
{
}

/*!
 * \brief Returns the value from the model at position (\a x, \a y).
 *
 * With the defining parameters of the Gaussian curve according to the constructor (see
 * GaussianModel2D()):
 *
 * - \a amplitude: \f$ A \f$
 * - \a meanX: \f$ \mu_x \f$
 * - \a meanY: \f$ \mu_y \f$
 * - \a stdX: \f$ \sigma_x \f$
 * - \a stdY: \f$ \sigma_y \f$
 * - \a correlation: \f$ c \f$
 *
 * the resulting value is given by follwoing relation:
 *
 * \f$
 * f(x)= \frac{A}{2\pi \sigma_x \sigma_y \sqrt{1-c^2}} \cdot
 * \exp\left[-\frac{1}{2 (1-c^2)} \cdot \left( \frac{(x-\mu_x)^2}{\sigma_x^2}
 * + \frac{(y-\mu_y)^2}{\sigma_y^2}
 * - \frac{2c \cdot (x-\mu_x)(y-\mu_y)}{\sigma_x \sigma_y} \right) \right]
 * \f$
 */
float GaussianModel2D::valueAt(float x, float y) const
{
    const auto xTerm = std::pow(x - _meanX, 2.0f) / std::pow(_stdX, 2.0f);
    const auto yTerm = std::pow(y - _meanY, 2.0f) / std::pow(_stdY, 2.0f);
    const auto corrTerm = - 2.0f * _corr * (x - _meanX) * (y - _meanY) / (_stdX * _stdY);
    const auto scaleFac = - 0.5f / (1.0f - std::pow(_corr, 2.0f));
    const auto normal   = 1.0f / (2.0f * float(PI) * _stdX * _stdY * std::sqrt(1.0f - std::pow(_corr, 2.0f)));

    return _ampl * normal * std::exp(scaleFac * (xTerm + yTerm + corrTerm));
}

// base class documentation
AbstractDataModel2D* GaussianModel2D::clone() const { return new GaussianModel2D(*this); }

// base class documentation
QVariant GaussianModel2D::toVariant() const
{
    QVariantMap ret = AbstractDataModel2D::toVariant().toMap();

    ret.insert(QStringLiteral("#"), "GaussianModel2D");

    return ret;
}

// base class documentation
QVariant GaussianModel2D::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("amplitude"), _ampl);
    ret.insert(QStringLiteral("meanX"), _meanX);
    ret.insert(QStringLiteral("meanY"), _meanY);
    ret.insert(QStringLiteral("stdX"), _stdX);
    ret.insert(QStringLiteral("stdY"), _stdY);
    ret.insert(QStringLiteral("correlation"), _corr);

    return ret;
}

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * The value passed to \a parameter must be a QVariantMap containing one or multiple of the
 * following (key, value)-pairs:
 *
 * -("amplitude", [\c float] \a amplitude),
 * -("meanX", [\c float] \a meanX),
 * -("meanY", [\c float] \a meanY),
 * -("stdX", [\c float] \a stdX),
 * -("stdY", [\c float] \a stdY),
 * -("correlation", [\c float] \a correlation).
 *
 * \a stdX and \a stdY must be strictly positive, i.e. i.e. \a stdX > 0, \a stdY > 0; for other
 * input, a warning is generated and a default value of 1.0 will be set.
 * \a correlation must be between (excluding the borders) -1 and 1, i.e. -1.0 < \a correlation
 * < 1.0; for other input, a warning is generated and a default value of 0.0 will be set.
 *
 * Example: setting mean in y direction to 3, amplitude to 2.5 and a correlation of 0.5
 * \code
 * auto gaussian = std::make_shared<GaussianModel2D>();
 * gaussian->setParameter(QVariantMap{ { "meanY", 3.0f },
 *                                     { "amplitude", 2.5f },
 *                                     { "correlation", 0.5f } } );
 * \endcode
 */
void GaussianModel2D::setParameter(const QVariant& parameter)
{
    const auto varMap = parameter.toMap();

    if(varMap.contains(QStringLiteral("amplitude")))
        _ampl  = varMap.value(QStringLiteral("amplitude")).toFloat();
    if(varMap.contains(QStringLiteral("meanX")))
        _meanX = varMap.value(QStringLiteral("meanX")).toFloat();
    if(varMap.contains(QStringLiteral("meanY")))
        _meanY = varMap.value(QStringLiteral("meanY")).toFloat();
    if(varMap.contains(QStringLiteral("stdX")))
        setSaveStdX(varMap.value(QStringLiteral("stdX")).toFloat());
    if(varMap.contains(QStringLiteral("stdY")))
        setSaveStdY(varMap.value(QStringLiteral("stdY")).toFloat());
    if(varMap.contains(QStringLiteral("correlation")))
        setSaveCorr(varMap.value(QStringLiteral("correlation")).toFloat());
}

/*!
 * \brief Helper function to set correlation value within valid range.
 */
void GaussianModel2D::setSaveCorr(float corr)
{
    if(corr <= -1.0f || corr >= 1.0f)
    {
        qWarning() << "GaussianModel2D: Trying to set invalid correlation value "
                      "(requested: " + QString::number(corr) + "; requires: -1.0 < correlation < 1.0)."
                      " Value will be ignored and set to default (corr = 0.0).";
        corr = 0.0f;
    }

    _corr = corr;
}

/*!
 * \brief Helper function to set standard deviation value within valid range.
 */
void GaussianModel2D::setSaveStdX(float std)
{
    if(std <= 0.0f)
    {
        qWarning() << "GaussianModel2D: Trying to set invalid standard deviation value in x "
                      "(requested: " + QString::number(std) + "; requires: stdX > 0.0). "
                      "Value will be ignored and set to default (stdX = 1.0).";
        std = 1.0f;
    }

    _stdX = std;
}

/*!
 * \brief Helper function to set standard deviation value within valid range.
 */
void GaussianModel2D::setSaveStdY(float std)
{
    if(std <= 0.0f)
    {
        qWarning() << "GaussianModel2D: Trying to set invalid standard deviation value in y "
                      "(requested: " + QString::number(std) + "; requires: stdY > 0.0). "
                      "Value will be ignored and set to default (stdY = 1.0).";
        std = 1.0f;
    }

    _stdY = std;
}


// ###############
//   RectModel2D

/*!
 * \brief Constructs a RectModel2D; a model representing a two-dimensional rectangular function.
 *
 * This creates a model representing a two-dimensional rectangular function specified by
 * \a amplitude and \a rect.
 *
 * The \a rect defines the value range in which the function value will be \a amplitude; it is zero
 * everywhere outside the \a rect.
 */
RectModel2D::RectModel2D(float amplitude, const QRectF& rect)
    : _amplitude(amplitude)
    , _rect(rect)
{
    setName(QStringLiteral("RectModel2D"));
}

/*!
 * \brief Constructs a RectModel2D; a model representing a two-dimensional rectangular function.
 *
 * This creates a model representing a two-dimensional rectangular function specified by
 * \a amplitude, \a xFrom, \a xTo, \a yFrom,  and \a yTo.
 *
 * \a xFrom, \a xTo, \a yFrom,  and \a yTo define the value range in which the function value will
 * be \a amplitude; it is zero everywhere outside this range.
 *
 * Same as RectModel2D(amplitude, QRectF(QPointF{ xFrom, yFrom }, QPointF{ xTo, yTo })).
 */
RectModel2D::RectModel2D(float amplitude, float xFrom, float xTo, float yFrom, float yTo)
    : RectModel2D(amplitude, QRectF(QPointF{ xFrom, yFrom }, QPointF{ xTo, yTo }))
{
}

/*!
 * \brief Constructs a RectModel2D; a model representing a two-dimensional rectangular function.
 *
 * This creates a model representing a two-dimensional rectangular function specified by
 * \a amplitude, \a xRange, and \a yRange.
 *
 * \a xRange and \a yRange define the value range in which the function value will be \a amplitude;
 * it is zero everywhere outside these ranges.
 *
 * Same as RectModel2D(amplitude, QRectF(QPointF{ xRange.start(), yRange.start() },
 *  QPointF{ xRange.end(), yRange.end() })).
 */
RectModel2D::RectModel2D(float amplitude, const SamplingRange& xRange, const SamplingRange& yRange)
 : RectModel2D(amplitude, QRectF(QPointF{ xRange.start(), yRange.start() }, QPointF{ xRange.end(), yRange.end() }))
{
}

/*!
 * \brief Returns the value from the model at position (\a x, \a y).
 *
 * Returns the amplitude value of this instance if (\a x, \a y) lies inside the specified rectangle;
 * returns zero if (\a x, \a y) is outside the rectangle.
 */
float RectModel2D::valueAt(float x, float y) const
{
    return _amplitude * static_cast<float>(_rect.contains(x, y));
}

// base class documentation
AbstractDataModel2D* RectModel2D::clone() const { return new RectModel2D(*this); }

// base class documentation
QVariant RectModel2D::parameter() const
{
    QVariantMap ret;
    ret.insert(QStringLiteral("amplitude"), _amplitude);
    ret.insert(QStringLiteral("xFrom"), _rect.left());
    ret.insert(QStringLiteral("xTo"), _rect.right());
    ret.insert(QStringLiteral("yFrom"), _rect.top());
    ret.insert(QStringLiteral("yTo"), _rect.bottom());

    return ret;
}

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * The value passed to \a parameter must be a QVariantMap containing one or multiple of the
 * following (key, value)-pairs:
 *
 * -("amplitude", [\c float] \a amplitude),
 * -("xFrom", [\c float] \a xFrom),
 * -("xTo", [\c float] \a xTo),
 * -("yFrom", [\c float] \a yFrom)
 * -("yTo", [\c float] \a yTo).
 *
 * Example: setting the end point of the rectangle to (2.0, 3.0)
 * \code
 * auto rect = std::make_shared<RectModel2D>();
 * rect->setParameter(QVariantMap{ { "xTo", 2.0f },
 *                                 { "yTo", 3.0f } } );
 * \endcode
 */
void RectModel2D::setParameter(const QVariant &parameter)
{
    const auto varMap = parameter.toMap();

    if(varMap.contains(QStringLiteral("amplitude")))
        _amplitude  = varMap.value(QStringLiteral("amplitude")).toFloat();
    if(varMap.contains(QStringLiteral("xFrom")))
        _rect.setLeft(varMap.value(QStringLiteral("xFrom")).toFloat());
    if(varMap.contains(QStringLiteral("xTo")))
        _rect.setRight(varMap.value(QStringLiteral("xTo")).toFloat());
    if(varMap.contains(QStringLiteral("yFrom")))
        _rect.setTop(varMap.value(QStringLiteral("yFrom")).toFloat());
    if(varMap.contains(QStringLiteral("yTo")))
        _rect.setBottom(varMap.value(QStringLiteral("yTo")).toFloat());

}

// base class documentation
QVariant RectModel2D::toVariant() const
{
    QVariantMap ret = AbstractDataModel2D::toVariant().toMap();

    ret.insert(QStringLiteral("#"), "RectModel2D");

    return ret;
}

/*!
 * \brief Constructs a SeparableProductModel; a model representing the product of two
 * one-dimensional models.
 *
 * This creates a model that is defined by the two one-dimensional models \a xModel and \a yModel
 * that describe the function individually in each dimension.
 *
 * This model maps input in each dimension according to the corresponding model and returns the
 * product of both values. This can be used to construct models that have no inter-dependency
 * between both dimensions (i.e. they are separable).
 *
 * Example: create a product of a 1D Gaussian (in x direction) and a rect function (in y direction)
 * \code
 *  auto gaussian = std::make_shared<GaussianModel1D>();
 *  auto rect     = std::make_shared<RectFunctionModel>();
 *
 *  auto product  = std::make_shared<SeparableProductModel>(gaussian, rect);
 * \endcode
 */
SeparableProductModel::SeparableProductModel(std::shared_ptr<AbstractDataModel> xModel,
                                             std::shared_ptr<AbstractDataModel> yModel)
    : _xModel(std::move(xModel))
    , _yModel(std::move(yModel))
{
    if(!_xModel || !_yModel)
        throw std::runtime_error("SeparableProductModel: Unable to construct data model."
                                 "At least one of the input models is nullptr.");

    setName(QStringLiteral("SeparableProductModel"));
}

/*!
 * \brief Returns a (modifiable) reference to the model in x-dimension.
 *
 * This can be used to change the parameters of the x model.
 */
AbstractDataModel& SeparableProductModel::xModel() { return *_xModel; }

/*!
 * \brief Returns a (modifiable) reference to the model in y-dimension.
 *
 * This can be used to change the parameters of the y model.
 */
AbstractDataModel& SeparableProductModel::yModel() { return *_yModel; }

/*!
 * \brief Returns the value from the model at position (\a x, \a y).
 *
 * Returns the product of the value returned by the individual models given their corresponding
 * input values.
 *
 * Same as: xModel().valueAt(x) * yModel().valueAt(y)
 */
float SeparableProductModel::valueAt(float x, float y) const
{
    return _xModel->valueAt(x) * _yModel->valueAt(y);
}

// base class documentation
AbstractDataModel2D* SeparableProductModel::clone() const
{
    return new SeparableProductModel(*this);
}

// base class documentation
QVariant SeparableProductModel::parameter() const
{
    QVariantMap ret;

    ret.insert(QStringLiteral("x model"), _xModel->toVariant());
    ret.insert(QStringLiteral("y model"), _yModel->toVariant());

    return ret;
}

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * The value passed to \a parameter must be a QVariantMap containing two (key, value)-pairs:
 *
 * -("x model", [\c QVariant] \a variant of xModel),
 * -("y model", [\c QVariant] \a variant of yModel).
 *
 * Note that the QVariants for \a xModel and \a yModel must be instances carrying the full
 * serialization information (such as it would be returned, e.g., from xModel().toVariant()).
 *
 * Direct use of this method is not recommended. To set individual parameters of the two underlying
 * data models, consider using their individual setParameter() methods.
 *
 * Example: create a product of a 1D Gaussian and a rect function; set the standard deviation of the
 * Gaussian to 4.0 and its amplitude to 10.0.
 * \code
 *  auto gaussian = std::make_shared<GaussianModel1D>();
 *  auto rect     = std::make_shared<RectFunctionModel>();
 *  auto product  = std::make_shared<SeparableProductModel>(gaussian, rect);
 *
 *  product->xModel().setParameter(QVariantMap{ { "std", 4.0f },
 *                                              { "amplitude", 10.0f } } );
 *
 *  // note that in this situation, using the shared_ptr directly leads to the same result
 *  gaussian->setParameter(QVariantMap{ { "std", 4.0f },
 *                                      { "amplitude", 10.0f } } );
 * \endcode
 */
void SeparableProductModel::setParameter(const QVariant& parameter)
{
    QVariantMap parMap = parameter.toMap();

    auto xModel = SerializationHelper::parseDataModel(parMap.value(QStringLiteral("x model")));
    auto yModel = SerializationHelper::parseDataModel(parMap.value(QStringLiteral("y model")));

    if(!xModel || !yModel)
        throw std::runtime_error("SeparableProductModel::setParameter: Unable to set data models "
                                 "from passed parameters. At least one of the models could not be "
                                 "constructed from the parameters.");
    _xModel.reset(xModel);
    _yModel.reset(yModel);
}

// base class documentation
QVariant SeparableProductModel::toVariant() const
{
    QVariantMap ret = AbstractDataModel2D::toVariant().toMap();

    ret.insert(QStringLiteral("#"), "SeparableProductModel");

    return ret;
}


} // namespace CTL
