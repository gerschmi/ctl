#include "abstractdatamodel2d.h"

#include "datamodeloperations.h"
#include "img/chunk2d.h"
#include "processing/range.h"

#include <QDebug>

namespace CTL {

/*!
 * Returns the parameters of this model as a QVariant.
 *
 * Default implementation returns an empty QVariant. Override in custom classes to encode all relevant
 * information from the class' members. The returned QVariant must contain all information to fully
 * specify the objects state. In particular, calling setParameter(parameter()) should not change the
 * state of an object.
 */
QVariant AbstractDataModel2D::parameter() const
{
    return QVariant();
}

/*!
 * Sets the parameters of this model based on data held in \a parameter.
 *
 * Default implementation does nothing. Override in custom classes to extract all relevant
 * information from \a parameter and set class members accordingly. A call to this method
 * (given an appropriate input \a parameter) should prepare the full state of the object.
 * In particular, calling setParameter(parameter()) should not change the state of an object.
 */
void AbstractDataModel2D::setParameter(const QVariant&)
{
}

/*!
 * Sets the parameter with tag \a name to \a value, by passing a QVariantMap containing only the
 * \a name, \a value pair as its sole entry to setParameter(const QVariant&).
 *
 * This method is only useful if setParameter(const QVariant&) is implemented in a way that it
 * allows for individual parameters to be set if the QVariant passed to it does only contain a
 * selection of parameters. When overriding setParameter(const QVariant&) in custom classes,
 * this method will be shadowed. In case you implemented setParameter(const QVariant&) in the way
 * described above (i.e. such that setting individual parameters is possible), re-enable this method
 * in the sub-class via
 * \code
 * using AbstractDataModel2D::setParameter;
 * \endcode.
 */
void AbstractDataModel2D::setParameter(const QString& name, const QVariant& value)
{
    setParameter(QVariantMap{ { { name, value } } });
}

void AbstractDataModel2D::fromVariant(const QVariant& variant)
{
    auto map = variant.toMap();
    if(map.value(QStringLiteral("type-id")).toInt() != type())
    {
        qWarning() << QString(typeid(*this).name())
                      + "::fromVariant: Could not construct instance! "
                        "reason: incompatible variant passed";
        return;
    }

    _name = map.value(QStringLiteral("name")).toString();
    setParameter(map.value(QStringLiteral("parameters")));
}

QVariant AbstractDataModel2D::toVariant() const
{
    QVariantMap ret = SerializationInterface::toVariant().toMap();

    QString nameString = _name.isEmpty() ? typeid(*this).name()
                                         : _name;
    ret.insert(QStringLiteral("name"), nameString);
    ret.insert(QStringLiteral("parameters"), parameter());

    return ret;
}

/*!
 * Samples values from this model on an equidistantly-spaced 2D grid and returns data as a Chunk2D.
 * The grid will cover the range specified by \a xRange and \a yRange with \a nbSamplesX and
 * \a nbSamplesY sampling points, respectively.
 */
Chunk2D<float>
AbstractDataModel2D::sampleChunk(const SamplingRange& xRange, const SamplingRange& yRange,
                                 uint nbSamplesX, uint nbSamplesY) const
{
    Chunk2D<float> ret(nbSamplesX, nbSamplesY);
    ret.allocateMemory();

    const auto xSamples = xRange.linspace(nbSamplesX);
    const auto ySamples = yRange.linspace(nbSamplesY);

    for(uint x = 0; x < nbSamplesX; ++x)
        for(uint y = 0; y < nbSamplesY; ++y)
            ret(x, y) = valueAt(xSamples[x], ySamples[y]);

    return ret;
}

// doc in header file
std::shared_ptr<AbstractDataModel2D> operator+(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs)
{
    return std::make_shared<DataModel2DAdd>(std::move(lhs), std::move(rhs));
}

// doc in header file
std::shared_ptr<AbstractDataModel2D> operator-(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs)
{
    return std::make_shared<DataModel2DSub>(std::move(lhs), std::move(rhs));
}

// doc in header file
std::shared_ptr<AbstractDataModel2D> operator*(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs)
{
    return std::make_shared<DataModel2DMul>(std::move(lhs), std::move(rhs));
}

// doc in header file
std::shared_ptr<AbstractDataModel2D> operator/(std::shared_ptr<AbstractDataModel2D> lhs,
                                               std::shared_ptr<AbstractDataModel2D> rhs)
{
    return std::make_shared<DataModel2DDiv>(std::move(lhs), std::move(rhs));
}

// doc in header file
std::shared_ptr<AbstractDataModel2D>& operator+=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs)
{
    lhs = lhs + rhs;
    return lhs;
}

// doc in header file
std::shared_ptr<AbstractDataModel2D>& operator-=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs)
{
    lhs = lhs - rhs;
    return lhs;
}

// doc in header file
std::shared_ptr<AbstractDataModel2D>& operator*=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs)
{
    lhs = lhs * rhs;
    return lhs;
}

// doc in header file
std::shared_ptr<AbstractDataModel2D>& operator/=(std::shared_ptr<AbstractDataModel2D>& lhs,
                                                 const std::shared_ptr<AbstractDataModel2D>& rhs)
{
    lhs = lhs / rhs;
    return lhs;
}

} // namespace CTL
