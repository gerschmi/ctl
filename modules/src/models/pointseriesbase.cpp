#include "pointseriesbase.h"
#include "processing/range.h"

namespace CTL {

/*!
 * Constructs a PointSeriesBase and sets the data to \a pointSeries (by copying).
 *
 * Note that, once created, data cannot be modified through the public interface of this class.
 */
PointSeriesBase::PointSeriesBase(const QList<QPointF>& pointSeries)
    : _data(pointSeries)
{
}

/*!
 * Constructs a PointSeriesBase and sets the data to \a pointSeries (by moving).
 *
 * Note that, once created, data cannot be modified through the public interface of this class.
 */
PointSeriesBase::PointSeriesBase(QList<QPointF>&& pointSeries)
    : _data(std::move(pointSeries))
{
}

/*!
 * Returns a reference-to-const to the data managed by this instance.
 */
const QList<QPointF>& PointSeriesBase::data() const
{
    return _data;
}

/*!
 * Returns the maximum of the values (i.e. 'y' part of the tuples) in this series.
 */
float PointSeriesBase::max() const
{
    auto maxEl = std::max_element(_data.begin(), _data.end(),
                                  [] (const QPointF& a, const QPointF& b) { return a.y()<b.y(); });

    return static_cast<float>(maxEl->y());
}

/*!
 * Returns the minimum of the values (i.e. 'y' part of the tuples) in this series.
 */
float PointSeriesBase::min() const
{
    auto minEl = std::min_element(_data.begin(), _data.end(),
                                  [] (const QPointF& a, const QPointF& b) { return a.y()<b.y(); });

    return static_cast<float>(minEl->y());
}

/*!
 * Returns the number of samples (i.e. sampling point / value pairs) in this series.
 */
uint PointSeriesBase::nbSamples() const
{
    return static_cast<uint>(_data.size());
}

/*!
 * Returns the number of samples (i.e. sampling point / value pairs) in this series.
 *
 * Same as nbSamples().
 */
uint PointSeriesBase::size() const
{
    return static_cast<uint>(_data.size());
}

/*!
 * Returns the sampling point (i.e. 'x' part) of the sample with index \a sampleNb in this series.
 */
float PointSeriesBase::samplingPoint(uint sampleNb) const
{
    return _data.at(sampleNb).x();
}

/*!
 * Returns all sampling points (i.e. 'x' part) in this series as an std::vector.
 *
 * Preserves the original order of the points.
 */
std::vector<float> PointSeriesBase::samplingPoints() const
{
    std::vector<float> ret(nbSamples());
    std::transform(_data.begin(), _data.end(), ret.begin(),
                   [](const QPointF& pt) -> float { return pt.x(); });
    return ret;
}

/*!
 * Returns the range (smallest to highest sampling point) covered by the sampling points (i.e. 'x'
 * part) in this series.
 */
Range<float> PointSeriesBase::samplingRange() const
{
    static const auto compareSamplingPt = [] (const QPointF& a, const QPointF& b)
                                          { return a.x()<b.x(); };

    auto minMaxEl = std::minmax_element(_data.begin(), _data.end(), compareSamplingPt);

    return Range<float>(minMaxEl.first->x(), minMaxEl.second->x());
}

/*!
 * Returns the sum over all values in the series.
 */
float PointSeriesBase::sum() const
{
    return std::accumulate(_data.constBegin(), _data.constEnd(), 0.0f,
                           [](float val, const QPointF& p2) { return val + float(p2.y()); });
}

/*!
 * Returns the weighted sum of all values in the series. Each value in the series is multiplied by
 * the corresponding weight factor from \a weights within summation. The weight vector must contain
 * at least as many elements as there are samples in the series.
 */
float PointSeriesBase::sum(const std::vector<float> &weights) const
{
    Q_ASSERT(weights.size() >= nbSamples());

    return std::inner_product(_data.constBegin(), _data.constEnd(), weights.cbegin(), 0.0f,
                              [](float a, float b) { return a + b; },
                              [](const QPointF& p, float w) { return float(p.y()) * w; });
}

/*!
 * Returns the value (i.e. 'y' part) of the sample with index \a sampleNb in this series.
 */
float PointSeriesBase::value(uint sampleNb) const
{
    return _data.at(sampleNb).y();
}

/*!
 * Returns all values (i.e. 'y' part) in this series as an std::vector.
 *
 * Preserves the original order of the points.
 */
std::vector<float> PointSeriesBase::values() const
{
    std::vector<float> ret(nbSamples());
    std::transform(_data.begin(), _data.end(), ret.begin(),
                   [](const QPointF& pt) -> float { return pt.y(); });
    return ret;
}

/*!
 * Returns the range (smallest to highest value) covered by the values (i.e. 'y' part) in this
 * series.
 *
 * Same as \code Range<float>(min(), max()) \endcode, but potentially more efficient.
 */
Range<float> PointSeriesBase::valueRange() const
{
    static const auto compareValue = [] (const QPointF& a, const QPointF& b)
                                     { return a.y()<b.y(); };

    auto minMaxEl = std::minmax_element(_data.begin(), _data.end(), compareValue);

    return Range<float>(minMaxEl.first->y(), minMaxEl.second->y());
}

/*!
 * Alias for sum(const std::vector<float>&), same as \code sum(weights) \endcode.
 */
float PointSeriesBase::weightedSum(const std::vector<float>& weights) const
{
    return sum(weights);
}

/*!
 * Normalizes the series by dividing all values (i.e. 'y' part) by the maximum absolute value.
 *
 * Example:
 * \code
 * auto x = std::vector<float>(6);
 * std::iota(x.begin(), x.end(), 0.0f);
 * auto y = std::vector<float>(6);
 * std::iota(y.begin(), y.end(), -3.0f);
 *
 * auto series = XYDataSeries(x, y);
 *
 * qInfo() << series.values();
 * // output: std::vector(-3, -2, -1, 0, 1, 2)
 *
 * series.normalizeByMaxAbsVal();
 *
 * qInfo() << series.values();
 * // output: std::vector(-1, -0.666667, -0.333333, 0, 0.333333, 0.666667)
 */
void PointSeriesBase::normalizeByMaxAbsVal()
{
    auto maxEl = std::max_element(_data.begin(), _data.end(),
                                  [] (const QPointF& a, const QPointF& b) {
                                  return qAbs(a.y()) < qAbs(b.y()); });
    scaleValues(1.0f / qAbs(maxEl->y()));
}

/*!
 * Normalizes the series by dividing all values (i.e. 'y' part) by the maximum value.
 *
 * Same as \code scaleValues(1.0f / max()) \endcode.
 *
 * Example:
 * \code
 * auto x = std::vector<float>(6);
 * std::iota(x.begin(), x.end(), 0.0f);
 * auto y = std::vector<float>(6);
 * std::iota(y.begin(), y.end(), -3.0f);
 *
 * auto series = XYDataSeries(x, y);
 *
 * qInfo() << series.values();
 * // output: std::vector(-3, -2, -1, 0, 1, 2)
 *
 * series.normalizeByMaxVal();
 *
 * qInfo() << series.values();
 * // output: std::vector(-1.5, -1, -0.5, 0, 0.5, 1)
 */
void PointSeriesBase::normalizeByMaxVal()
{
    scaleValues(1.0f / max());
}

/*!
 * Multiplies all values (i.e. 'y' part) in the series by \a factor.
 *
 * Example:
 * \code
 * auto iotaVec = std::vector<float>(5);
 * std::iota(iotaVec.begin(), iotaVec.end(), 0.0f);
 *
 * auto series = XYDataSeries(iotaVec, iotaVec);
 *
 * qInfo() << series.data();
 * // output: (QPointF(0,0), QPointF(1,1), QPointF(2,2), QPointF(3,3), QPointF(4,4))
 *
 * series.scaleValues(2.0f);
 *
 * qInfo() << series.data();
 * // output: (QPointF(0,0), QPointF(1,2), QPointF(2,4), QPointF(3,6), QPointF(4,8))
 * \endcode
 */
void PointSeriesBase::scaleValues(float factor)
{
    for(auto& pt : _data)
        pt.ry() *= factor;
}

} // namespace CTL
