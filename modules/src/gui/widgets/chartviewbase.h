#ifndef CTL_CHARTVIEWBASE_H
#define CTL_CHARTVIEWBASE_H

#include "qtchartsnamespace.h"
#include <QChartView>
#include <QLineSeries>

namespace CTL {
namespace gui {

/*!
 * \class ChartViewBase
 *
 * \brief The ChartViewBase class is the base class for plot-like visualizers in the CTL.
 *
 * This class is used as base class for LineSeriesView and IntervalSeriesView. Refer to the
 * corresponding documentations of these classes for details on usage.
 */

class ChartViewBase : public QChartView
{
    Q_OBJECT
public:
    QImage image(const QSize& renderSize = QSize());

public Q_SLOTS:
    void autoRange();
    bool save(const QString& fileName);
    void saveDialog();
    void setLabelX(const QString& label);
    void setLabelY(const QString& label);
    void setLogAxisY(bool enabled);
    void setOverRangeY(bool enabled);
    void setRangeX(double from, double to);
    void setRangeY(double from, double to);
    void setUseNiceX(bool enabled);
    void toggleLinLogY();

protected:
    explicit ChartViewBase(QWidget* parent = nullptr);

    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;

    virtual void copyDataToClipboard() const;
    void mySetAxisX(QAbstractAxis* axisX, QAbstractSeries* series);
    void mySetAxisY(QAbstractAxis* axisY, QAbstractSeries* series);
    QAbstractAxis* myAxisX(QAbstractSeries* series);
    QAbstractAxis* myAxisY(QAbstractSeries* series);
    void setSeriesShow(QAbstractSeries* series, bool shown);
    void switchToLinAxisY();
    void switchToLogAxisY();
    bool yAxisIsLinear() const;

    QChart* _chart;
    QAbstractSeries* _plottableSeries;
    QAbstractSeries* _plottableSeriesLog;
    QLineSeries* _dataSeries;
    QLineSeries* _dataSeriesLog;

private:
    bool _overRangeY = false;
    bool _useNiceX = false;
};


} // namespace gui
} // namespace CTL

#endif // CTL_CHARTVIEWBASE_H
