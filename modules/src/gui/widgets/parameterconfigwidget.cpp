#include "parameterconfigwidget.h"

#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>

namespace CTL {
namespace gui {

ParameterConfigWidget::ParameterConfigWidget(QWidget* parent)
    : QWidget(parent)
    , _layout(new QGridLayout)
{
    this->setLayout(_layout);
}

bool ParameterConfigWidget::isEmpty() const
{
    return !static_cast<bool>(_layout->count());
}

void ParameterConfigWidget::updateInterface(QVariant templateParameter)
{
    clearLayout();

    QVariantMap dataMap = templateParameter.toMap();

    int row = 0;
    QVariantMap::const_iterator i = dataMap.constBegin();
    while (i != dataMap.constEnd())
    {
        if(i.value().type() == QVariant::Bool)
        {
            auto widget = new QCheckBox("enable");
            widget->setChecked(i.value().toBool());
            _layout->addWidget(widget, row, 1);
            connect(widget, SIGNAL(toggled(bool)), this, SLOT(somethingChanged()));
        }
        else if(i.value().type() == QVariant::Int || i.value().type() == QVariant::UInt)
        {
            auto widget = new QSpinBox;
            widget->setValue(i.value().toInt());
            widget->setRange(-100000,100000);
            _layout->addWidget(widget, row, 1);
            connect(widget, SIGNAL(valueChanged(int)), this, SLOT(somethingChanged()));
        }
        else if(i.value().type() == QVariant::Double || i.value().type() == 38)
        {
            auto widget = new QDoubleSpinBox;
            widget->setValue(i.value().toDouble());
            widget->setRange(-100000.0,100000.0);
            _layout->addWidget(widget, row, 1);
            connect(widget, SIGNAL(valueChanged(double)), this, SLOT(somethingChanged()));
        }
        else // cannot be represented
        {
            ++i;
            continue;
        }

        _layout->addWidget(new QLabel(i.key()), row, 0);
        ++row;
        ++i;
    }

}

void ParameterConfigWidget::clearLayout()
{
    const auto nbItems = _layout->count();

    for(int item = nbItems-1; item >=0 ; --item)
    {
        QWidget* w = _layout->itemAt(item)->widget();
        if(w != NULL)
        {
            _layout->removeWidget(w);
            delete w;
        }
    }
}

QVariant ParameterConfigWidget::parsedInputWidget(QWidget *widget)
{
    QVariant ret;

    if(dynamic_cast<QCheckBox*>(widget))
        ret = dynamic_cast<QCheckBox*>(widget)->isChecked();
    else if(dynamic_cast<QSpinBox*>(widget))
        ret = dynamic_cast<QSpinBox*>(widget)->value();
    else if(dynamic_cast<QDoubleSpinBox*>(widget))
        ret = dynamic_cast<QDoubleSpinBox*>(widget)->value();

    return ret;
}

void ParameterConfigWidget::somethingChanged()
{
    const auto nbItems = _layout->count();
    const auto nbRows = nbItems / 2;

    QVariantMap map;
    for (int row = 0; row < nbRows; ++row)
    {
        auto name = static_cast<QLabel*>(_layout->itemAtPosition(row, 0)->widget())->text();
        map.insert(name, parsedInputWidget(_layout->itemAtPosition(row, 1)->widget()));
    }

    Q_EMIT parameterChanged(map);
}

} // namespace gui
} // namespace CTL
