#include "voxelvolume.h"
#include "processing/threadpool.h"
#include <cmath>

namespace CTL {

namespace details {
    template <typename T> void grindBall(VoxelVolume<T>& volume, float radius);
    template <typename T> void grindCylinderX(VoxelVolume<T>& volume, float radius);
    template <typename T> void grindCylinderY(VoxelVolume<T>& volume, float radius);
    template <typename T> void grindCylinderZ(VoxelVolume<T>& volume, float radius);
} // namespace details

/*!
 * Constructs a voxelized volume with \a nbVoxels voxels.
 *
 * This constuctor does not allocate memory for the data. To enforce memory allocation,
 * use allocateMemory().
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(const Dimensions& nbVoxels)
    : _dim(nbVoxels)
{
}

/*!
 * Constructs a voxelized volume with [\a nbVoxelX, \a nbVoxelY, \a nbVoxelZ] voxels.
 *
 * This constuctor does not allocate memory for the data. To enforce memory allocation,
 * use allocateMemory().
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(uint nbVoxelX, uint nbVoxelY, uint nbVoxelZ)
    : _dim({ nbVoxelX, nbVoxelY, nbVoxelZ })
{
}

/*!
 * Constructs a voxelized volume with \a nbVoxels voxels. Each voxel has the (physical) dimensions
 * specified by \a voxelSize.
 *
 * This constuctor does not allocate memory for the data. To enforce memory allocation,
 * use allocateMemory().
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(const Dimensions& nbVoxels, const VoxelSize& voxelSize)
    : _dim(nbVoxels)
    , _size(voxelSize)
{
}

/*!
 * Constructs a voxelized volume with [\a nbVoxelX, \a nbVoxelY, \a nbVoxelZ] voxels. Each
 * voxel has the (physical) dimensions \a xSize x \a ySize x \a zSize (in millimeters).
 *
 * This constuctor does not allocate memory for the data. To enforce memory allocation,
 * use allocateMemory().
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(
    uint nbVoxelX, uint nbVoxelY, uint nbVoxelZ, float xSize, float ySize, float zSize)
    : _dim({ nbVoxelX, nbVoxelY, nbVoxelZ })
    , _size({ xSize, ySize, zSize })
{
}

/*!
 * Constructs a voxelized volume with \a nbVoxels voxels and sets its data to \a data.
 *
 * A dimension consistency check is performed to set the data, i.e. the number of elements
 * in \a data need to match the number of voxels in the volume. Throws std::domain_error in
 * case of mismatching dimensions.
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(const Dimensions& nbVoxels, std::vector<T> data)
    : _dim(nbVoxels)
{
    setData(std::move(data));
}

/*!
 * Constructs a voxelized volume with \a nbVoxels voxels and sets its data to \a data. Each voxel
 * has the (physical) dimensions specified by \a voxelSize.
 *
 * A dimension consistency check is performed to set the data, i.e. the number of elements
 * in \a data need to match the number of voxels in the volume. Throws std::domain_error in
 * case of mismatching dimensions.
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(const Dimensions& nbVoxels,
                            const VoxelSize& voxelSize,
                            std::vector<T> data)
    : _dim(nbVoxels)
    , _size(voxelSize)
{
    setData(std::move(data));
}

/*!
 * Constructs a voxelized volume with [\a nbVoxelX, \a nbVoxelY, \a nbVoxelZ] voxels and sets
 * its data to \a data.
 *
 * A dimension consistency check is performed to set the data, i.e. the number of elements
 * in \a data need to match the number of voxels in the volume. Throws std::domain_error in
 * case of mismatching dimensions.
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(uint nbVoxelX, uint nbVoxelY, uint nbVoxelZ, std::vector<T> data)
    : _dim({ nbVoxelX, nbVoxelY, nbVoxelZ })
{
    setData(std::move(data));
}

/*!
 * Constructs a voxelized volume with [\a nbVoxelX, \a nbVoxelY, \a nbVoxelZ] voxels and sets
 * its data to \a data. The voxels have the (physical) dimensions \a xSize x \a ySize x \a zSize
 * (in millimeters).
 *
 * A dimension consistency check is performed to set the data, i.e. the number of elements
 * in \a data need to match the number of voxels in the volume. Throws std::domain_error in
 * case of mismatching dimensions.
 */
template <typename T>
VoxelVolume<T>::VoxelVolume(uint nbVoxelX,
                            uint nbVoxelY,
                            uint nbVoxelZ,
                            float xSize,
                            float ySize,
                            float zSize,
                            std::vector<T> data)
    : _dim({ nbVoxelX, nbVoxelY, nbVoxelZ })
    , _size({ xSize, ySize, zSize })
{
    setData(std::move(data));
}

/*!
 * Constructs a voxelized volume from a stack of slices (vector of Chunk2D). All slices in \a stack
 * will be concatenated in *z*-direction.
 *
 * Note that all slices are required to have identical dimensions. Throws an std::domain_error otherwise.
 * Returns an empty VoxelVolume (all dimensions zero) when an empty stack is passed.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::fromChunk2DStack(const std::vector<Chunk2D<T>>& stack)
{
    // check if stack is empty
    if(stack.empty())
        return { 0, 0, 0 };

    // get dim information from stack
    const auto& firstChunk = stack.front();
    const auto& chunkDim = firstChunk.dimensions();
    const auto chunkElements = firstChunk.nbElements();
    Dimensions volDim = { chunkDim.width, chunkDim.height, uint(stack.size()) };

    // dimension consistency check within the passed stack
    for(auto& chunk : stack)
        if(chunk.dimensions() != chunkDim)
            throw std::domain_error("Chunks in stack have different dimensions");

    // allocate volume object
    VoxelVolume<T> ret(volDim);
    ret.allocateMemory();

    // fill in data -> the data of each chunk will be copied into the volume as a z-slice
    auto retPtr = ret.rawData();
    for(const auto& chunk : stack)
    {
        std::copy_n(chunk.rawData(), chunkElements, retPtr);
        retPtr += chunkElements;
    }

    return ret;
}

/*!
 * Constructs a cubic voxelized volume with \a nbVoxel x \a nbVoxel x \a nbVoxel voxels (voxel
 * dimension: \a voxelSize x \a voxelSize x \a voxelSize), filled with \a fillValue.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::cube(uint nbVoxel, float voxelSize, const T& fillValue)
{
    const Dimensions dim{ nbVoxel, nbVoxel, nbVoxel };

    return { dim, { voxelSize, voxelSize, voxelSize },
             std::vector<T>(dim.totalNbElements(), fillValue) };
}

/*!
 * Constructs a voxelized volume with voxels of isotropic dimensions \a voxelSize (in mm) and fills
 * all voxels inside a ball of radius \a radius (in mm) around the center of the volume with
 * \a fillValue. The voxels surrounding the ball are filled with zeros.
 *
 * The resulting volume will have \f$ \left\lceil 2\cdot radius/voxelSize\right\rceil \f$ voxels in
 * each dimension.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::ball(float radius, float voxelSize, const T& fillValue)
{
    const auto nbVox = static_cast<uint>(std::ceil(2.0f * radius / voxelSize));

    const VoxelVolume<T>::Dimensions volDim{ nbVox, nbVox, nbVox };
    const VoxelVolume<T>::VoxelSize voxSize{ voxelSize, voxelSize, voxelSize };
    VoxelVolume<T> ret{ volDim, voxSize };
    ret.fill(fillValue);

    details::grindBall(ret, radius);

    return ret;
}

/*!
 * Constructs a voxelized volume with voxels of isotropic dimensions \a voxelSize (in mm) and fills
 * all voxels inside a cylinder of radius \a radius (in mm) and height \a height (in mm) aligned
 * with the *x*-axis with* \a fillValue. The voxels surrounding the cylinder are filled with zeros.
 *
 * The resulting volume will have \f$ \left\lceil 2\cdot radius/voxelSize\right\rceil \f$ voxels in
 * *y*- and *z*-dimension and \f$ \left\lceil height/voxelSize\right\rceil \f$ in *x*-direction.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::cylinderX(float radius, float height, float voxelSize, const T& fillValue)
{
    const auto nbVoxCircle = static_cast<uint>(std::ceil(2.0f * radius / voxelSize));
    const auto nbVoxHeight = static_cast<uint>(std::ceil(height / voxelSize));

    const VoxelVolume<T>::Dimensions volDim{ nbVoxHeight, nbVoxCircle, nbVoxCircle };
    const VoxelVolume<T>::VoxelSize voxSize{ voxelSize, voxelSize, voxelSize };
    VoxelVolume<T> ret{ volDim, voxSize };
    ret.fill(fillValue);

    details::grindCylinderX(ret, radius);

    return ret;
}

/*!
 * Constructs a voxelized volume with voxels of isotropic dimensions \a voxelSize (in mm) and fills
 * all voxels inside a cylinder of radius \a radius (in mm) and height \a height (in mm) aligned
 * with the *y*-axis with* \a fillValue. The voxels surrounding the cylinder are filled with zeros.
 *
 * The resulting volume will have \f$ \left\lceil 2\cdot radius/voxelSize\right\rceil \f$ voxels in
 * *x*- and *z*-dimension and \f$ \left\lceil height/voxelSize\right\rceil \f$ in *y*-direction.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::cylinderY(float radius, float height, float voxelSize, const T& fillValue)
{
    const auto nbVoxCircle = static_cast<uint>(std::ceil(2.0f * radius / voxelSize));
    const auto nbVoxHeight = static_cast<uint>(std::ceil(height / voxelSize));

    const VoxelVolume<T>::Dimensions volDim{ nbVoxCircle, nbVoxHeight, nbVoxCircle };
    const VoxelVolume<T>::VoxelSize voxSize{ voxelSize, voxelSize, voxelSize };
    VoxelVolume<T> ret{ volDim, voxSize };
    ret.fill(fillValue);

    details::grindCylinderY(ret, radius);

    return ret;
}

/*!
 * Constructs a voxelized volume with voxels of isotropic dimensions \a voxelSize (in mm) and fills
 * all voxels inside a cylinder of radius \a radius (in mm) and height \a height (in mm) aligned
 * with the *z*-axis with* \a fillValue. The voxels surrounding the cylinder are filled with zeros.
 *
 * The resulting volume will have \f$ \left\lceil 2\cdot radius/voxelSize\right\rceil \f$ voxels in
 * *x*- and *y*-dimension and \f$ \left\lceil height/voxelSize\right\rceil \f$ in *z*-direction.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::cylinderZ(float radius, float height, float voxelSize, const T& fillValue)
{
    const auto nbVoxCircle = static_cast<uint>(std::ceil(2.0f * radius / voxelSize));
    const auto nbVoxHeight = static_cast<uint>(std::ceil(height / voxelSize));

    const VoxelVolume<T>::Dimensions volDim{ nbVoxCircle, nbVoxCircle, nbVoxHeight };
    const VoxelVolume<T>::VoxelSize voxSize{ voxelSize, voxelSize, voxelSize };
    VoxelVolume<T> ret{ volDim, voxSize };
    ret.fill(fillValue);

    details::grindCylinderZ(ret, radius);

    return ret;
}

template<typename T>
typename VoxelVolume<T>::iterator VoxelVolume<T>::begin()
{
    return iterator{ _data.begin(), this };
}

template<typename T>
typename VoxelVolume<T>::iterator VoxelVolume<T>::end()
{
    return iterator{ _data.end(), this };
}

template<typename T>
typename VoxelVolume<T>::const_iterator VoxelVolume<T>::begin() const
{
    return const_iterator{ _data.begin(), this };
}

template<typename T>
typename VoxelVolume<T>::const_iterator VoxelVolume<T>::end() const
{
    return const_iterator{ _data.end(), this };
}

template<typename T>
typename VoxelVolume<T>::const_iterator VoxelVolume<T>::cbegin() const
{
    return const_iterator{ _data.cbegin(), this };
}

template<typename T>
typename VoxelVolume<T>::const_iterator VoxelVolume<T>::cend() const
{
    return const_iterator{ _data.cend(), this };
}

template<typename T>
typename VoxelVolume<T>::reverse_iterator VoxelVolume<T>::rbegin()
{
    return reverse_iterator{ end() };
}

template<typename T>
typename VoxelVolume<T>::reverse_iterator VoxelVolume<T>::rend()
{
    return reverse_iterator{ begin() };
}

template<typename T>
typename VoxelVolume<T>::const_reverse_iterator VoxelVolume<T>::rbegin() const
{
    return const_reverse_iterator{ end() };
}

template<typename T>
typename VoxelVolume<T>::const_reverse_iterator VoxelVolume<T>::rend() const
{
    return const_reverse_iterator{ begin() };
}

template<typename T>
typename VoxelVolume<T>::const_reverse_iterator VoxelVolume<T>::crbegin() const
{
    return const_reverse_iterator{ cend() };
}

template<typename T>
typename VoxelVolume<T>::const_reverse_iterator VoxelVolume<T>::crend() const
{
    return const_reverse_iterator{ cbegin() };
}

/*!
 * Sets an isotropic voxels size of \a isotropicSize (in mm).
 */
template <typename T>
void VoxelVolume<T>::setVoxelSize(float isotropicSize)
{
    _size = { isotropicSize, isotropicSize, isotropicSize };
}

/*!
 * Deletes the data of the voxel volume.
 *
 * \sa allocateMemory()
 */
template <typename T>
void VoxelVolume<T>::freeMemory()
{
    _data.clear();
    _data.shrink_to_fit();
}

/*!
 * Returns a reference to the data at voxel \f$[x,y,z]\f$. Does not perform boundary checks.
 */
template <typename T>
typename std::vector<T>::reference VoxelVolume<T>::operator()(uint x, uint y, uint z)
{
    const auto voxPerSlice = size_t(_dim.x) * size_t(_dim.y);
    const auto voxPerLine = size_t(_dim.x);
    const auto lup = size_t(z) * voxPerSlice + size_t(y) * voxPerLine + size_t(x);

    Q_ASSERT_X(lup < _data.size(), "VoxelVolume::operator(x,y,z)",
               "The index lookup exceeds the internally stored buffer. Have you called `allocateMemory`?");
    return _data[lup];
}

/*!
 * Returns a constant reference to the data at voxel \f$[x,y,z]\f$. Does not perform boundary checks.
 */
template <typename T>
typename std::vector<T>::const_reference VoxelVolume<T>::operator()(uint x, uint y, uint z) const
{
    const auto voxPerSlice = size_t(_dim.x) * size_t(_dim.y);
    const auto voxPerLine = size_t(_dim.x);
    const auto lup = size_t(z) * voxPerSlice + size_t(y) * voxPerLine + size_t(x);

    Q_ASSERT_X(lup < _data.size(), "VoxelVolume::operator(x,y,z)",
               "The index lookup exceeds the internally stored buffer. Have you initialized the volume properly?");
    return _data[lup];
}

/*!
 * Returns a constant reference to the data at voxel index \a index. Does not perform boundary checks.
 */
template<typename T>
typename std::vector<T>::reference VoxelVolume<T>::operator()(const VoxelIndex& index)
{
    return (*this)(index.x(), index.y(), index.z());
}

/*!
 * Returns a constant reference to the data at voxel index \a index. Does not perform boundary checks.
 */
template<typename T>
typename std::vector<T>::const_reference VoxelVolume<T>::operator()(const VoxelIndex& index) const
{
    return (*this)(index.x(), index.y(), index.z());
}

/*!
 * Returns the *yz*-slice of the volume at position \f$x=\f$ \a slice as a Chunk2D.
 */
template <typename T>
Chunk2D<T> VoxelVolume<T>::sliceX(uint slice) const
{
    Q_ASSERT(slice < _dim.x);

    Chunk2D<T> ret(_dim.y, _dim.z);
    ret.allocateMemory();

    const auto voxPerYZSlice = size_t(_dim.y) * size_t(_dim.z);

    std::vector<T> dataVec(voxPerYZSlice);
    // ekel loop
    auto dataIt = dataVec.begin();
    for(auto zIdx = 0u; zIdx < _dim.z; ++zIdx)
        for(auto yIdx = 0u; yIdx < _dim.y; ++yIdx, ++dataIt)
            *dataIt = (*this)(slice, yIdx, zIdx);

    ret.setData(std::move(dataVec));
    return ret;
}

/*!
 * Returns the *xz*-slice of the volume at position \f$y=\f$ \a slice as a Chunk2D.
 */
template <typename T>
Chunk2D<T> VoxelVolume<T>::sliceY(uint slice) const
{
    Q_ASSERT(slice < _dim.y);

    Chunk2D<T> ret(_dim.x, _dim.z);

    const auto voxPerXZSlice = size_t(_dim.x) * size_t(_dim.z);
    const auto voxPerXYSlice = size_t(_dim.x) * size_t(_dim.y);

    std::vector<T> dataVec(voxPerXZSlice);
    // ekel loop
    const auto sliceOffset = size_t(slice) * size_t(_dim.x);
    for(auto zIdx = 0u; zIdx < _dim.z; ++zIdx)
    {
        const auto lup = sliceOffset + size_t(zIdx) * voxPerXYSlice;
        std::copy_n(_data.cbegin() + lup, _dim.x, dataVec.begin() + size_t(zIdx) * size_t(_dim.x));
    }

    ret.setData(std::move(dataVec));
    return ret;
}

/*!
 * Returns the *xy*-slice of the volume at position \f$z=\f$ \a slice as a Chunk2D.
 */
template <typename T>
Chunk2D<T> VoxelVolume<T>::sliceZ(uint slice) const
{
    Q_ASSERT(slice < _dim.z);

    Chunk2D<T> ret(_dim.x, _dim.y);

    const auto voxPerSlice = size_t(_dim.x) * size_t(_dim.y);
    const auto lup = size_t(slice) * voxPerSlice;

    std::vector<T> dataVec(voxPerSlice);
    std::copy_n(_data.cbegin() + lup, voxPerSlice, dataVec.begin());

    ret.setData(std::move(dataVec));
    return ret;
}

/*!
 * Returns a copy of the volume resliced along the *x*-axis. If \a reverse is set \c true,
 * reslicing will be performed in inverse order (i.e. with descending *x* value).
 *
 * The reslicing process switches the orientation of the volume, such that the original axes:
 * [x,y,z] change to [y,z,x].
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::reslicedByX(bool reverse) const
{
    // re-slicing
    std::vector<Chunk2D<T>> chunkStack;
    chunkStack.reserve(_dim.x);
    if(reverse)
        for(auto i = _dim.x - 1u; i != static_cast<uint>(-1); --i)
            chunkStack.push_back(sliceX(i));
    else
        for(auto i = 0u; i < _dim.x; ++i)
            chunkStack.push_back(sliceX(i));

    return VoxelVolume<T>::fromChunk2DStack(chunkStack);
}

/*!
 * Returns a copy of the volume resliced along the *y*-axis. If \a reverse is set \c true,
 * reslicing will be performed in inverse order (i.e. with descending *y* value).
 *
 * The reslicing process switches the orientation of the volume, such that the original axes:
 * [x,y,z] change to [x,z,y].
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::reslicedByY(bool reverse) const
{
    // re-slicing
    std::vector<Chunk2D<T>> chunkStack;
    chunkStack.reserve(_dim.y);
    if(reverse)
        for(auto i = _dim.y - 1u; i != static_cast<uint>(-1); --i)
            chunkStack.push_back(sliceY(i));
    else
        for(auto i = 0u; i < _dim.y; ++i)
            chunkStack.push_back(sliceY(i));

    return VoxelVolume<T>::fromChunk2DStack(chunkStack);
}

/*!
 * Returns a copy of the volume resliced along the *z*-axis. If \a reverse is set \c true,
 * reslicing will be performed in inverse order (i.e. with descending *z* value).
 *
 * Since the volume is sliced in *z*-direction by default, this method will not change the
 * axes when \a reverse is \c false (default value). It simply returns an exact copy of the volume.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::reslicedByZ(bool reverse) const
{
    // re-slicing
    std::vector<Chunk2D<T>> chunkStack;
    chunkStack.reserve(_dim.z);
    if(reverse)
        for(auto i = _dim.z - 1u; i != static_cast<uint>(-1); --i)
            chunkStack.push_back(sliceZ(i));
    else
        return *this;

    return VoxelVolume<T>::fromChunk2DStack(chunkStack);
}

/*!
 * Returns the highest value in this volume. Returns zero for empty VoxelVolume objects.
 */
template <typename T>
T VoxelVolume<T>::max() const
{
    if(allocatedElements() == 0)
        return T(0);

    return *std::max_element(_data.cbegin(), _data.cend());
}

/*!
 * Returns the smallest value in this volume. Returns zero for empty VoxelVolume objects.
 */
template <typename T>
T VoxelVolume<T>::min() const
{
    if(allocatedElements() == 0)
        return T(0);

    return *std::min_element(_data.cbegin(), _data.cend());
}

// operators
/*!
 * Adds the data from \a other to this volume and returns a reference to this instance.
 * Throws an std::domain_error if the dimensions of \a other and this volume do not match.
 *
 * Note that the voxel size will be ignored (i.e. there is no consistency check). Hence, the result
 * will always have the voxel size of this instance.
 */
template <typename T>
VoxelVolume<T>& VoxelVolume<T>::operator+=(const VoxelVolume<T>& other)
{    
    if(dimensions() != other.dimensions())
        throw std::domain_error("Inconsistent dimensions of VoxelVolumes in '+=' operation.");

    const auto dataIt = _data.begin();
    const auto otherDataIt = other._data.cbegin();

    auto threadTask = [dataIt, otherDataIt](size_t begin, size_t end)
    {
        std::transform(dataIt + begin, dataIt + end, otherDataIt + begin, dataIt + begin,
                       std::plus<T>());
    };

    this->parallelExecution(threadTask);

    return *this;
}

/*!
 * Subtracts the data from \a other from this volume and returns a reference to this instance.
 * Throws an std::domain_error if the dimensions of \a other and this volume do not match.
 *
 * Note that the voxel size will be ignored (i.e. there is no consistency check). Hence, the result
 * will always have the voxel size of this instance.
 */
template <typename T>
VoxelVolume<T>& VoxelVolume<T>::operator-=(const VoxelVolume<T>& other)
{
    if(dimensions() != other.dimensions())
        throw std::domain_error("Inconsistent dimensions of VoxelVolumes in '-=' operation.");

    const auto dataIt = _data.begin();
    const auto otherDataIt = other._data.cbegin();

    auto threadTask = [dataIt, otherDataIt](size_t begin, size_t end)
    {
        std::transform(dataIt + begin, dataIt + end, otherDataIt + begin, dataIt + begin,
                       std::minus<T>());
    };

    this->parallelExecution(threadTask);

    return *this;
}

/*!
 * Adds \a additiveShift to all voxel values in this volume and returns a reference to this instance.
 */
template <typename T>
VoxelVolume<T>& VoxelVolume<T>::operator+=(const T& additiveShift)
{
    const auto dataIt = _data.begin();

    auto threadTask = [dataIt, additiveShift](size_t begin, size_t end)
    {
        std::transform(dataIt + begin, dataIt + end, dataIt + begin,
                       [additiveShift](const T& voxVal) { return voxVal + additiveShift; });
    };

    this->parallelExecution(threadTask);

    return *this;
}

/*!
 * Subtracts \a subtractiveShift from all voxel values in this volume and returns a reference to
 * this instance.
 */
template <typename T>
VoxelVolume<T>& VoxelVolume<T>::operator-=(const T& subtractiveShift)
{
    const auto dataIt = _data.begin();

    auto threadTask = [dataIt, subtractiveShift](size_t begin, size_t end)
    {
        std::transform(dataIt + begin, dataIt + end, dataIt + begin,
                       [subtractiveShift](const T& voxVal) { return voxVal - subtractiveShift; });
    };

    this->parallelExecution(threadTask);

    return *this;
}

/*!
 * Multiplies all voxel values in this volume by \a factor and returns a reference to this instance.
 */
template <typename T>
VoxelVolume<T>& VoxelVolume<T>::operator*=(const T& factor)
{
    const auto dataIt = _data.begin();

    auto threadTask = [dataIt, factor](size_t begin, size_t end)
    {
        std::transform(dataIt + begin, dataIt + end, dataIt + begin,
                       [factor](const T& voxVal) { return voxVal * factor; });
    };

    this->parallelExecution(threadTask);

    return *this;
}

/*!
 * Divides all voxel values in this volume by \a divisor and returns a reference to this instance.
 */
template <typename T>
VoxelVolume<T>& VoxelVolume<T>::operator/=(const T& divisor)
{
    const auto dataIt = _data.begin();

    auto threadTask = [dataIt, divisor](size_t begin, size_t end)
    {
        std::transform(dataIt + begin, dataIt + end, dataIt + begin,
                       [divisor](const T& voxVal) { return voxVal / divisor; });
    };

    this->parallelExecution(threadTask);

    return *this;
}

/*!
 * Returns the (element-wise) sum of this volume and \a other.
 * Throws an std::domain_error if the dimensions of \a other and this volume do not match.
 *
 * Note that the voxel size will be ignored (i.e. there is no consistency check). Hence, the result
 * will always have the voxel size of this instance (i.e. left hand side operand).
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::operator+(const VoxelVolume<T>& other) const
{
    if(dimensions() != other.dimensions())
        throw std::domain_error("Inconsistent dimensions of VoxelVolumes in '+' operation.");

    VoxelVolume<T> ret(*this);
    ret += other;
    return ret;
}

/*!
 * Returns the (element-wise) difference between this volume and \a other.
 * Throws an std::domain_error if the dimensions of \a other and this volume do not match.
 *
 * Note that the voxel size will be ignored (i.e. there is no consistency check). Hence, the result
 * will always have the voxel size of this instance (i.e. left hand side operand).
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::operator-(const VoxelVolume<T>& other) const
{
    if(dimensions() != other.dimensions())
        throw std::domain_error("Inconsistent dimensions of VoxelVolumes in '-' operation.");

    VoxelVolume<T> ret(*this);
    ret -= other;
    return ret;
}

/*!
 * Returns a copy of this volume in which \a additiveShift has been added to all voxel values.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::operator+(const T& additiveShift) const
{
    VoxelVolume<T> ret(*this);
    ret += additiveShift;
    return ret;
}

/*!
 * Returns a copy of this volume in which \a subtractiveShift has been subtracted from all voxel values.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::operator-(const T& subtractiveShift) const
{
    VoxelVolume<T> ret(*this);
    ret -= subtractiveShift;
    return ret;
}

/*!
 * Returns a copy of this volume with all its voxel values multiplied by \a factor.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::operator*(const T& factor) const
{
    VoxelVolume<T> ret(*this);
    ret *= factor;
    return ret;
}

/*!
 * Returns a copy of this volume with all its voxel values divided by \a divisor.
 */
template <typename T>
VoxelVolume<T> VoxelVolume<T>::operator/(const T& divisor) const
{
    VoxelVolume<T> ret(*this);
    ret /= divisor;
    return ret;
}


/*!
 * Returns true if all three dimensions of this instance and \a other are equal.
 */
inline bool VoxelVolumeDimensions::operator==(const VoxelVolumeDimensions& other) const
{
    return (x == other.x) && (y == other.y) && (z == other.z);
}

/*!
 * Returns true if any of the three dimensions of this instance and \a other differ.
 */
inline bool VoxelVolumeDimensions::operator!=(const VoxelVolumeDimensions& other) const
{
    return (x != other.x) || (y != other.y) || (z != other.z);
}

/*!
 * Returns a string that contains the dimensions joined with " x ".
 */
inline std::string VoxelVolumeDimensions::info() const
{
    return std::to_string(x) + " x " + std::to_string(y) + " x " + std::to_string(z);
}

/*!
 * Returns the total number of voxels in the volume.
 */
inline size_t VoxelVolumeDimensions::totalNbElements() const
{
    return size_t(x) * size_t(y) * size_t(z);
}

/*!
 * Returns true if the voxel sizes of this instance and \a other are equal (in all three dimensions).
 */
inline bool VoxelVolumeVoxelSize::operator==(const VoxelVolumeVoxelSize& other) const
{
    return qFuzzyCompare(x, other.x) && qFuzzyCompare(y, other.y) && qFuzzyCompare(z, other.z);
}

/*!
 * Returns true if voxel sizes of this instance and \a other differ (in any of the three
 * dimensions).
 */
inline bool VoxelVolumeVoxelSize::operator!=(const VoxelVolumeVoxelSize& other) const
{
    return !qFuzzyCompare(x, other.x) || !qFuzzyCompare(y, other.y) || !qFuzzyCompare(z, other.z);
}

/*!
 * Returns a formatted string that contains the voxel size joined with " x " and unit "mm".
 */
inline std::string VoxelVolumeVoxelSize::info() const
{
    return std::to_string(x) + "mm x " + std::to_string(y) + "mm x " + std::to_string(z) + "mm";
}

/*!
 * Returns the product of the size in all three dimensions, i.e. \f$ x \cdot y\ cdot z\f$.
 * This represents the volume of a single voxel.
 */
inline float VoxelVolumeVoxelSize::product() const
{
    return x * y * z;
}

/*!
 * Returns \c true if the voxel sizes in all three dimensions are considered equal; thus the size
 * is isotropic. Returns \c false otherwise.
 */
inline bool VoxelVolumeVoxelSize::isIsotropic() const
{
    return qFuzzyCompare(x, y) && qFuzzyCompare(y, z);
}

/*!
 * Returns true if the volume offset of this instance and \a other are equal (in all three dimensions).
 */
inline bool VoxelVolumeOffset::operator==(const VoxelVolumeOffset& other) const
{
    return qFuzzyCompare(x, other.x) && qFuzzyCompare(y, other.y) && qFuzzyCompare(z, other.z);
}

/*!
 * Returns true if volume offset of this instance and \a other differ (in any of the three
 * dimensions).
 */
inline bool VoxelVolumeOffset::operator!=(const VoxelVolumeOffset& other) const
{
    return !qFuzzyCompare(x, other.x) || !qFuzzyCompare(y, other.y) || !qFuzzyCompare(z, other.z);
}

/*!
 * Returns a formatted string that contains the volume offset joined with " x " and unit "mm".
 */
inline std::string VoxelVolumeOffset::info() const
{
    return std::to_string(x) + "mm x " + std::to_string(y) + "mm x " + std::to_string(z) + "mm";
}

/*!
 * Returns the number of elements for which memory has been allocated. This is either zero if no
 * memory has been allocated (after instantiation with a non-allocating constructor) or equal to the
 * number of elements.
 *
 * Same as: constData().size().
 *
 * \sa totalVoxelCount(), allocateMemory().
 */
template <typename T>
size_t VoxelVolume<T>::allocatedElements() const
{
    return _data.size();
}

/*!
 * Enforces memory allocation. This resizes the internal std::vector to the required number of
 * elements, given by the dimensions of the chunk, i.e. width x heigth.
 *
 * As a result, allocatedElements() will return the same as totalVoxelCount().
 *
 * \sa totalVoxelCount().
 */
template <typename T>
void VoxelVolume<T>::allocateMemory()
{
    _data.resize(totalVoxelCount());
}

/*!
 * Enforces memory allocation and if the current number of allocated elements is less than the
 * number of elements in the chunk, additional copies of \a initValue are appended.
 *
 * \sa allocatedElements(), allocateMemory(), fill().
 */
template <typename T>
void VoxelVolume<T>::allocateMemory(const T& initValue)
{
    _data.resize(totalVoxelCount(), initValue);
}

/*!
 * Fills the volume with \a fillValue. Note that this will overwrite all data stored in the volume.
 *
 * This method allocates memory for the data if it has not been allocated before.
 */
template <typename T>
void VoxelVolume<T>::fill(const T &fillValue)
{
    if(allocatedElements() != totalVoxelCount())
        allocateMemory();

    std::fill(_data.begin(), _data.end(), fillValue);
}

/*!
 * Returns a constant reference to the stored data vector.
 */
template <typename T>
const std::vector<T>& VoxelVolume<T>::constData() const
{
    return _data;
}

/*!
 * Returns a constant reference to the stored data vector.
 */
template <typename T>
const std::vector<T>& VoxelVolume<T>::data() const
{
    return _data;
}

/*!
 * Returns a reference to the stored data vector.
 */
template <typename T>
std::vector<T>& VoxelVolume<T>::data()
{
    return _data;
}

/*!
 * Returns `true` if the number of allocated elements is equal to the total number of voxels.
 * Otherwise the function returns `false`.
 * \sa totalVoxelCount(), allocatedElements()
 */
template <typename T>
bool VoxelVolume<T>::hasData() const
{
    return totalVoxelCount() == allocatedElements();
}

/*!
 * Returns \c true if the voxel sizes in all three dimensions are considered equal; thus the size
 * is isotropic. Returns \c false otherwise.
 *
 * Same as \code voxelSize().isIsotropic() \endcode.
 */
template<typename T>
bool VoxelVolume<T>::isIsotropic() const
{
    return _size.isIsotropic();
}

/*!
 * Returns the number of voxels in all three dimensions.
 *
 * For the total number of voxels in the volume, use totalVoxelCount().
 */
template <typename T>
const typename VoxelVolume<T>::Dimensions& VoxelVolume<T>::nbVoxels() const
{
    return _dim;
}

/*!
 * Returns the offset of the volume.
 */
template <typename T>
const typename VoxelVolume<T>::Offset& VoxelVolume<T>::offset() const
{
    return _offset;
}

/*!
 * Returns the pointer to the raw data.
 *
 * Same as data().data().
 */
template <typename T>
T* VoxelVolume<T>::rawData()
{
    return _data.data();
}

/*!
 * Returns the pointer to the constant raw data.
 *
 * Same as constData().data().
 */
template <typename T>
const T* VoxelVolume<T>::rawData() const
{
    return _data.data();
}

/*!
 * Returns the total number of voxels in the volume. This is the product of the voxel count in
 * all three dimensions.
 */
template <typename T>
size_t VoxelVolume<T>::totalVoxelCount() const
{
    return size_t(_dim.x) * size_t(_dim.y) * size_t(_dim.z);
}

/*!
 * Returns the size of the voxels (in millimeter).
 */
template <typename T>
const typename VoxelVolume<T>::VoxelSize& VoxelVolume<T>::voxelSize() const
{
    return _size;
}

/*!
 * Returns the smallest edge length of the voxels (in millimeter).
 */
template <typename T>
float VoxelVolume<T>::smallestVoxelSize() const
{
    return std::min(std::min(_size.x, _size.y), _size.z);
}

/*!
 * Sets the offset of the volume to \a offset. This is expected to be specified in millimeter.
 */
template <typename T>
void VoxelVolume<T>::setVolumeOffset(const Offset& offset)
{
    _offset = offset;
}

/*!
 * Convenience setter. Sets the offset of the volume using the components \a xOffset, \a yOffset and
 * \a zOffset. Offset values are expected to be specified in millimeter.
 */
template <typename T>
void VoxelVolume<T>::setVolumeOffset(float xOffset, float yOffset, float zOffset)
{
    _offset = { xOffset, yOffset, zOffset };
}

/*!
 * Sets the voxel size to \a size. This is expected to be specified in millimeter.
 */
template <typename T>
void VoxelVolume<T>::setVoxelSize(const VoxelSize& size)
{
    _size = size;
}

/*!
 * Convenience setter. Sets the voxel size using the components \a xSize, \a ySize and
 * \a zSize. Dimensions are expected to be specified in millimeter.
 */
template <typename T>
void VoxelVolume<T>::setVoxelSize(float xSize, float ySize, float zSize)
{
    _size = { xSize, ySize, zSize };
}

/*!
 * Returns the number of voxels in all three dimensions.
 *
 * Same as nbVoxels().
 */
template <typename T>
const typename VoxelVolume<T>::Dimensions& VoxelVolume<T>::dimensions() const
{
    return _dim;
}

/*!
 * Move-sets the data vector to \a data. This method performs a dimensionality check and throws an
 * error if dimensions mismatch.
 */
template <typename T>
void VoxelVolume<T>::setData(std::vector<T>&& data)
{
    if(!hasEqualSizeAs(data))
        throw std::domain_error("data vector has incompatible size for VoxelVolume");

    _data = std::move(data);
}

/*!
 * Sets the data vector to \a data. This method performs a dimensionality check and throws an error
 * if dimensions mismatch.
 */
template <typename T>
void VoxelVolume<T>::setData(const std::vector<T>& data)
{
    if(!hasEqualSizeAs(data))
        throw std::domain_error("data vector has incompatible size for VoxelVolume");

    _data = data;
}

/*!
 * Checks if the number of elements in \a other match the voxel count of this instance.
 */
template <typename T>
bool VoxelVolume<T>::hasEqualSizeAs(const std::vector<T>& other) const
{
    return totalVoxelCount() == other.size();
}

/*!
 * Helper function for running tasks in parallel over voxels.
 */
template <typename T>
template <class Function>
void VoxelVolume<T>::parallelExecution(const Function& f) const
{
    ThreadPool tp;
    const auto nbThreads = tp.nbThreads();
    const auto nbVoxels = this->data().size();
    const auto voxelPerThread = nbVoxels / nbThreads;
    size_t t = 0;

    for(; t < nbThreads; ++t)
        tp.enqueueThread(f, t * voxelPerThread, (t + 1) * voxelPerThread);
    // last thread does the rest (voxelPerThread + x, with x < nbThreads)
    tp.enqueueThread(f, t * voxelPerThread, nbVoxels);
}

/*!
 * Returns the (world) coordinates (in mm) of the center of the voxel with index \a index.
 */
template<typename T>
VoxelCoordinates VoxelVolume<T>::coordinates(const VoxelIndex& index) const
{
    const auto corner = cornerVoxel();

    return { corner.x() + float(index.x()) * _size.x,
             corner.y() + float(index.y()) * _size.y,
             corner.z() + float(index.z()) * _size.z };
}

/*!
 * Returns the (world) coordinates (in mm) of the center of the voxel with index \f$[x,y,z]\f$.
 */
template<typename T>
VoxelCoordinates VoxelVolume<T>::coordinates(uint x, uint y, uint z) const
{
    const auto corner = cornerVoxel();

    return { corner.x() + float(x) * _size.x,
             corner.y() + float(y) * _size.y,
             corner.z() + float(z) * _size.z };
}

/*!
 * Returns the (world) coordinates (in mm) of the center of the voxel with index \f$[0,0,0]\f$,
 * i.e. located at the corner of the volume.
 */
template<typename T>
VoxelCoordinates VoxelVolume<T>::cornerVoxel() const
{
    const auto x = _offset.x - 0.5f * float(_dim.x - 1) * _size.x;
    const auto y = _offset.y - 0.5f * float(_dim.y - 1) * _size.y;
    const auto z = _offset.z - 0.5f * float(_dim.z - 1) * _size.z;

    return { x, y, z };
}

/*!
 * Returns the index of the voxel whose center is closest to the world coordinate position
 * \a coordinates.
 *
 * Does not perform out of bounds checks.
 */
template<typename T>
VoxelIndex VoxelVolume<T>::index(const VoxelCoordinates& coordinates) const
{
    const auto corner = cornerVoxel();

    const auto x = static_cast<uint>(std::round( (coordinates.x() - corner.x()) / _size.x ) );
    const auto y = static_cast<uint>(std::round( (coordinates.y() - corner.y()) / _size.y ) );
    const auto z = static_cast<uint>(std::round( (coordinates.z() - corner.z()) / _size.z ) );

    return { x, y, z };
}

/*!
 * Returns the index of the voxel whose center is closest to the world coordinate position
 * \f$[x_mm, y_mm, z_mm]\f$).
 *
 * Does not perform out of bounds checks.
 */
template<typename T>
VoxelIndex VoxelVolume<T>::index(float x_mm, float y_mm, float z_mm) const
{
    const auto corner = cornerVoxel();

    const auto x = static_cast<uint>(std::round( (x_mm - corner.x()) / _size.x ) );
    const auto y = static_cast<uint>(std::round( (y_mm - corner.y()) / _size.y ) );
    const auto z = static_cast<uint>(std::round( (z_mm - corner.z()) / _size.z ) );

    return { x, y, z };
}

/*!
 * Returns the (world) coordinates (in mm) of the corner of the volume (referring to voxel
 * \f$[0,0,0]\f$).
 */
template<typename T>
VoxelCoordinates VoxelVolume<T>::volumeCorner() const
{
    const auto x = _offset.x - 0.5f * float(_dim.x) * _size.x;
    const auto y = _offset.y - 0.5f * float(_dim.y) * _size.y;
    const auto z = _offset.z - 0.5f * float(_dim.z) * _size.z;

    return { x, y, z };
}

// iterators
template<class T>
template<class IteratorType>
VoxelVolume<T>::VoxelIterator<IteratorType>::VoxelIterator(IteratorType voxel, const VoxelVolume<T>* ptrToVol)
    : _dataItr(voxel)
    , _ptrToVol(ptrToVol)
{
}

// handle MSVC bug (need to omit template keyword of depended template type)
#if defined(_MSC_FULL_VER) && (_MSC_FULL_VER <= 191025027)

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::VoxelIterator<IteratorType>&
VoxelVolume<T>::VoxelIterator<IteratorType>::operator++()
{
    ++_dataItr;

    return (*this);
}

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::VoxelIterator<IteratorType>
VoxelVolume<T>::VoxelIterator<IteratorType>::operator++(int)
{
    auto temp(*this);
    ++_dataItr;
    return temp;
}

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::VoxelIterator<IteratorType>&
VoxelVolume<T>::VoxelIterator<IteratorType>::operator--()
{
    --_dataItr;

    return (*this);
}

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::VoxelIterator<IteratorType>
VoxelVolume<T>::VoxelIterator<IteratorType>::operator--(int)
{
    auto temp(*this);
    --_dataItr;
    return temp;
}

#else // no or new MSVC

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::template VoxelIterator<IteratorType>&
VoxelVolume<T>::VoxelIterator<IteratorType>::operator++()
{
    ++_dataItr;

    return (*this);
}

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::template VoxelIterator<IteratorType>
VoxelVolume<T>::VoxelIterator<IteratorType>::operator++(int)
{
    auto temp(*this);
    ++_dataItr;
    return temp;
}

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::template VoxelIterator<IteratorType>&
VoxelVolume<T>::VoxelIterator<IteratorType>::operator--()
{
    --_dataItr;

    return (*this);
}

template<class T>
template<class IteratorType>
typename VoxelVolume<T>::template VoxelIterator<IteratorType>
VoxelVolume<T>::VoxelIterator<IteratorType>::operator--(int)
{
    auto temp(*this);
    --_dataItr;
    return temp;
}

#endif // _MSC_FULL_VER

template<class T>
template<class IteratorType>
VoxelIndex CTL::VoxelVolume<T>::VoxelIterator<IteratorType>::voxelIndex() const
{
    const auto dist = std::distance(_ptrToVol->data().cbegin(),
                                    static_cast<typename std::vector<T>::const_iterator>(_dataItr));
    const auto& dim = _ptrToVol->dimensions();

    const auto z = static_cast<uint>(dist / (dim.x * dim.y));
    const auto y = static_cast<uint>((dist - z * (dim.x * dim.y)) / dim.x);
    const auto x = static_cast<uint>(dist % dim.x);

    return { x, y, z };
}

template<typename T>
template<class IteratorType>
typename VoxelVolume<T>::template VoxelIterator<IteratorType>::reference
VoxelVolume<T>::VoxelIterator<IteratorType>::value() const
{
    return *_dataItr;
}

template<typename T>
template<class IteratorType>
VoxelVolume<T>::VoxelIterator<IteratorType>::operator VoxelIterator<typename std::vector<T>::const_iterator>() const
{
    return VoxelIterator<typename std::vector<T>::const_iterator>{ this->_dataItr, this->_ptrToVol };
}

namespace details {
    template <typename T>
    void grindBall(VoxelVolume<T>& volume, float radius)
    {
        const auto nbVox = volume.dimensions().x;
        const auto center = float(nbVox - 1) / 2.0f;

        auto dist2Center = [center](float x, float y, float z)
        {
            const auto dx = x - center;
            const auto dy = y - center;
            const auto dz = z - center;
            return dx * dx + dy * dy + dz * dz;
        };

        const auto voxSize = volume.voxelSize().x;
        const auto rSquaredInVoxel = (radius / voxSize) * (radius / voxSize);

        // erase exterior space
        for(auto x = 0u; x < nbVox; ++x)
            for(auto y = 0u; y < nbVox; ++y)
                for(auto z = 0u; z < nbVox; ++z)
                    if(dist2Center(x, y, z) > rSquaredInVoxel)
                        volume(x, y, z) = 0.0f;
    }

    template <typename T>
    void grindCylinderX(VoxelVolume<T>& volume, float radius)
    {
        const auto nbVoxCircle = volume.dimensions().y;
        const auto nbVoxHeight = volume.dimensions().x;
        const auto center = float(nbVoxCircle - 1) / 2.0f;

        auto dist2Center = [center](float y, float z)
        {
            const auto dy = y - center;
            const auto dz = z - center;
            return dy * dy + dz * dz;
        };

        const auto voxSize = volume.voxelSize().x; // isotropic
        const auto rSquaredInVoxel = (radius / voxSize) * (radius / voxSize);

        // erase exterior space
        for(auto y = 0u; y < nbVoxCircle; ++y)
            for(auto z = 0u; z < nbVoxCircle; ++z)
                if(dist2Center(y, z) > rSquaredInVoxel)
                    for(auto x = 0u; x < nbVoxHeight; ++x)
                        volume(x, y, z) = 0.0f;
    }

    template <typename T>
    void grindCylinderY(VoxelVolume<T>& volume, float radius)
    {
        const auto nbVoxCircle = volume.dimensions().x;
        const auto nbVoxHeight = volume.dimensions().y;
        const auto center = float(nbVoxCircle - 1) / 2.0f;

        auto dist2Center = [center](float x, float z)
        {
            const auto dx = x - center;
            const auto dz = z - center;
            return dx * dx + dz * dz;
        };

        const auto voxSize = volume.voxelSize().x; // isotropic
        const auto rSquaredInVoxel = (radius / voxSize) * (radius / voxSize);

        // erase exterior space
        for(auto x = 0u; x < nbVoxCircle; ++x)
            for(auto z = 0u; z < nbVoxCircle; ++z)
                if(dist2Center(x, z) > rSquaredInVoxel)
                    for(auto y = 0u; y < nbVoxHeight; ++y)
                        volume(x, y, z) = 0.0f;
    }

    template <typename T>
    void grindCylinderZ(VoxelVolume<T>& volume, float radius)
    {
        const auto nbVoxCircle = volume.dimensions().x;
        const auto nbVoxHeight = volume.dimensions().z;
        const auto center = float(nbVoxCircle - 1) / 2.0f;

        auto dist2Center = [center](float x, float y)
        {
            const auto dx = x - center;
            const auto dy = y - center;
            return dx * dx + dy * dy;
        };

        const auto voxSize = volume.voxelSize().x; // isotropic
        const auto rSquaredInVoxel = (radius / voxSize) * (radius / voxSize);

        // erase exterior space
        for(auto x = 0u; x < nbVoxCircle; ++x)
            for(auto y = 0u; y < nbVoxCircle; ++y)
                if(dist2Center(x, y) > rSquaredInVoxel)
                    for(auto z = 0u; z < nbVoxHeight; ++z)
                        volume(x, y, z) = 0.0f;
    }
}


namespace assist {

/*!
 * \brief Returns a VoxelVolume<ToType>, where \a ToType must be specified as a template parameter.
 *
 * The conversion from \a volume is performed element-wise using the callable \a f.
 * If \a f is not passed as a second argument, the default is a `static_cast<ToType>` operation.
 *
 * Besides the converted data, all properties of \a volume (voxel size, offset) are copied to the
 * returned object.
 */
template <typename ToType, typename FromType, typename ConversionFun>
VoxelVolume<ToType> convertTo(const VoxelVolume<FromType>& volume, ConversionFun f)
{
    VoxelVolume<ToType> ret{ volume.nbVoxels(), volume.voxelSize() };
    ret.setVolumeOffset(volume.offset());
    ret.allocateMemory();
    std::transform(volume.begin(), volume.end(), ret.begin(), f);
    return ret;
}

inline float interpolate3D(const VoxelVolume<float>& volume, const mat::Matrix<3, 1>& position)
{
    return interpolate3D(volume, position.get<0>(),
                                 position.get<1>(),
                                 position.get<2>());
}

inline float interpolate3D(const VoxelVolume<float>& volume, double x, double y, double z)
{
    const auto& nbVoxels = volume.dimensions();

    // read fully outside volume
    if(x < -1.0               || y < -1.0               || z < -1.0               ||
       x > double(nbVoxels.x) || y > double(nbVoxels.y) || z > double(nbVoxels.z))
        return 0.0;

    // voxel 000 (smallest indices): "left-most, bottom voxel on the front"
    const std::array<int, 3> vox { static_cast<int>(std::floor(x)),
                                   static_cast<int>(std::floor(y)),
                                   static_cast<int>(std::floor(z)) };

    // check if a border voxel is involved
    const bool borderIdxLow[3] = { vox[0] < 0,
                                   vox[1] < 0,
                                   vox[2] < 0 };
    const bool borderIdxHigh[3] = { vox[0] >= int(nbVoxels.x) - 1,
                                    vox[1] >= int(nbVoxels.y) - 1,
                                    vox[2] >= int(nbVoxels.z) - 1 };

    // compute weight factors
    const std::array<double, 3> weights { x - double(vox[0]),
                                          y - double(vox[1]),
                                          z - double(vox[2]) };

    // read out values of all 8 voxels (or assign zero if border)
    const auto v000 = (borderIdxLow[0]  || borderIdxLow[1]  || borderIdxLow[2])  ? 0.0f : volume(vox[0],   vox[1],   vox[2]);
    const auto v001 = (borderIdxLow[0]  || borderIdxLow[1]  || borderIdxHigh[2]) ? 0.0f : volume(vox[0],   vox[1],   vox[2]+1);
    const auto v010 = (borderIdxLow[0]  || borderIdxHigh[1] || borderIdxLow[2])  ? 0.0f : volume(vox[0],   vox[1]+1, vox[2]);
    const auto v011 = (borderIdxLow[0]  || borderIdxHigh[1] || borderIdxHigh[2]) ? 0.0f : volume(vox[0],   vox[1]+1, vox[2]+1);
    const auto v100 = (borderIdxHigh[0] || borderIdxLow[1]  || borderIdxLow[2])  ? 0.0f : volume(vox[0]+1, vox[1],   vox[2]);
    const auto v101 = (borderIdxHigh[0] || borderIdxLow[1]  || borderIdxHigh[2]) ? 0.0f : volume(vox[0]+1, vox[1],   vox[2]+1);
    const auto v110 = (borderIdxHigh[0] || borderIdxHigh[1] || borderIdxLow[2])  ? 0.0f : volume(vox[0]+1, vox[1]+1, vox[2]);
    const auto v111 = (borderIdxHigh[0] || borderIdxHigh[1] || borderIdxHigh[2]) ? 0.0f : volume(vox[0]+1, vox[1]+1, vox[2]+1);

    const auto w0_opp = 1.0f - weights[0];
    const auto c00 = w0_opp * v000 + weights[0] * v100;
    const auto c01 = w0_opp * v001 + weights[0] * v101;
    const auto c10 = w0_opp * v010 + weights[0] * v110;
    const auto c11 = w0_opp * v011 + weights[0] * v111;

    const auto w1_opp = 1.0f - weights[1];
    const auto c0 = c00 * w1_opp + c10 * weights[1];
    const auto c1 = c01 * w1_opp + c11 * weights[1];

    return c0 * (1.0f - weights[2]) + c1 * weights[2];
}

} // namespace assist

} // namespace CTL
