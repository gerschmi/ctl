#include "genericoclprojectionfilter.h"

#include "img/projectiondata.h"
#include "ocl/clfileloader.h"
#include "ocl/openclfunctions.h"
#include "ocl/pinnedmem.h"

#include <QDebug>
#include <QFile>

namespace CTL {
namespace OCL {

DECLARE_SERIALIZABLE_TYPE(GenericOCLProjectionFilter)

/*!
 * \brief Creates a GenericOCLProjectionFilter that executes the OpenCL kernel \a kernel.
 *
 * For internal use only. Do not use this ctor directly---i.e. without an associated .cl file
 * holding the kernel code---as this would prevent serializability of the GenericOCLProjectionFilter
 * instance.
 */
GenericOCLProjectionFilter::GenericOCLProjectionFilter(cl::Kernel* kernel,
                                                       const std::vector<float>& arguments)
    : _kernel(kernel)
    , _queue(getCommandQueue())
{
    if(!_kernel)
        throw std::runtime_error("GenericOCLProjectionFilter: Could not create filter. "
                                 "Reason: OpenCL Kernel is invalid.");

    setAdditionalKernelArgs(arguments);
}

/*!
 * \brief Creates a GenericOCLProjectionFilter instance that is capable of executing the OpenCL
 * kernel (`filter`) of the file \a clFileName.
 *
 * The \a clFileName (incl. path) is relative to the runtime cl_src folder of the CTL
 * (see ClFileLoader::openCLSourceDir()). More details can be found under 'Notes' in the detailed
 * class description).
 * For further details on the requirements for the kernel, please also refer to the detailed
 * class description.
 * Note that, if the kernel specified in \a clFileName has additional arguments, the same number of
 * arguments must be passed to \a arguments to make the filter work. Alternatively, additional
 * arguments may be passed later on through setAdditionalKernelArgs().
 */
GenericOCLProjectionFilter::GenericOCLProjectionFilter(const std::string& clFileName,
                                                       const std::vector<float>& arguments)
    : GenericOCLProjectionFilter(addKernelFromFile(clFileName), arguments)
{
    _clFileName = clFileName;
}

/*!
 * \brief Creates a GenericOCLProjectionFilter without any OpenCL kernel associated to it.
 *
 * For the purpose of deserialization only.
 */
GenericOCLProjectionFilter::GenericOCLProjectionFilter()
    : _kernel(nullptr)
    , _queue(getCommandQueue())
{
}

/*!
 * \brief Filters the input \a projections by executing the OpenCL kernel of this instance.
 *
 * This method will handle all OpenCL host code required to run the kernel of this instance with
 * the input data from \a projections and the additional kernel arguments specified in the
 * constructor or from the latest call to setAdditionalKernelArgs().
 *
 * Individual kernel calls are queued for each view of the input projection dataset. The global and
 * local worksizes for the kernel execution are taken from globalWorksize() and localWorksize(),
 * respectively.
 * Data from a single view is transfered to the OpenCL device before executing the kernel for the
 * corresponding view. Transfered projection data is represented as a \c cl::Image3D whose
 * dimensions correspond to the number of channels, rows, and modules of a single view in
 * \a projections. Note that if autoCombine()==\c true, data are concatenated to a single module.
 * Similarly, the output buffer has the size of all pixels in one view (channels\*rows\*modules).
 * Note that the result buffer will not be (re-)initialized after a view has been processed, so make
 * sure to always write values for all pixels.
 *
 * Please also note that the view index, which is passed to the kernel as third argument, is only of
 * importance if the specific filter operation has any dependency on the particular view (e.g. by
 * means of some geometry information). If processing a view is independent of the particular index,
 * it can simply be ignored, since there is only a single view being processed at a time.
 * (In particular, do not get confused with the third dimension in the input projection image object
 * within the kernel in that regard. It is not associated to the view index in any manner, but
 * represents the module index.)
 *
 * This implementation requires OpenCL image support, but no image write support.
 */
void GenericOCLProjectionFilter::filter(ProjectionData& projections)
{
    try // OpenCL exception handling
    {
        // automatic combine
        if(projections.dimensions().nbModules > 1 && autoCombine())
            projections = projections.combined();

        // create image objects
        const auto nbChans   = projections.dimensions().nbChannels;
        const auto nbRows    = projections.dimensions().nbRows;
        const auto nbModules = projections.dimensions().nbModules;

        PinnedImg3DHostWrite     origProj(nbChans, nbRows, nbModules, _queue);
        PinnedBufHostRead<float> filtProj(nbChans*nbRows*nbModules, _queue);

        // set arguments
        _kernel->setArg(0, origProj.devImage());
        _kernel->setArg(1, filtProj.devBuffer());

        // get worksizes
        const auto globalWS = globalWorksize(projections);
        const auto localWS = localWorksize(projections);

        // process views
        const auto pixelPerModule = nbChans * nbRows;
        for(auto v = 0u, nbViews = projections.nbViews(); v < nbViews; ++v)
        {
            // transfer projection data (single view)
            for(auto mod = 0u; mod < nbModules; ++mod)
                std::copy_n(projections.view(v).module(mod).rawData(), pixelPerModule,
                            origProj.hostPtr() + mod * pixelPerModule);
            origProj.transferPinnedMemToDev(false);

            // set view id
            _kernel->setArg(2, v);

            // run kernel
            _queue.enqueueNDRangeKernel(*_kernel, cl::NullRange, globalWS, localWS);

            // read result
            filtProj.transferDevToPinnedMem();
            for(auto mod = 0u; mod < nbModules; ++mod)
                std::copy_n(filtProj.hostPtr() + mod * pixelPerModule, pixelPerModule,
                            projections.view(v).module(mod).rawData());

        }

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    }
}

/*!
 * \brief Returns the global worksize for the kernel call.
 *
 * By default, this returns a two-dimensional \c cl::NDRange, where the first and second dimension
 * are the number of channels and rows in \a projections, respectively.
 *
 * Re-implement in sub-classes to change this behavior.
 */
cl::NDRange GenericOCLProjectionFilter::globalWorksize(const ProjectionData& projections) const
{
    const auto U = projections.dimensions().nbChannels;
    const auto V = projections.dimensions().nbRows;
    return cl::NDRange(U, V);
}

/*!
 * \brief Returns the local worksize for the kernel call.
 *
 * By default, this returns \c cl::NullRange. Re-implement in sub-classes to change this behavior.
 */
cl::NDRange GenericOCLProjectionFilter::localWorksize(const ProjectionData&) const
{
    return cl::NullRange;
}

/*!
 * \brief Returns true if projections will be automatically combined before being processed by the
 * kernel.
 *
 * If autoCombine()==\c true, ProjectionData::combined() will be called on the input data before it
 * is being processed. This results in all modules being concatenated horizontally (such that rows
 * join together); thus, the resulting projections consist of a single module only.
 *
 * Re-implement this method to return false in sub-classes that can (or need to) process modules
 * individually. Note that in these cases, it might also be advisable to include the number of
 * modules in the global worksize (see globalWorksize()), for example, by extending it to a 3D range
 * (e.g. with the number of modules as a third dimension).
 */
bool GenericOCLProjectionFilter::autoCombine() const
{
    return true;
}

/*!
 * \brief Adds the OpenCL kernel defined in the file \a clFileName to the OpenCL environment and
 * returns a pointer to the created cl::Kernel object.
 *
 * A two stage approach is used to load the specified file \a clFileName. If the specified file
 * exists under the specified name (incl. path), it will be loaded directly. Otherwise, an attempt
 * to load the file from the runtime cl_src folder of the CTL (see ClFileLoader::openCLSourceDir())
 * will be made. See also ClFileLoader for more details.
 */
cl::Kernel* GenericOCLProjectionFilter::addKernelFromFile(const std::string& clFileName)
{
    static const std::string kernelName = "filter";
    const std::string programName = "GenericOCLProjectionFilter_" + clFileName;

    // load source code from file
    std::string clSourceCode;
    QFile infile(QString::fromStdString(clFileName));
    // if specified file exists explicitely
    if(infile.exists())
    {
        if(!infile.open(QIODevice::ReadOnly | QIODevice::Text))
            throw std::runtime_error(clFileName + "\nis not readable");

        clSourceCode = infile.readAll().toStdString();
    }
    else // use ClFileLoader
    {
        ClFileLoader clFile(clFileName);
        if(!clFile.isValid())
            throw std::runtime_error(clFileName + "\ndoes not exist in cl_src folder");
        clSourceCode = clFile.loadSourceCode();
    }

    // add kernel to OCLConfig
    OpenCLConfig::instance().addKernel(kernelName, OpenCLFunctions::write_bufferf, clSourceCode,
                                       programName);

    // return kernel (compiles program on device if required)
    try
    {
        return OpenCLConfig::instance().kernel(kernelName, programName);
    }
    catch(const cl::Error& err)
    {
        if(err.err() == CL_INVALID_KERNEL_NAME)
            qCritical() << programName.c_str()
                        << ": Kernel name is invalid. "
                           "Make sure it is correctly named 'filter'.";
        else
            qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";

        return nullptr;
    }
}

/*!
 * \brief Returns the command queue to be used by this instance.
 *
 * Note that this will always use the first OpenCL device found in
 * OpenCLConfig::instance().devices(). Configure the device list accordingly before instantiating
 * this class if you want to utilize a particular device.
 */
cl::CommandQueue GenericOCLProjectionFilter::getCommandQueue()
{
    try // OpenCL exception handling
    {
        const auto& config  = OpenCLConfig::instance();
        const auto& context = config.context();
        return cl::CommandQueue(context, config.devices().front());
    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
    }

    return {};
}

/*!
 * \brief Convenience method for simpler setting of a single additional argument.
 *
 * This is a convenience method to be used only for kernels that have a single additional
 * argument.
 *
 * Same as: `setAdditionalKernelArgs( { argument } );`
 */
void GenericOCLProjectionFilter::setAdditionalKernelArg(float argument)
{
    setAdditionalKernelArgs( { argument } );
}

/*!
 * \brief Sets the additional kernel arguments to \a arguments.
 *
 * Each element of \a arguments is passed as an individual argument to the OpenCL kernel of this
 * instance. The index of these arguments starts at 3 (three fixed arguments + additional args from
 * \a arguments).
 *
 * Note that, if the kernel used by this instance has additional arguments, the same number of
 * arguments must be passed to \a arguments to make the filter work.
 */
void GenericOCLProjectionFilter::setAdditionalKernelArgs(const std::vector<float>& arguments)
{
    _kernel->setArg(0, dummyImage3D()); // required for compatibility with Intel OCL driver

    for(size_t arg = 0; arg < arguments.size(); ++arg)
        _kernel->setArg(uint(3 + arg), arguments[arg]);

    // remember arguments (to enable later serialization)
    _additionalArgs = arguments;
}

/*!
 * \brief Returns the parameters of this instance as QVariant.
 *
 * This returns a QVariantMap with two key-value-pairs:
 * - ("Kernel file name", file name of the .cl file containing the kernel),
 * - ("Additional arguments", list of the used additional kernel parameters).
 */
QVariant GenericOCLProjectionFilter::parameter() const
{
    QVariantMap ret = AbstractProjectionFilter::parameter().toMap();

    QVariantList parList;
    parList.reserve(static_cast<int>(_additionalArgs.size()));
    for(const auto& arg : _additionalArgs)
        parList.append(arg);

    ret.insert(QStringLiteral("Kernel file name"), QString::fromStdString(_clFileName));
    ret.insert(QStringLiteral("Additional arguments"), parList);

    return ret;
}

/*!
 * \brief Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Parameters must be passed as a QVariantMap with one or both of the follwing key-value-pairs:
 * - ("Kernel file name", file name of the .cl file containing the kernel),
 * - ("Additional arguments", list of the used additional kernel parameters).
 *
 * Note that it is not recommended to change settings of an GenericOCLProjectionFilter this way.
 * This methods mainly serves its purpose in deserialization.
 */
void GenericOCLProjectionFilter::setParameter(const QVariant& parameter)
{
    AbstractProjectionFilter::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("Kernel file name")))
    {
        _clFileName = parMap.value(QStringLiteral("Kernel file name")).toString().toStdString();
        _kernel = addKernelFromFile(_clFileName);
    }
    if(parMap.contains(QStringLiteral("Additional arguments")))
    {
        auto argList = parMap.value(QStringLiteral("Additional arguments")).toList();
        std::vector<float> argVec;
        argVec.reserve(argList.size());
        for(const auto& arg : qAsConst(argList))
            argVec.push_back(arg.toFloat());

        // Intel OCL driver compatibility
        const cl::Image3D dummyImg(_queue.getInfo<CL_QUEUE_CONTEXT>(),
                                   CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                                   cl::ImageFormat(CL_INTENSITY, CL_FLOAT), 1, 1, 1);
        _kernel->setArg(0, dummyImg);

        setAdditionalKernelArgs(argVec);
    }
}


} // namespace OCL
} // namespace CTL
