#ifndef CTL_THRESHOLDVOLUMESPARSIFIER_H
#define CTL_THRESHOLDVOLUMESPARSIFIER_H

#include "abstractvolumesparsifier.h"

namespace CTL {

/*!
 * \brief The ThresholdVolumeSparsifier class is a simple sparsifier that keeps all values above a
 * defined threshold.
 *
 * This class implements a simple sparsifying operation which evaluates the input volume and only
 * carries over those voxels that exceed the defined threshold value.
 *
 * Example:
 *\code
 *  // create a volume with 100³ voxels and fill in uniform random values from [0, 1.5]
 *  auto volume = VoxelVolume<float>::cube(100, 1.0f, 0.0f);
 *  std::generate(volume.begin(), volume.end(),
 *                [] () { return QRandomGenerator::global()->bounded(1.5f); });
 *
 *  qInfo() << volume.totalVoxelCount();
 *  // output: 1000000
 *
 *  // create a ThresholdVolumeSparsifier with a threshold of 0.75
 *  ThresholdVolumeSparsifier sparsifier(0.75f);
 *
 *  // sparsify 'volume'
 *  const auto sparseVol = sparsifier.sparsify(volume);
 *
 *  qInfo() << sparseVol.nbVoxels();
 *  // output: 500586
 * \endcode
 */
class ThresholdVolumeSparsifier : public AbstractVolumeSparsifier
{
    public:virtual SparseVoxelVolume sparsify(const VoxelVolume<float>& volume) override;

public:
    ThresholdVolumeSparsifier(float threshold);

    void setThreshold(float threshold);
    float threshold() const;

private:
    float _thresh;

};

} // namespace CTL

#endif // CTL_THRESHOLDVOLUMESPARSIFIER_H
