#ifndef CTL_ABSTRACTVOLUMESPARSIFIER_H
#define CTL_ABSTRACTVOLUMESPARSIFIER_H

#include "img/sparsevoxelvolume.h"

namespace CTL {

/*!
 * \brief The AbstractVolumeSparsifier class defines the (abstract) interface for volume sparsifier
 * classes.
 *
 * The purpose of a volume sparsifier is taking an input volume, which is a regular fully-populated
 * voxelized dataset, and return a sparse representation of it as a SparseVoxelVolume. The interface
 * for the sparsifying operation is sparsify(). This method needs to be implemented by sub-classes.
 */
class AbstractVolumeSparsifier
{
    public:virtual SparseVoxelVolume sparsify(const VoxelVolume<float>& volume) = 0;

public:
    virtual ~AbstractVolumeSparsifier() = default;

protected:
    AbstractVolumeSparsifier() = default;
    AbstractVolumeSparsifier(const AbstractVolumeSparsifier&) = default;
    AbstractVolumeSparsifier(AbstractVolumeSparsifier&&) = default;
    AbstractVolumeSparsifier& operator=(const AbstractVolumeSparsifier&) = default;
    AbstractVolumeSparsifier& operator=(AbstractVolumeSparsifier&&) = default;
};

/*!
 * \fn SparseVoxelVolume AbstractVolumeSparsifier::sparsify(const VoxelVolume<float>& volume)
 *
 * \brief Computes and returns a sparse representation of \a volume.
 *
 * This method takes the input \a volume---as a regular fully-populated voxelized dataset---and
 * shall return a sparse representation of its data as a SparseVoxelVolume.
 *
 * Implement this method in sub-classes.
 */

} // namespace CTL

#endif // CTL_ABSTRACTVOLUMESPARSIFIER_H
