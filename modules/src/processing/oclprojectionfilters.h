#ifndef CTL_OCLPROJECTIONFILTERS_H
#define CTL_OCLPROJECTIONFILTERS_H

#include "genericoclprojectionfilter.h"
#include <algorithm>
#include <complex>

namespace CTL {
class AcquisitionSetup;
class FullGeometry;

namespace OCL {

/*!
 * \brief The CosineWeighting class is an OpenCL implementation of cosine weighting.
 *
 * This applies a weighting to individual pixel values with respect to their angle to the central
 * ray.
 *
 * Note that the filter operation carried out by this class depends on geometry information.
 * Therefore, as opposed to GenericOCLProjectionFilter clases in general, the filter() method does
 * require either an AcquisitionSetup or the pre-encoded projection matrices (i.e. FullGeometry).
 * The regular (i.e. single argument) filter() method is non-functional here.
 *
 * This filter can operate on individual detector modules. Hence, automated combining of
 * multi-module projections is disabled (see autoCombine()) and parallelization is extended across
 * the module count as well (see globalWorksize()).
 */
class CosineWeighting : public GenericOCLProjectionFilter
{
    CTL_TYPE_ID(3101)

public:
    CosineWeighting();
    void filter(ProjectionData& projections, const FullGeometry& pMats);
    void filter(ProjectionData& projections, const AcquisitionSetup& setup);

protected:
    bool autoCombine() const override;
    cl::NDRange globalWorksize(const ProjectionData& projections) const override;

private:
    void filter(ProjectionData&) override{};
};

/*!
 * \brief The ParkerWeightingRev class is an OpenCL implementation of the revised Parker weighting.
 *
 * The revised Parker weighting is an extension of the original Parker redundancy weightings for
 * short projection data as proposed by
 *
 *     Authors:  Wesarg, Ebert, and Bortfeld
 *     Title:    Parker weights revisited
 *     Journal:  Medical Physics 29(3), 2002
 *
 * In comparison to the original Parker weights, there are two major novelties:
 * 1. The revised version can correctly handle overscanning, i.e. scans covering an angular range
 * of more than 180 degrees plus fan angle.
 * 2. The revised version introduces a parameterized weighting function that allows for a flexible
 * choice of smoothness between areas of redundant data. This can positively affect noise reduction,
 * especially in case of considerable overscan.
 *
 * The parameter introduced for 2. is denoted with \f$ q \f$ here. Note that for \f$ q=1 \f$, the
 * revised weighting function coincides with the classical Parker weights. However, the ability to
 * deal with overscanning is still preserved. Thus, only in case of a scan with an exact angular
 * range of 180 degrees plus fan angle, the revised Parker weights (with \f$ q=1 \f$) are fully
 * equal to the classical ones. For values \f$ 0 < q < 1 \f$, the signal-to-noise ratio can be
 * improved as redundant values from opposing views contribute more equally with low \f$ q \f$.
 * However, too small values can lead to high frequency artifacts, in particular in case of a low
 * number of views.
 *
 * As an additional feature, this version of the revised Parker weights also provides:
 * 3. A somewhat reasonable weighting in case of underscanning. Note, of course, that analytical
 * reconstruction of such data still remains strongly ill-posed from a mathematical point of view.
 * The provided weighting simply avoids undefined or unreasonable values, which may occur using the
 * proposed construction of the revised Parker weights as is (in the original manuscript; which is
 * strictly defined for regular scans or positive overscanning only).
 *
 * The original Parker weights have been proposed here:
 *
 *     Author:   Dennis Parker
 *     Title:    Optimal short scan convolution reconstruction for fanbeam CT
 *     Journal:  Medical Physics 9(2), 1982
 */
class ParkerWeightingRev : public GenericOCLProjectionFilter
{
    CTL_TYPE_ID(3102)

public:
    explicit ParkerWeightingRev(float q = 1.0f);
    void filter(ProjectionData& projections, const FullGeometry& pMats);
    void filter(ProjectionData& projections, const AcquisitionSetup& setup);

    float lastAngularRange() const;

private:
    void filter(ProjectionData&) override{};
    float _q = 1.0f;
    float _lastAngularRange = 0.0f;
};

/**
 * \class ApodizationFilter
 *
 * \brief The ApodizationFilter class is an OpenCL implementation of several apodization functions.
 *
 * For direct reconstructions of volumes from X-ray projections, e.g. using
 * FDKReconstructor, the Ram-Lak filter is applied on the projections in
 * frequency domain. Due to the discrete nature of detector pixels, the Ram-Lak
 * filter is band-limited and sampled at discrete positions, which leads to
 * spectral leakage. Moreover, in case of a low signal-to-noise ratio of the
 * projections, it is sensible to attenuate high frequency content to reduce noise
 * in the reconstructions. Apodization functions, also known as window functions, are
 * used to handle these two problems. They are (usually) symmetric functions that are
 * zero-valued outside a certain interval. For CT reconstruction, they are multiplied
 * with the Ram-Lak filter in frequency domain.
 *
 */
class ApodizationFilter : public GenericOCLProjectionFilter
{
    CTL_TYPE_ID(3103)
    Q_GADGET

public:
    void filter(ProjectionData& projections) override;

public:
    enum FilterType { RamLak, SheppLogan, Cosine, Hann };
    Q_ENUM(FilterType)

    explicit ApodizationFilter(FilterType filterType, float frequencyScaling = 1.0f);

    // AbstractProjectionFilter interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    FilterType _filterType;
    float _frequencyScaling = 1.0f;
    std::vector<float> _filter;

    ApodizationFilter();
    std::vector<float> generateCoefficients(unsigned filterSize) const;

    static QMetaEnum metaEnum();
};

/*!
 * \brief The RamLakFilter class is an OpenCL implementation of the Ram-Lak filter.
 *
 * This applies a ramp filter along the first projection dimension according to the Ram-Lak approach
 * in spatial domain.
 *
 * This class also supports scaling of all pixels along with the filter operation (potentially
 * relevant for reasons of efficiency). The scaling factor can be provided in the constructor.
 *
 * RamLakFilter only represents the unmodified version of Ram-Lak filtering; for apodization
 * techniques, please consider using ApodizationFilter.
 */
class RamLakFilter : public GenericOCLProjectionFilter
{
    CTL_TYPE_ID(3104)

public:
    explicit RamLakFilter(float scaling = 1.0f);
};

/*!
 * \brief The HilbertFilter class is an OpenCL implementation of the discrete Hilbert transform.
 *
 * This applies a convolution with the Hilbert filter
 * \f$h(x)=\frac{1}{\pi x}\f$
 * along the first projection dimension.
 *
 * This class also supports scaling of all pixels along with the filter operation (potentially
 * relevant for reasons of efficiency). The scaling factor can be provided in the constructor.
 */
class HilbertFilter : public GenericOCLProjectionFilter
{
    CTL_TYPE_ID(3105)

public:
    explicit HilbertFilter(float scaling = 1.0f);
};

/** \enum ApodizationFilter::Type
 * \brief Different types of apodization filters to be applied to the Ram-Lak filter.
 *
 * \f$\omega\in[-\pi,\pi]\f$ denotes the frequency. \f$s\in(0,1]\f$ denotes the \a frequencyScaling.
 * \f$\mathrm{apod}(\cdot)\f$ denotes the respective apodization function.
 * \f$\mathrm{apod}(\omega)=0\f$ holds true for \f$\omega\not\in[-s\pi,s\pi]\f$.
 */

/** \var ApodizationFilter::RamLak
 * \brief No apodization.
 *
 * This is the only apodization type that does not need to calculate the discrete Fourier
 * transform and its inverse if \f$s=1\f$.
 */

/** \var ApodizationFilter::SheppLogan
 * \brief Multiplies the Ram-Lak filter by the sinc function.
 *
 * \f[
 *     \mathrm{apod}(\omega) = \frac{\sin(s^{-1}\omega)}{s^{-1}\omega}
 * \f]
 *
 * L. A. Shepp and B. F. Logan, "The Fourier reconstruction of a head section,"
 * in IEEE Transactions on Nuclear Science, vol. 21, no. 3, pp. 21-43, June 1974,
 * doi: 10.1109/TNS.1974.6499235.
 *
 */

/** \var ApodizationFilter::Cosine
 * \brief Multiplies the Ram-Lak filter by the cosine function.
 *
 * \f[
 *     \mathrm{apod}(\omega) = \cos(0.5s^{-1}\omega)
 * \f]
 *
 * F. J. Harris, "On the use of windows for harmonic analysis with the discrete
 * Fourier transform," in Proceedings of the IEEE, vol. 66, no. 1, pp. 51-83,
 * Jan. 1978, doi: 10.1109/PROC.1978.10837.
 *
 */

/** \var ApodizationFilter::Hann
 * \brief Multiplies the Ram-Lak filter by the Hann window.
 *
 * \f[
 *     \mathrm{apod}(\omega) = \frac{1}{2}(1 + \cos(s^{-1}\omega))
 * \f]
 *
 * R. B. Blackman and J. W. Tukey, "The measurement of power spectra from the
 * point of view of communications engineering — Part I," in The Bell System
 * Technical Journal, vol. 37, no. 1, pp. 185-282, Jan. 1958,
 * doi: 10.1002/j.1538-7305.1958.tb03874.x.
 *
 */

} // namespace OCL

namespace assist {

// shifts the zero-frequency component to the center of the spectrum
template <typename T>
void fftshift(std::vector<T>& v)
{
    std::rotate(v.rbegin(), v.rbegin() + v.size() / 2, v.rend());
}

// inverse of fftshift
template <typename T>
void ifftshift(std::vector<T>& v)
{
    std::rotate(v.begin(), v.begin() + v.size() / 2, v.end());
}

std::vector<std::complex<double>> dft(const std::vector<float>& image);
std::vector<float> idft(const std::vector<std::complex<double>>& fourier);

} // namespace assist
} // namespace CTL

#endif // CTL_OCLPROJECTIONFILTERS_H
