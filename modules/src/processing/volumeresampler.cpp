#include "volumeresampler.h"
#include "mat/homography.h"
#include "mat/matrix_utils.h"
#include "ocl/clfileloader.h"
#include <QDebug>

const std::string CL_FILE_NAME = "processing/volumeResampler.cl"; //!< path to .cl file
const std::string CL_KERNEL_NAME = "resample"; //!< name of the OpenCL kernel function
const std::string CL_KERNEL_NAME_SUBSET_SAMPLER = "sample"; //!< name of the OpenCL kernel function
const std::string CL_PROGRAM_NAME = "volumeResampler"; //!< OCL program name



namespace CTL {

namespace {

struct SamplingInfo
{
    std::array<Range<float>, 3> boundingBox;
    std::array<uint, 3> nbSamples;

    std::vector<float> samplingPoints(uint dim) const;
    VoxelVolumeOffset volumeOffset() const;
};

SamplingInfo increaseBoundingBoxToFitVoxelSize(const std::array<Range<float>, 3>& minBoundingBox,
                                             const CTL::VoxelVolumeVoxelSize& targetVoxelSize);

} // unnamed namespace

namespace OCL {

VolumeResampler::VolumeResampler(const Chunk3D& volume,
                                 const SamplingRange& rangeDim1,
                                 const SamplingRange& rangeDim2,
                                 const SamplingRange& rangeDim3,
                                 uint oclDeviceNb)
    : _volDim(volume.dimensions())
    , _rangeDim1(rangeDim1)
    , _rangeDim2(rangeDim2)
    , _rangeDim3(rangeDim3)
    , _q(OpenCLConfig::instance().context(), OpenCLConfig::instance().devices()[oclDeviceNb])
    , _volImage3D(OpenCLConfig::instance().context(),
                  CL_MEM_READ_ONLY,
                  cl::ImageFormat(CL_INTENSITY, CL_FLOAT),
                  volume.dimensions().x,
                  volume.dimensions().y,
                  volume.dimensions().z)
    , _range1Buf(OpenCLConfig::instance().context(),
                 CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                 2 * sizeof(float))
    , _range2Buf(OpenCLConfig::instance().context(),
                 CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                 2 * sizeof(float))
    , _range3Buf(OpenCLConfig::instance().context(),
                 CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY,
                 2 * sizeof(float))
{
    ClFileLoader clFile(CL_FILE_NAME);
    if(!clFile.isValid())
        throw std::runtime_error(CL_FILE_NAME + "\nis not readable");
    const auto clSourceCode = clFile.loadSourceCode();

    OpenCLConfig::instance().addKernel(CL_KERNEL_NAME, clSourceCode, CL_PROGRAM_NAME);
    OpenCLConfig::instance().addKernel(CL_KERNEL_NAME_SUBSET_SAMPLER, clSourceCode, CL_PROGRAM_NAME);

    // Create kernel
    try
    {
        _kernel = OpenCLConfig::instance().kernel(CL_KERNEL_NAME, CL_PROGRAM_NAME);
        _kernelSubsetSampler = OpenCLConfig::instance().kernel(CL_KERNEL_NAME_SUBSET_SAMPLER, CL_PROGRAM_NAME);

        cl::size_t<3> volDim;
        volDim[0] = volume.dimensions().x;
        volDim[1] = volume.dimensions().y;
        volDim[2] = volume.dimensions().z;
        _q.enqueueWriteImage(_volImage3D, CL_TRUE, cl::size_t<3>(), volDim, 0, 0,
                             const_cast<float*>(volume.rawData()));
        _q.enqueueWriteBuffer(_range1Buf, CL_FALSE, 0, 2 * sizeof(float), &_rangeDim1);
        _q.enqueueWriteBuffer(_range2Buf, CL_FALSE, 0, 2 * sizeof(float), &_rangeDim2);
        _q.enqueueWriteBuffer(_range3Buf, CL_FALSE, 0, 2 * sizeof(float), &_rangeDim3);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }

    if(_kernel == nullptr || _kernelSubsetSampler == nullptr)
        throw std::runtime_error("kernel pointer not valid");
}

VolumeResampler::VolumeResampler(const VoxelVolume<float>& volume, uint oclDeviceNb)
    : VolumeResampler(
          volume,
          { volume.offset().x - 0.5f * volume.voxelSize().x * (volume.nbVoxels().x - 1),
            volume.offset().x + 0.5f * volume.voxelSize().x * (volume.nbVoxels().x - 1) },
          { volume.offset().y - 0.5f * volume.voxelSize().y * (volume.nbVoxels().y - 1),
            volume.offset().y + 0.5f * volume.voxelSize().y * (volume.nbVoxels().y - 1) },
          { volume.offset().z - 0.5f * volume.voxelSize().z * (volume.nbVoxels().z - 1),
            volume.offset().z + 0.5f * volume.voxelSize().z * (volume.nbVoxels().z - 1) },
          oclDeviceNb)
{
    qDebug().noquote() << "ranges in VolumeResampler:\n"
                       << "range1:" << _rangeDim1.start() << _rangeDim1.end() << '\n'
                       << "range2:" << _rangeDim2.start() << _rangeDim2.end() << '\n'
                       << "range3:" << _rangeDim3.start() << _rangeDim3.end();
}

void VolumeResampler::setSamplingRanges(const SamplingRange& rangeDim1,
                                        const SamplingRange& rangeDim2,
                                        const SamplingRange& rangeDim3)
{
    _rangeDim1 = rangeDim1;
    _rangeDim2 = rangeDim2;
    _rangeDim3 = rangeDim3;

    try
    {
        _q.enqueueWriteBuffer(_range1Buf, CL_FALSE, 0, 2 * sizeof(float), &_rangeDim1);
        _q.enqueueWriteBuffer(_range2Buf, CL_FALSE, 0, 2 * sizeof(float), &_rangeDim2);
        _q.enqueueWriteBuffer(_range3Buf, CL_FALSE, 0, 2 * sizeof(float), &_rangeDim3);
    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }
}

const cl::Image3D& VolumeResampler::oclVolume() const { return _volImage3D; }

const SamplingRange& VolumeResampler::rangeDim1() const { return _rangeDim1; }

const SamplingRange& VolumeResampler::rangeDim2() const { return _rangeDim2; }

const SamplingRange& VolumeResampler::rangeDim3() const { return _rangeDim3; }

/*!
 * Same as using the `resample` function with the identity matrix for the rotation matrix, i.e.
 * `resample(samplingPtsDim1, samplingPtsDim2, samplingPtsDim3, mat::eye<3, 3>())`.
 *
 * \sa resample(const std::vector<float>&, const std::vector<float>&, const std::vector<float>&, const mat::Matrix<3, 3>&)
 */
VolumeResampler::Chunk3D VolumeResampler::resample(const std::vector<float>& samplingPtsDim1,
                                                   const std::vector<float>& samplingPtsDim2,
                                                   const std::vector<float>& samplingPtsDim3) const
{
    return resample(samplingPtsDim1, samplingPtsDim2, samplingPtsDim3, mat::eye<3, 3>());
}

/*!
 * Returns resampled values defined by a x-y-z grid wrapped in a VoxelVolume. The grid is given by
 * the x, y and z positions: \a samplingPtsDim1, \a samplingPtsDim2 and \a samplingPtsDim3.
 * Moreover, a \a rotation (given by a rotation matrix) is applied to the managed volume during
 * the resampling.
 */
VolumeResampler::Chunk3D VolumeResampler::resample(const std::vector<float>& samplingPtsDim1,
                                                   const std::vector<float>& samplingPtsDim2,
                                                   const std::vector<float>& samplingPtsDim3,
                                                   const mat::Matrix<3, 3>& rotation) const
{
    const auto nbSmpl1 = static_cast<uint>(samplingPtsDim1.size());
    const auto nbSmpl2 = static_cast<uint>(samplingPtsDim2.size());
    const auto nbSmpl3 = static_cast<uint>(samplingPtsDim3.size());

    VoxelVolume<float> ret(nbSmpl1, nbSmpl2, nbSmpl3);

    try
    {
        const auto& context = OpenCLConfig::instance().context();

        // write sampling points into buffers
        const auto memReadFlag = CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY;
        const auto size1 = nbSmpl1 * sizeof(float);
        const auto size2 = nbSmpl2 * sizeof(float);
        const auto size3 = nbSmpl3 * sizeof(float);
        cl::Buffer smpl1Buf(context, memReadFlag, size1);
        cl::Buffer smpl2Buf(context, memReadFlag, size2);
        cl::Buffer smpl3Buf(context, memReadFlag, size3);
        _q.enqueueWriteBuffer(smpl1Buf, CL_FALSE, 0, size1, samplingPtsDim1.data());
        _q.enqueueWriteBuffer(smpl2Buf, CL_FALSE, 0, size2, samplingPtsDim2.data());
        _q.enqueueWriteBuffer(smpl3Buf, CL_FALSE, 0, size3, samplingPtsDim3.data());

        // write rotation matrix into buffer
        cl_float fltArray[9];
        const auto inverseRotation = rotation.transposed();
        std::copy(inverseRotation.begin(), inverseRotation.end(), fltArray);
        constexpr auto bytesOfRotMat = 9 * sizeof(cl_float);
        cl::Buffer rotMatBuf(context, memReadFlag, bytesOfRotMat);
        _q.enqueueWriteBuffer(rotMatBuf, CL_FALSE, 0, bytesOfRotMat, fltArray);

        // buffer for resampled result
        const auto volSize = nbSmpl1 * nbSmpl2 * nbSmpl3 * sizeof(float);
        cl::Buffer resampledVolume(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, volSize);

        // set kernel arguments
        _kernel->setArg(0, _range1Buf);
        _kernel->setArg(1, _range2Buf);
        _kernel->setArg(2, _range3Buf);
        _kernel->setArg(3, smpl1Buf);
        _kernel->setArg(4, smpl2Buf);
        _kernel->setArg(5, smpl3Buf);
        _kernel->setArg(6, rotMatBuf);
        _kernel->setArg(7, _volImage3D);
        _kernel->setArg(8, resampledVolume);

        // run kernel
        _q.enqueueNDRangeKernel(*_kernel, cl::NullRange, cl::NDRange(nbSmpl1, nbSmpl2, nbSmpl3));

        // read result
        ret.allocateMemory();
        _q.enqueueReadBuffer(resampledVolume, CL_TRUE, 0, volSize, ret.rawData());

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }

    return ret;
}

/*!
 * Returns a resampled version of the managed volume with a given \a targetVoxelSize.
 * It is the same as calling `resample(targetVoxelSize, mat::eye<3, 3>())`.
 */
VoxelVolume<float> VolumeResampler::resample(const VoxelVolume<float>::VoxelSize& targetVoxelSize) const
{
    return resample(targetVoxelSize, mat::eye<3, 3>());
}

/*!
 * Returns a resampled version of the managed volume with a given \a targetVoxelSize.
 * Moreover, a \a rotation (given by a rotation matrix) is applied to the managed volume during
 * the resampling.
 */
VoxelVolume<float> VolumeResampler::resample(const VoxelVolume<float>::VoxelSize& targetVoxelSize,
                                             const mat::Matrix<3, 3>& rotation) const
{
    const auto minBoundingBox = boundingBox(rotation);
    const auto finalSampling = increaseBoundingBoxToFitVoxelSize(minBoundingBox, targetVoxelSize);

    auto ret = resample(finalSampling.samplingPoints(0),
                        finalSampling.samplingPoints(1),
                        finalSampling.samplingPoints(2),
                        rotation);
    ret.setVoxelSize(targetVoxelSize);
    ret.setVolumeOffset(finalSampling.volumeOffset());

    return ret;
}

/*!
 * Returns a resampled version of the managed volume that is given by a \a rotation (given by a
 * rotation matrix) applied to the volume. The returned volume has the same voxel size as the
 * managed volume, i.e. it is the same as calling
 * `resampler.resample(resampler.volVoxSize(), rotation)`.
 *
 * \sa resample(const VoxelVolume<float>::VoxelSize&, const mat::Matrix<3, 3>&)
 */
VoxelVolume<float> VolumeResampler::resample(const mat::Matrix<3, 3>& rotation) const
{
    return resample(volVoxSize(), rotation);
}

/*!
 * Returns a volume that has the same properties as \a mask, except that its values are resampled
 * values of the managed volume at the voxel positions of the \a mask volume multiplied with the
 * mask values.
 */
VoxelVolume<float> VolumeResampler::resampleOnMask(const VoxelVolume<float>& mask) const
{
    VoxelVolume<float> ret(mask.nbVoxels(), mask.voxelSize());
    ret.setVolumeOffset(mask.offset());
    ret.allocateMemory();

    std::vector<Generic3DCoord> samplingPts(mask.dimensions().x);

    for(auto z = 0u; z < mask.dimensions().z; ++ z)
        for(auto y = 0u; y < mask.dimensions().y; ++y)
        {
            for(auto x = 0u; x < mask.dimensions().x; ++x)
                samplingPts[x] = mask.coordinates(x, y, z);

            const auto xLine = sample(samplingPts);

            std::transform(xLine.begin(), xLine.end(), &mask(0, y, z),
                           &ret(0, y, z), std::multiplies<float>{});
        }

    return ret;
}

std::vector<float> VolumeResampler::sample(const std::vector<Generic3DCoord>& samplingPts) const
{
    const auto nbSmpls = uint(samplingPts.size());
    std::vector<float> ret(nbSmpls);

    try
    {
        const auto& context = OpenCLConfig::instance().context();

        // write sampling points into buffers
        const auto memReadFlag = CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY;
        const auto size = 3 * nbSmpls * sizeof(float);
        cl::Buffer smplBuf(context, memReadFlag, size);
        _q.enqueueWriteBuffer(smplBuf, CL_FALSE, 0, size, samplingPts.data());

        // buffer for resampled result
        auto volSize = nbSmpls * sizeof(float);
        cl::Buffer resampledVolume(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, volSize);

        // set kernel arguments
        _kernelSubsetSampler->setArg(0, _range1Buf);
        _kernelSubsetSampler->setArg(1, _range2Buf);
        _kernelSubsetSampler->setArg(2, _range3Buf);
        _kernelSubsetSampler->setArg(3, smplBuf);
        _kernelSubsetSampler->setArg(4, _volImage3D);
        _kernelSubsetSampler->setArg(5, resampledVolume);

        // run kernel
        _q.enqueueNDRangeKernel(*_kernelSubsetSampler, cl::NullRange, cl::NDRange(nbSmpls));

        // read result
        _q.enqueueReadBuffer(resampledVolume, CL_TRUE, 0, volSize, ret.data());

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }

    return ret;
}

std::vector<float> VolumeResampler::sample(const cl::Buffer& coord3dBuffer) const
{
    std::vector<float> ret;

    try
    {
        cl_int error;
        const auto bytesOfBuffer = coord3dBuffer.getInfo<CL_MEM_SIZE>(&error);
        constexpr auto bytesOfCoordTriple = sizeof(float) * 3;
        if(error != CL_SUCCESS || bytesOfBuffer % bytesOfCoordTriple)
            throw std::runtime_error("VolumeResampler::sample: invalid CL buffer for coordinates.");

        // buffer for resampled result
        const auto nbSmpls = bytesOfBuffer / bytesOfCoordTriple;
        const auto volSize = nbSmpls * sizeof(float);
        cl::Buffer resampledVolume(OpenCLConfig::instance().context(),
                                   CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, volSize);

        // set kernel arguments
        _kernelSubsetSampler->setArg(0, _range1Buf);
        _kernelSubsetSampler->setArg(1, _range2Buf);
        _kernelSubsetSampler->setArg(2, _range3Buf);
        _kernelSubsetSampler->setArg(3, coord3dBuffer);
        _kernelSubsetSampler->setArg(4, _volImage3D);
        _kernelSubsetSampler->setArg(5, resampledVolume);

        // run kernel
        _q.enqueueNDRangeKernel(*_kernelSubsetSampler, cl::NullRange, cl::NDRange(nbSmpls));

        // read result
        ret.resize(nbSmpls);
        _q.enqueueReadBuffer(resampledVolume, CL_TRUE, 0, volSize, ret.data());

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error:" << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }

    return ret;
}

VoxelVolume<float> VolumeResampler::volume() const
{
    VoxelVolume<float> ret(_volDim, volVoxSize());
    ret.setVolumeOffset(volOffset());

    cl::size_t<3> volDim;
    volDim[0] = _volDim.x;
    volDim[1] = _volDim.y;
    volDim[2] = _volDim.z;

    ret.allocateMemory();
    _q.enqueueReadImage(_volImage3D, CL_TRUE, cl::size_t<3>(), volDim, 0, 0, ret.rawData());

    return ret;
}

/*!
 * Returns the dimensions (i.e. number of voxels) of the volume managed by this instance.
 */
const VoxelVolume<float>::Dimensions& VolumeResampler::volDim() const
{
    return _volDim;
}

/*!
 * Returns the offset (in mm) of the volume managed by this instance.
 */
VoxelVolume<float>::Offset VolumeResampler::volOffset() const
{
    return { _rangeDim1.center(), _rangeDim2.center(), _rangeDim3.center() };
}

/*!
 * Returns the size of the voxels in the volume managed by this instance.
 */
VoxelVolume<float>::VoxelSize VolumeResampler::volVoxSize() const
{
    return { _rangeDim1.spacing(_volDim.x),
             _rangeDim2.spacing(_volDim.y),
             _rangeDim3.spacing(_volDim.z) };
}

std::array<Range<float>, 3> VolumeResampler::boundingBox(const mat::Matrix<3, 3>& rotation) const
{
    std::array<Vector3x1, 8> volumeCorners{
        Vector3x1(_rangeDim1.start(), _rangeDim2.start(), _rangeDim3.start()),
        Vector3x1(_rangeDim1.start(), _rangeDim2.start(), _rangeDim3.end()  ),
        Vector3x1(_rangeDim1.start(), _rangeDim2.end()  , _rangeDim3.start()),
        Vector3x1(_rangeDim1.start(), _rangeDim2.end()  , _rangeDim3.end()  ),
        Vector3x1(_rangeDim1.end()  , _rangeDim2.start(), _rangeDim3.start()),
        Vector3x1(_rangeDim1.end()  , _rangeDim2.start(), _rangeDim3.end()  ),
        Vector3x1(_rangeDim1.end()  , _rangeDim2.end()  , _rangeDim3.start()),
        Vector3x1(_rangeDim1.end()  , _rangeDim2.end()  , _rangeDim3.end()  ),
    };

    // rotate coordinates
    std::transform(volumeCorners.begin(), volumeCorners.end(), volumeCorners.begin(),
                   [&rotation](const Vector3x1& v) { return rotation * v; });

    const auto cmpX = [](const Vector3x1& v1, const Vector3x1& v2) { return v1.get<0>() < v2.get<0>(); };
    const auto cmpY = [](const Vector3x1& v1, const Vector3x1& v2) { return v1.get<1>() < v2.get<1>(); };
    const auto cmpZ = [](const Vector3x1& v1, const Vector3x1& v2) { return v1.get<2>() < v2.get<2>(); };

    return {
        Range<float>(std::min_element(volumeCorners.begin(), volumeCorners.end(), cmpX)->get<0>() - volVoxSize().x,
                     std::max_element(volumeCorners.begin(), volumeCorners.end(), cmpX)->get<0>() + volVoxSize().x),
        Range<float>(std::min_element(volumeCorners.begin(), volumeCorners.end(), cmpY)->get<1>() - volVoxSize().y,
                     std::max_element(volumeCorners.begin(), volumeCorners.end(), cmpY)->get<1>() + volVoxSize().y),
        Range<float>(std::min_element(volumeCorners.begin(), volumeCorners.end(), cmpZ)->get<2>() - volVoxSize().z,
                     std::max_element(volumeCorners.begin(), volumeCorners.end(), cmpZ)->get<2>() + volVoxSize().z),
    };
}

/*!
 * Returns a resamples version of the \a volume at the new \a location defined by an Euclidian
 * transformation, i.e. the object in the volume is rotated by the matrix `location.rotation` and
 * then shifted by the vector `location.position`.
 * The original voxel grid is maintained, which means that parts of the object may be truncated by
 * the transformation.
 * Internally the resampling is performed by linear interpolation using the VolumeResampler.
 * Same as `euclidianTransform(volume, location.rotation, location.position)`.
 *
 * \relates VolumeResampler
 */
VoxelVolume<float> euclidianTransform(const VoxelVolume<float>& volume,
                                      const mat::Location& location)
{
    return euclidianTransform(volume, location.rotation, location.position);
}

/*!
 * Returns a resamples version of the \a volume at a new location defined by an Euclidian
 * transformation (rigid), i.e. the object in the volume is rotated by the matrix \a rotation and
 * then shifted by the vector `translation`.
 * The original voxel grid is maintained, which means that parts of the object may be truncated by
 * the transformation.
 * Internally the resampling is performed by linear interpolation using the VolumeResampler.
 *
 * \relates VolumeResampler
 */
VoxelVolume<float> euclidianTransform(const VoxelVolume<float>& volume,
                                      const mat::Matrix<3, 3>& rotation,
                                      const mat::Matrix<3, 1>& translation)
{
    const auto H = mat::Homography3D::passive(rotation, translation).subMat<0,2, 0,3>();
    const auto X = volume.dimensions().x, Y = volume.dimensions().y, Z = volume.dimensions().z;
    const auto resampler = VolumeResampler{ volume };

    auto ret = VoxelVolume<float>{ volume.dimensions() };
    ret.allocateMemory();
    auto outIt = ret.data().begin();

    auto samplePoints = std::vector<CTL::Generic3DCoord>();
    samplePoints.reserve(X * Y);

    for(auto z = 0u; z < Z; ++z)
    {
        samplePoints.clear();

        for(auto y = 0u; y < Y; ++y)
            for(auto x = 0u; x < X; ++x)
            {
                const auto targetCoord = volume.coordinates(x, y, z);
                const auto homCoord = mat::Matrix<4, 1>{ targetCoord.x(),
                                                         targetCoord.y(),
                                                         targetCoord.z(),
                                                         1.0 };
                const auto sourceCoord = H * homCoord;

                samplePoints.emplace_back(float(sourceCoord.get<0>()),
                                          float(sourceCoord.get<1>()),
                                          float(sourceCoord.get<2>()));
            }

        const auto resampled = resampler.sample(samplePoints);
        outIt = std::copy(resampled.cbegin(), resampled.cend(), outIt);
    }

    return ret;
}

} // namespace OCL

namespace {

SamplingInfo increaseBoundingBoxToFitVoxelSize(const std::array<Range<float>, 3>& minBoundingBox,
                                               const CTL::VoxelVolumeVoxelSize& targetVoxelSize)
{
    const std::array<uint, 3> nbSamples{
        static_cast<uint>(std::ceil(minBoundingBox[0].width() / targetVoxelSize.x)),
        static_cast<uint>(std::ceil(minBoundingBox[1].width() / targetVoxelSize.y)),
        static_cast<uint>(std::ceil(minBoundingBox[2].width() / targetVoxelSize.z))
    };

    const std::array<float, 3> enlarge{
        (nbSamples[0] * targetVoxelSize.x - minBoundingBox[0].width()) * 0.5f,
        (nbSamples[1] * targetVoxelSize.y - minBoundingBox[1].width()) * 0.5f,
        (nbSamples[2] * targetVoxelSize.z - minBoundingBox[2].width()) * 0.5f,
    };

    const std::array<Range<float>, 3> newRange{
        Range<float>(minBoundingBox[0].start() - enlarge[0], minBoundingBox[0].end() + enlarge[0]),
        Range<float>(minBoundingBox[1].start() - enlarge[1], minBoundingBox[1].end() + enlarge[1]),
        Range<float>(minBoundingBox[2].start() - enlarge[2], minBoundingBox[2].end() + enlarge[2])
    };

    return { newRange, nbSamples };
}

} // unnamed namespace

std::vector<float> SamplingInfo::samplingPoints(uint dim) const
{
    return boundingBox[dim].linspace(nbSamples[dim]);
}

VoxelVolumeOffset SamplingInfo::volumeOffset() const
{
    return { boundingBox[0].center(), boundingBox[1].center(), boundingBox[2].center() };
}

} // namespace CTL
