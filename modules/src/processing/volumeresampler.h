#ifndef CTL_VOLUMERESAMPLER_H
#define CTL_VOLUMERESAMPLER_H

#include "img/voxelvolume.h"
#include "ocl/openclconfig.h"
#include "processing/coordinates.h"
#include "processing/range.h"

namespace CTL {

namespace mat {
struct Location;
}

namespace OCL {

class VolumeResampler
{
public:
    //! A voxel volume with no voxel size and offset information
    using Chunk3D = VoxelVolume<float>;

    explicit VolumeResampler(const VoxelVolume<float>& volume, uint oclDeviceNb = 0);
    VolumeResampler(const Chunk3D& volume,
                    const SamplingRange& rangeDim1,
                    const SamplingRange& rangeDim2,
                    const SamplingRange& rangeDim3,
                    uint oclDeviceNb = 0);

    const SamplingRange& rangeDim1() const;
    const SamplingRange& rangeDim2() const;
    const SamplingRange& rangeDim3() const;

    // sampling functions wrapping the result in a volume object
    Chunk3D resample(const std::vector<float>& samplingPtsDim1,
                     const std::vector<float>& samplingPtsDim2,
                     const std::vector<float>& samplingPtsDim3) const;

    Chunk3D resample(const std::vector<float>& samplingPtsDim1,
                     const std::vector<float>& samplingPtsDim2,
                     const std::vector<float>& samplingPtsDim3,
                     const mat::Matrix<3, 3>& rotation) const;

    // resample to true VoxelVolume with voxel size and volume offset information
    VoxelVolume<float> resample(const VoxelVolume<float>::VoxelSize& targetVoxelSize) const;

    VoxelVolume<float> resample(const VoxelVolume<float>::VoxelSize& targetVoxelSize,
                                const mat::Matrix<3, 3>& rotation) const;

    VoxelVolume<float> resample(const mat::Matrix<3, 3>& rotation) const;

    VoxelVolume<float> resampleOnMask(const VoxelVolume<float>& mask) const;

    // generic sampling functions
    std::vector<float> sample(const std::vector<Generic3DCoord>& samplingPts) const;
    std::vector<float> sample(const cl::Buffer& coord3dBuffer) const;

    void setSamplingRanges(const SamplingRange& rangeDim1,
                           const SamplingRange& rangeDim2,
                           const SamplingRange& rangeDim3);

    const cl::Image3D& oclVolume() const;
    VoxelVolume<float> volume() const;

    const VoxelVolume<float>::Dimensions& volDim() const;
    VoxelVolume<float>::Offset volOffset() const;
    VoxelVolume<float>::VoxelSize volVoxSize() const;

private:
    VoxelVolume<float>::Dimensions _volDim; //!< Dimensions of the volume

    SamplingRange _rangeDim1;
    SamplingRange _rangeDim2;
    SamplingRange _rangeDim3;

    cl::CommandQueue _q;
    cl::Kernel* _kernel;
    cl::Kernel* _kernelSubsetSampler;
    cl::Image3D _volImage3D;
    cl::Buffer _range1Buf;
    cl::Buffer _range2Buf;
    cl::Buffer _range3Buf;

    std::array<Range<float>, 3> boundingBox(const mat::Matrix<3, 3>& rotation) const;
};

VoxelVolume<float> euclidianTransform(const VoxelVolume<float>& volume,
                                      const mat::Location& location);
VoxelVolume<float> euclidianTransform(const VoxelVolume<float>& volume,
                                      const mat::Matrix<3, 3>& rotation,
                                      const mat::Matrix<3, 1>& translation);

} // namespace OCL
} // namespace CTL

/*! \file */
///@{

///@}

#endif // CTL_VOLUMERESAMPLER_H
