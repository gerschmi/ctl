#include "regularizers.h"

#include "img/voxelvolume.h"
#include "processing/neighborselector.h"

#include <array>
#include <QMetaEnum>
#include <QDebug>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(IdentityRegularizer)
DECLARE_SERIALIZABLE_TYPE(MedianFilterRegularizer)
DECLARE_SERIALIZABLE_TYPE(TVRegularizer)
DECLARE_SERIALIZABLE_TYPE(HuberRegularizer)

/*!
 * \brief Trivial implementation that does nothing.
 */
void IdentityRegularizer::filter(VoxelVolume<float>&)
{
}

/*!
 * \brief \copybrief AbstractVolumeFilter::filter
 *
 * This filter computes the median of a voxel and its neighbors and adds a fraction of the
 * difference between the voxel's value and the median to the voxel. The fraction to be applied can
 * be specified during construction or later on through setRegularizationStrength(). Default
 * strength is 0.1 (i.e. applying 10\% of the difference).
 *
 * The voxel's neighborhood that is considered for computation of the median value can be selected
 * from:
 * - NeighborHood::NearestOnly: takes only the six direct neighbors into account [default]
 * - NeighborHood::Box3x3x3: considers all 27 voxels of a 3x3x3 box centered at the voxel of interest
 * Select the neighborhood type during construction or later on through setNeighborHoodType().
 *
 * Example:
 * \code
 *  // generata a volume with random data from [0, 1]
 *  auto volume = VoxelVolume<float>::cube(100, 1.0f, 0.0);
 *  std::mt19937 rng;
 *  std::uniform_real_distribution<float> dist(0.0, 1.0);
 *  std::generate(volume.begin(), volume.end(), [&rng, &dist]() { return dist(rng); });
 *
 *  // compute RMSE of the original volume
 *  qInfo() << metric::RMSE(volume, VoxelVolume<float>::cube(100, 1.0f, 0.0));      // output: 0.577281
 *
 *  // filter 'volume' with a MedianFilterRegularizer using strength=0.5 and direct neighbors (default)
 *  MedianFilterRegularizer filt(0.5f);
 *  filt.filter(volume);
 *
 *  // compute RMSE of the filtered volume
 *  qInfo() << metric::RMSE(volume, VoxelVolume<float>::cube(100, 1.0f, 0.0));      // output: 0.534548
 * \endcode
 */
void MedianFilterRegularizer::filter(VoxelVolume<float>& volume)
{
    switch (_neighbors) {
    case CTL::MedianFilterRegularizer::Box3x3x3:
        applyMedian3(volume);
        break;
    case CTL::MedianFilterRegularizer::NearestOnly:
        applyMedianNN(volume);
        break;
    }
}

/*!
 * \brief Creates a MedianFilterRegularizer applying \a strength times the median filter effect
 * computed with neighborhood type \a neighbors.
 *
 * Filter strength and neighborhood type can be changed later on through setRegularizationStrength()
 * and setNeighborHoodType(), respectively.
 */
MedianFilterRegularizer::MedianFilterRegularizer(float strength, MedianFilterRegularizer::NeighborHood neighbors)
    : _strength(strength)
    , _neighbors(neighbors)
{
}

/*!
 * \brief Sets the neighborhood type used for computation of the median to \a neighbors.
 *
 * The voxel's neighborhood that is considered for computation of the median value can be selected
 * from:
 * - NeighborHood::NearestOnly: takes only the six direct neighbors into account
 * - NeighborHood::Box3x3x3: considers all 27 voxels of a 3x3x3 box centered at the voxel of interest
 */
void MedianFilterRegularizer::setNeighborHoodType(NeighborHood neighbors)
{
    _neighbors = neighbors;
}

/*!
 * \brief Sets the strength of the filter effect to \a strength.
 *
 * The filter strength corresponds to the fraction of the difference between a voxel's own value and
 * the median of its and its neighbors' values that is added to the voxel during filtering.
 * For example, a value of 0.75 means that 75\% of the difference to the median will be added to a
 * voxel. A strength of 1.0, therefore, refers to 'traditional' median filtering (i.e. replacing a
 * voxel by its neighborhood median).
 *
 * For details on the considered neighboring voxels, see setNeighborHoodType().
 */
void MedianFilterRegularizer::setRegularizationStrength(float strength)
{
    _strength = strength;
}

// use base class documentation
QVariant MedianFilterRegularizer::parameter() const
{
    QVariantMap ret = AbstractVolumeFilter::parameter().toMap();

    ret.insert(QStringLiteral("strength"), _strength);
    ret.insert(QStringLiteral("neighbors"), metaEnum().valueToKey(_neighbors));

    return ret;
}

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * Note: it is not recommended to set parameters of the filter this way. Consider using the regular
 * setter methods instead (see setRegularizationStrength() and setNeighborHoodType()).
 *
 * The value passed to \a parameter must be a QVariantMap containing one or multiple of the
 * following (key, value)-pairs:
 *
 * - ("strength", [\c float] the regularization strength),
 * - ("neighbors", [\c string] a valid neighborhood type).
 *
 * The neighborhood type can be either "Box3x3x3" or "NearestOnly".
 *
 * Example: setting filter strength to 0.75 and the 3x3x3 neighborhood type
 * \code
 * MedianFilterRegularizer filt;
 * filt.setParameter(QVariantMap{ { "strength", 0.75f },
 *                                { "neighbors", "Box3x3x3" } } );
 * \endcode
 */
void MedianFilterRegularizer::setParameter(const QVariant& parameter)
{
    AbstractVolumeFilter::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("strength")))
        _strength = parMap.value(QStringLiteral("strength")).toFloat();
    if(parMap.contains(QStringLiteral("neighbors")))
    {
        const auto typeStr = parMap.value(QStringLiteral("neighbors")).toString();
        int type = metaEnum().keyToValue(qPrintable(typeStr));
        if(type == -1)
        {
            qWarning() << "MedianFilterRegularizer::setParameter: Incorrect neighbor type specified. "
                          " (valid options: [Box3x3x3 | NearestOnly]). "
                          "Using default value (NearestOnly).";
            type = 1;
        }

        _neighbors = static_cast<NeighborHood>(type);
    }
}

/*!
 * \brief Applies the median filtering (wrt. to the filter strength) on a 3x3x3 neighborhood.
 */
void MedianFilterRegularizer::applyMedian3(VoxelVolume<float> &volume)
{
    const int X = int(volume.dimensions().x);
    const int Y = int(volume.dimensions().y);
    const int Z = int(volume.dimensions().z);

    if(std::min({ X, Y, Z }) < 3)
        throw std::domain_error("Median filtering failed: volume has too few voxels in at least one "
                                "dimensions (req. at least 3 x 3 x 3).");

    VoxelVolume<float> volCpy(volume);
    const NeighborSelector selector(volCpy);

    const auto processSlice = [this, &volume, &selector, X ,Y] (int z)
    {
        std::array<float, 27> neighbors;
        for(int y = 1; y < Y - 1; ++y)
            for(int x = 1; x < X - 1; ++x)
            {
                neighbors = selector.voxelAndNeighborsUnsafe(x, y, z);

                std::nth_element(neighbors.begin(), neighbors.begin() + 13, neighbors.end());
                volume(x, y, z) += _strength * (neighbors[13] - volume(x, y, z));
            }
    };

    const auto processBorder = [this, &volume, &selector] (int x, int y, int z)
    {
        using assist::mirrorIdx;
        std::array<float, 27> neighbors = selector.voxelAndNeighborsMirrored(x, y, z);

        std::nth_element(neighbors.begin(), neighbors.begin() + 13, neighbors.end());
        volume(x, y, z) += _strength * (neighbors[13] - volume(x, y, z));
    };


    // process inner (ie. non-border) voxels
    ThreadPool pool;
    for(int z = 1; z < Z - 1; ++z)
        pool.enqueueThread(processSlice, z);

    pool.waitForFinished();

    const auto processXBorders = [&processBorder, X, Y, Z] ()
    {
        for(int y = 0; y < Y; ++y)
            for(int z = 0; z < Z; ++z)
            {
                processBorder(0, y, z);
                processBorder(X-1, y, z);
            }
    };

    const auto processYBorders = [&processBorder, X, Y, Z] ()
    {
        for(int x = 1; x < X-1; ++x)
            for(int z = 0; z < Z; ++z)
            {
                processBorder(x, 0, z);
                processBorder(x, Y-1, z);
            }
    };

    const auto processZBorders = [&processBorder, X, Y, Z] ()
    {
        for(int x = 1; x < X-1; ++x)
            for(int y = 1; y < Y-1; ++y)
            {
                processBorder(x, y, 0);
                processBorder(x, y, Z-1);
            }
    };

    pool.enqueueThread(processXBorders);
    pool.enqueueThread(processYBorders);
    pool.enqueueThread(processZBorders);

    pool.waitForFinished();
}

/*!
 * \brief Applies the median filtering (wrt. to the filter strength) on the nearest neighborhood,
 * i.e. on the voxel plus its six direct neighbors.
 */
void MedianFilterRegularizer::applyMedianNN(VoxelVolume<float> &volume)
{
    const int X = int(volume.dimensions().x);
    const int Y = int(volume.dimensions().y);
    const int Z = int(volume.dimensions().z);

    if(std::min({ X, Y, Z }) < 3)
        throw std::domain_error("Median filtering failed: volume has too few voxels in at least one "
                                "dimensions (req. at least 3 x 3 x 3).");

    VoxelVolume<float> volCpy(volume);
    NeighborSelector selector(volCpy);

    const auto processSlice = [this, &volume, &selector, X ,Y] (int z)
    {
        for(int y = 0; y < Y; ++y)
            for(int x = 0; x < X; ++x)
            {
                auto neighbors = selector.voxelAndDirectNeighborsMirrored(x, y, z);

                std::nth_element(neighbors.begin(), neighbors.begin() + 3, neighbors.end());
                volume(x, y, z) += _strength * (neighbors[3] - volume(x, y, z));
            }
    };

    // process inner (ie. non-border) voxels
    ThreadPool pool;
    for(int z = 0; z < Z; ++z)
        pool.enqueueThread(processSlice, z);

    pool.waitForFinished();
}

/*!
 * \brief QMetaEnum object used to decode/encode neighborhood enum values.
 */
QMetaEnum MedianFilterRegularizer::metaEnum()
{
    const auto& mo = MedianFilterRegularizer::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("NeighborHood");
    return mo.enumerator(idx);
}

/*!
 * \brief \copybrief AbstractVolumeFilter::filter
 *
 * This performs an anisotropic version of total variation (TV) regularization on \a volume.
 *
 * \a volume must have a minimum size of 3x3x3 voxels; throws \c std::domain_error otherwise.
 *
 * The strength of the regularization effect is controlled via the regularization parameter. This
 * defines the maximum change in multiples of 10 Hounsfield units (HU) that can be applied to a
 * single voxel. Regularization strength can be specified during construction or through
 * setRegularizationStrength().
 *
 * The computed change is applied proportionally, depending on the average number of neighbors of
 * that voxel that have a positive / negative difference to the voxel. Hence, if all surrounding
 * voxels have higher/lower values than the voxel of interest, its value will be increased/decreased
 * by 'regularizationStrength' * 10 HU. If the tendency of neighbor values is mixed, only the
 * corresponding fraction of change is applied, respectively.
 *
 * See setNeighborHoodType() for details on the different neighborhood types (or filter sizes).
 */
void TVRegularizer::filter(VoxelVolume<float>& volume)
{
    switch (_neighbors) {
    case TVRegularizer::Box3x3x3:
        applyTV3(volume);
        break;
    case TVRegularizer::NearestOnly:
        applyTVNN(volume);
        break;
    }
}

/*!
 * \brief Constructs a TVRegularizer with a maximum filter effect of \a maxChangeIn10HU times 10 HU
 * on each voxel, according to an anisotropic version of total variation (TV) minimization in the
 * voxel's neighborhood of type \a neighbors.
 *
 * For details on the neighborhood type, see setNeighborHoodType().
 */
TVRegularizer::TVRegularizer(float maxChangeIn10HU, TVRegularizer::NeighborHood neighbors)
    : _neighbors(neighbors)
{
    setRegularizationStrength(maxChangeIn10HU);
}

// use base class documentation
QVariant TVRegularizer::parameter() const
{
    QVariantMap ret = AbstractVolumeFilter::parameter().toMap();

    ret.insert(QStringLiteral("strength"), _strength);
    ret.insert(QStringLiteral("neighbors"), metaEnum().valueToKey(_neighbors));

    return ret;
}

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * Note: it is not recommended to set parameters of the filter this way. Consider using the regular
 * setter methods instead (see setRegularizationStrength() and setNeighborHoodType()).
 *
 * The value passed to \a parameter must be a QVariantMap containing one or multiple of the
 * following (key, value)-pairs:
 *
 * - ("strength", [\c float] the regularization strength),
 * - ("neighbors", [\c string] a valid neighborhood type).
 *
 * The neighborhood type can be either "Box3x3x3" or "NearestOnly".
 *
 * Example: setting filter strength to 0.75 and the 3x3x3 neighborhood type
 * \code
 * TVRegularizer filt;
 * filt.setParameter(QVariantMap{ { "strength", 0.75f },
 *                                { "neighbors", "Box3x3x3" } } );
 * \endcode
 */
void TVRegularizer::setParameter(const QVariant& parameter)
{
    AbstractVolumeFilter::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("strength")))
        _strength = parMap.value(QStringLiteral("strength")).toFloat();
    if(parMap.contains(QStringLiteral("neighbors")))
    {
        const auto typeStr = parMap.value(QStringLiteral("neighbors")).toString();
        int type = metaEnum().keyToValue(qPrintable(typeStr));
        if(type == -1)
        {
            qWarning() << "TVRegularizer::setParameter: Incorrect neighbor type specified. "
                          " (valid options: [Box3x3x3 | NearestOnly]). "
                          "Using default value (NearestOnly).";
            type = 1;
        }

        _neighbors = static_cast<NeighborHood>(type);
    }
}

/*!
 * \brief Sets the neighborhood type used for computation of the TV gradient to \a neighbors.
 *
 * The voxel's neighborhood that is considered for computation of the TV gradient can be selected
 * from:
 * - NeighborHood::NearestOnly: takes only the six direct neighbors into account; in this setting,
 * all considered neighbors are weighted equally
 * - NeighborHood::Box3x3x3: considers all 26 neighbors of a 3x3x3 box centered at the voxel of
 * interest; in this setting, differences will be weighted wrt. the distance of the neighbor to the
 * center voxel.
 */
void TVRegularizer::setNeighborHoodType(TVRegularizer::NeighborHood neighbors)
{
    _neighbors = neighbors;
}

/*!
 * \brief Sets the strength of the filter effect to \a strength.
 *
 * The parameter \a strength defines the maximum change in multiples of 10 Hounsfield units (HU,
 * at 50 keV) that can be applied to a single voxel in one execution of filter().
 * [1.0 means the maximum change is +-10 HU]
 *
 * For details on the considered neighboring voxels, see setNeighborHoodType().
 */
void TVRegularizer::setRegularizationStrength(float maxChangeIn10HU)
{
    _strength = maxChangeIn10HU * 10.0f;
}

/*!
 * \brief Applies the TV filtering (wrt. to the filter strength) on a 3x3x3 neighborhood.
 */
void TVRegularizer::applyTV3(VoxelVolume<float>& volume)
{
    const int X = int(volume.dimensions().x);
    const int Y = int(volume.dimensions().y);
    const int Z = int(volume.dimensions().z);

    if(std::min({ X, Y, Z }) < 3)
        throw std::domain_error("TV regularization: volume has too few voxels in at least one "
                                "dimensions (req. at least 3 x 3 x 3).");

    VoxelVolume<float> volCpy(volume);
    const NeighborSelector selector(volCpy);

    const auto processSlice = [this, &volume, &selector, X ,Y] (int z)
    {
        for(int y = 1; y < Y - 1; ++y)
            for(int x = 1; x < X - 1; ++x)
            {
                static const auto normalize = 6.0f * 1.0f + 12.0f * 1.0f/std::sqrt(2.0f) + 8.0f * 1.0f/std::sqrt(3.0f);
                static const auto gradStepNb1 = _step / normalize;
                static const auto gradStepNb2 = gradStepNb1 / std::sqrt(2.0f);
                static const auto gradStepNb3 = gradStepNb1 / std::sqrt(3.0f);

                //  neighbors
                const auto neighbors = selector.neighborsUnsafe(x, y, z);

                const auto refValue = neighbors.center;
                float grad = 0.0f;

                // direct neighbors
                grad += std::copysign(gradStepNb1, neighbors.order1[0] - refValue);
                grad += std::copysign(gradStepNb1, neighbors.order1[1] - refValue);
                grad += std::copysign(gradStepNb1, neighbors.order1[2] - refValue);
                grad += std::copysign(gradStepNb1, neighbors.order1[3] - refValue);
                grad += std::copysign(gradStepNb1, neighbors.order1[4] - refValue);
                grad += std::copysign(gradStepNb1, neighbors.order1[5] - refValue);

                // order 2 neighbors (distance sqrt(2) from center voxel)
                grad += std::copysign(gradStepNb2, neighbors.order2[0] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[1] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[2] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[3] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[4] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[5] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[6] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[7] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[8] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[9] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[10] - refValue);
                grad += std::copysign(gradStepNb2, neighbors.order2[11] - refValue);

                // order 3 neighbors (distance sqrt(3) from center voxel)
                grad += std::copysign(gradStepNb3, neighbors.order3[0] - refValue);
                grad += std::copysign(gradStepNb3, neighbors.order3[1] - refValue);
                grad += std::copysign(gradStepNb3, neighbors.order3[2] - refValue);
                grad += std::copysign(gradStepNb3, neighbors.order3[3] - refValue);
                grad += std::copysign(gradStepNb3, neighbors.order3[4] - refValue);
                grad += std::copysign(gradStepNb3, neighbors.order3[5] - refValue);
                grad += std::copysign(gradStepNb3, neighbors.order3[6] - refValue);
                grad += std::copysign(gradStepNb3, neighbors.order3[7] - refValue);

                volume(x, y, z) += _strength * grad;
            }
    };

    // process inner (ie. non-border) voxels
    ThreadPool pool;
    for(int z = 1; z < Z - 1; ++z)
        pool.enqueueThread(processSlice, z);

    pool.waitForFinished();

    // process borders
    const auto processBorder = [this, &volume, &selector] (int x, int y, int z)
    {
        static const auto normalize = 6.0f * 1.0f + 12.0f * 1.0f/std::sqrt(2.0f) + 8.0f * 1.0f/std::sqrt(3.0f);
        static const auto gradStepNb1 = _step / normalize;
        static const auto gradStepNb2 = gradStepNb1 / std::sqrt(2.0f);
        static const auto gradStepNb3 = gradStepNb1 / std::sqrt(3.0f);


        //  neighbors
        const auto neighbors = selector.neighborsMirrored(x, y, z);
        const auto refValue = neighbors.center;

        float grad = 0.0f;
        // direct neighbors
        grad += std::copysign(gradStepNb1, neighbors.order1[0] - refValue);
        grad += std::copysign(gradStepNb1, neighbors.order1[1] - refValue);
        grad += std::copysign(gradStepNb1, neighbors.order1[2] - refValue);
        grad += std::copysign(gradStepNb1, neighbors.order1[3] - refValue);
        grad += std::copysign(gradStepNb1, neighbors.order1[4] - refValue);
        grad += std::copysign(gradStepNb1, neighbors.order1[5] - refValue);

        // order 2 neighbors (distance sqrt(2) from center voxel)
        grad += std::copysign(gradStepNb2, neighbors.order2[0] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[1] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[2] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[3] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[4] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[5] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[6] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[7] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[8] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[9] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[10] - refValue);
        grad += std::copysign(gradStepNb2, neighbors.order2[11] - refValue);

        // order 3 neighbors (distance sqrt(3) from center voxel)
        grad += std::copysign(gradStepNb3, neighbors.order3[0] - refValue);
        grad += std::copysign(gradStepNb3, neighbors.order3[1] - refValue);
        grad += std::copysign(gradStepNb3, neighbors.order3[2] - refValue);
        grad += std::copysign(gradStepNb3, neighbors.order3[3] - refValue);
        grad += std::copysign(gradStepNb3, neighbors.order3[4] - refValue);
        grad += std::copysign(gradStepNb3, neighbors.order3[5] - refValue);
        grad += std::copysign(gradStepNb3, neighbors.order3[6] - refValue);
        grad += std::copysign(gradStepNb3, neighbors.order3[7] - refValue);

        volume(x, y, z) += _strength * grad;
    };

    const auto processXBorders = [&processBorder, X, Y, Z] ()
    {
        for(int y = 0; y < Y; ++y)
            for(int z = 0; z < Z; ++z)
            {
                processBorder(0, y, z);
                processBorder(X-1, y, z);
            }
    };

    const auto processYBorders = [&processBorder, X, Y, Z] ()
    {
        for(int x = 1; x < X-1; ++x)
            for(int z = 0; z < Z; ++z)
            {
                processBorder(x, 0, z);
                processBorder(x, Y-1, z);
            }
    };

    const auto processZBorders = [&processBorder, X, Y, Z] ()
    {
        for(int x = 1; x < X-1; ++x)
            for(int y = 1; y < Y-1; ++y)
            {
                processBorder(x, y, 0);
                processBorder(x, y, Z-1);
            }
    };

    pool.enqueueThread(processXBorders);
    pool.enqueueThread(processYBorders);
    pool.enqueueThread(processZBorders);

    pool.waitForFinished();
}

/*!
 * \brief Applies the TV filtering (wrt. to the filter strength) considering only the (six) nearest
 * neighbors of a voxel.
 */
void TVRegularizer::applyTVNN(VoxelVolume<float>& volume)
{
    const int X = int(volume.dimensions().x);
    const int Y = int(volume.dimensions().y);
    const int Z = int(volume.dimensions().z);

    if(std::min({ X, Y, Z }) < 3)
        throw std::domain_error("TV regularization: volume has too few voxels in at least one "
                                "dimensions (req. at least 3 x 3 x 3).");

    VoxelVolume<float> volCpy(volume);
    NeighborSelector selector(volCpy);

    const auto processSlice = [this, &volume, &selector, X ,Y] (int z)
    {
        for(int y = 0; y < Y; ++y)
            for(int x = 0; x < X; ++x)
            {
                static constexpr auto gradStep = _step / 6.0f;
                const auto neighbors = selector.voxelAndDirectNeighborsMirrored(x, y, z);

                float grad = 0.0f;
                grad += std::copysign(gradStep, neighbors[1] - neighbors[0]);
                grad += std::copysign(gradStep, neighbors[2] - neighbors[0]);
                grad += std::copysign(gradStep, neighbors[3] - neighbors[0]);
                grad += std::copysign(gradStep, neighbors[4] - neighbors[0]);
                grad += std::copysign(gradStep, neighbors[5] - neighbors[0]);
                grad += std::copysign(gradStep, neighbors[6] - neighbors[0]);

                volume(x, y, z) += _strength * grad;
            }
    };

    // process inner (ie. non-border) voxels
    ThreadPool pool;
    for(int z = 0; z < Z; ++z)
        pool.enqueueThread(processSlice, z);

    pool.waitForFinished();
}

/*!
 * \brief QMetaEnum object used to decode/encode neighborhood enum values.
 */
QMetaEnum TVRegularizer::metaEnum()
{
    const auto& mo = MedianFilterRegularizer::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("NeighborHood");
    return mo.enumerator(idx);
}

/*!
 * \brief \copybrief AbstractVolumeFilter::filter
 *
 * This applies the Huber regularization to \a volume.
 * \a volume must have a minimum size of 3x3x3 voxels; throws \c std::domain_error otherwise.
 *
 * This regularizer uses the so-called Huber potential, which consists of a quadratic part that
 * transitions into a linear part for arguments whose absolute value exceeds a defined threshold
 * (called Huber edge here). Consequently, voxel differences (wrt. absolute value) below the
 * threshold contribute to the gradient linearly wrt. their difference. Differences above said
 * threshold are capped by that threshold value, thus, resulting in a TV-like contribution to the
 * regularization.
 *
 * The regularization effect can be controlled with a series of parameters; see HuberRegularizer
 * (main class documentation) for more details.
 */
void HuberRegularizer::filter(VoxelVolume<float>& volume)
{
    const int X = int(volume.dimensions().x);
    const int Y = int(volume.dimensions().y);
    const int Z = int(volume.dimensions().z);

    if(std::min({ X, Y, Z }) < 3)
        throw std::domain_error("Huber regularization: volume has too few voxels in at least one "
                                "dimensions (req. at least 3 x 3 x 3).");

    VoxelVolume<float> volCpy(volume);
    const NeighborSelector selector(volCpy);

    const auto processSlice = [this, &volume, &selector, X ,Y] (int z)
    {
        using assist::clamp;
        for(int y = 1; y < Y - 1; ++y)
            for(int x = 1; x < X - 1; ++x)
            {
                const float wZ1  = _betaZ * _directZ;
                const float wXY1 = _betaXY;

                const float wZ2  = _betaZ / std::sqrt(2.0f);
                const float wXY2 = _betaXY / std::sqrt(2.0f);

                const float wZ3  = _betaZ / std::sqrt(3.0f);

                //  neighbors
                const auto neighbors = selector.neighborsUnsafe(x, y, z);

                const auto refValue = neighbors.center;
                float grad = 0.0f;

                // direct neighbors
                grad += wXY1 * clamp(refValue - neighbors.order1[0], -_huberEdge, _huberEdge);
                grad += wXY1 * clamp(refValue - neighbors.order1[1], -_huberEdge, _huberEdge);
                grad += wXY1 * clamp(refValue - neighbors.order1[2], -_huberEdge, _huberEdge);
                grad += wXY1 * clamp(refValue - neighbors.order1[3], -_huberEdge, _huberEdge);
                grad += wZ1 *  clamp(refValue - neighbors.order1[4], -_huberEdge, _huberEdge);
                grad += wZ1 *  clamp(refValue - neighbors.order1[5], -_huberEdge, _huberEdge);

                // order 2 neighbors (distance sqrt(2) from center voxel)
                grad += wZ2 *  clamp(refValue - neighbors.order2[0], -_huberEdge, _huberEdge);
                grad += wZ2 *  clamp(refValue - neighbors.order2[1], -_huberEdge, _huberEdge);
                grad += wZ2 *  clamp(refValue - neighbors.order2[2], -_huberEdge, _huberEdge);
                grad += wZ2 *  clamp(refValue - neighbors.order2[3], -_huberEdge, _huberEdge);
                grad += wZ2 *  clamp(refValue - neighbors.order2[4], -_huberEdge, _huberEdge);
                grad += wZ2 *  clamp(refValue - neighbors.order2[5], -_huberEdge, _huberEdge);
                grad += wZ2 *  clamp(refValue - neighbors.order2[6], -_huberEdge, _huberEdge);
                grad += wZ2 *  clamp(refValue - neighbors.order2[7], -_huberEdge, _huberEdge);
                grad += wXY2 * clamp(refValue - neighbors.order2[8], -_huberEdge, _huberEdge);
                grad += wXY2 * clamp(refValue - neighbors.order2[9], -_huberEdge, _huberEdge);
                grad += wXY2 * clamp(refValue - neighbors.order2[10], -_huberEdge, _huberEdge);
                grad += wXY2 * clamp(refValue - neighbors.order2[11], -_huberEdge, _huberEdge);

                // order 3 neighbors (distance sqrt(3) from center voxel)
                grad += wZ3 * clamp(refValue - neighbors.order3[0], -_huberEdge, _huberEdge);
                grad += wZ3 * clamp(refValue - neighbors.order3[1], -_huberEdge, _huberEdge);
                grad += wZ3 * clamp(refValue - neighbors.order3[2], -_huberEdge, _huberEdge);
                grad += wZ3 * clamp(refValue - neighbors.order3[3], -_huberEdge, _huberEdge);
                grad += wZ3 * clamp(refValue - neighbors.order3[4], -_huberEdge, _huberEdge);
                grad += wZ3 * clamp(refValue - neighbors.order3[5], -_huberEdge, _huberEdge);
                grad += wZ3 * clamp(refValue - neighbors.order3[6], -_huberEdge, _huberEdge);
                grad += wZ3 * clamp(refValue - neighbors.order3[7], -_huberEdge, _huberEdge);

                volume(x, y, z) -= _strength * grad;
            }
    };

    // process inner (ie. non-border) voxels
    ThreadPool pool;
    for(int z = 1; z < Z - 1; ++z)
        pool.enqueueThread(processSlice, z);

    pool.waitForFinished();

    // process borders
    const auto processBorder = [this, &volume, &selector] (int x, int y, int z)
    {
        using assist::clamp;

        const float wZ1  = _betaZ * _directZ;
        const float wXY1 = _betaXY;

        const float wZ2  = _betaZ / std::sqrt(2.0f);
        const float wXY2 = _betaXY / std::sqrt(2.0f);

        const float wZ3  = _betaZ / std::sqrt(3.0f);

        //  neighbors
        const auto neighbors = selector.neighborsMirrored(x, y, z);

        const auto refValue = neighbors.center;
        float grad = 0.0f;

        // direct neighbors
        grad += wXY1 * clamp(refValue - neighbors.order1[0], -_huberEdge, _huberEdge);
        grad += wXY1 * clamp(refValue - neighbors.order1[1], -_huberEdge, _huberEdge);
        grad += wXY1 * clamp(refValue - neighbors.order1[2], -_huberEdge, _huberEdge);
        grad += wXY1 * clamp(refValue - neighbors.order1[3], -_huberEdge, _huberEdge);
        grad += wZ1 *  clamp(refValue - neighbors.order1[4], -_huberEdge, _huberEdge);
        grad += wZ1 *  clamp(refValue - neighbors.order1[5], -_huberEdge, _huberEdge);

        // order 2 neighbors (distance sqrt(2) from center voxel)
        grad += wZ2 *  clamp(refValue - neighbors.order2[0], -_huberEdge, _huberEdge);
        grad += wZ2 *  clamp(refValue - neighbors.order2[1], -_huberEdge, _huberEdge);
        grad += wZ2 *  clamp(refValue - neighbors.order2[2], -_huberEdge, _huberEdge);
        grad += wZ2 *  clamp(refValue - neighbors.order2[3], -_huberEdge, _huberEdge);
        grad += wZ2 *  clamp(refValue - neighbors.order2[4], -_huberEdge, _huberEdge);
        grad += wZ2 *  clamp(refValue - neighbors.order2[5], -_huberEdge, _huberEdge);
        grad += wZ2 *  clamp(refValue - neighbors.order2[6], -_huberEdge, _huberEdge);
        grad += wZ2 *  clamp(refValue - neighbors.order2[7], -_huberEdge, _huberEdge);
        grad += wXY2 * clamp(refValue - neighbors.order2[8], -_huberEdge, _huberEdge);
        grad += wXY2 * clamp(refValue - neighbors.order2[9], -_huberEdge, _huberEdge);
        grad += wXY2 * clamp(refValue - neighbors.order2[10], -_huberEdge, _huberEdge);
        grad += wXY2 * clamp(refValue - neighbors.order2[11], -_huberEdge, _huberEdge);

        // order 3 neighbors (distance sqrt(3) from center voxel)
        grad += wZ3 * clamp(refValue - neighbors.order3[0], -_huberEdge, _huberEdge);
        grad += wZ3 * clamp(refValue - neighbors.order3[1], -_huberEdge, _huberEdge);
        grad += wZ3 * clamp(refValue - neighbors.order3[2], -_huberEdge, _huberEdge);
        grad += wZ3 * clamp(refValue - neighbors.order3[3], -_huberEdge, _huberEdge);
        grad += wZ3 * clamp(refValue - neighbors.order3[4], -_huberEdge, _huberEdge);
        grad += wZ3 * clamp(refValue - neighbors.order3[5], -_huberEdge, _huberEdge);
        grad += wZ3 * clamp(refValue - neighbors.order3[6], -_huberEdge, _huberEdge);
        grad += wZ3 * clamp(refValue - neighbors.order3[7], -_huberEdge, _huberEdge);

        volume(x, y, z) -= _strength * grad;
    };

    const auto processXBorders = [&processBorder, X, Y, Z] ()
    {
        for(int y = 0; y < Y; ++y)
            for(int z = 0; z < Z; ++z)
            {
                processBorder(0, y, z);
                processBorder(X-1, y, z);
            }
    };

    const auto processYBorders = [&processBorder, X, Y, Z] ()
    {
        for(int x = 1; x < X-1; ++x)
            for(int z = 0; z < Z; ++z)
            {
                processBorder(x, 0, z);
                processBorder(x, Y-1, z);
            }
    };

    const auto processZBorders = [&processBorder, X, Y, Z] ()
    {
        for(int x = 1; x < X-1; ++x)
            for(int y = 1; y < Y-1; ++y)
            {
                processBorder(x, y, 0);
                processBorder(x, y, Z-1);
            }
    };

    pool.enqueueThread(processXBorders);
    pool.enqueueThread(processYBorders);
    pool.enqueueThread(processZBorders);

    pool.waitForFinished();
}

/*!
 * \brief Constructs a HuberRegularizer with the given parameters (see details).
 *
 * The parameters define the following things:
 * - \a strength linearly scales the portion of the gradient that is applied to change
 * a single voxel's value in one execution of filter()
 * [1.0 means 1% of the gradient is applied].
 * - \a huberEdgeIn100HU defines the (absolute) difference threshold in multiples of 100 HU (at 50 keV) to
 * which differences will be clamped for computation of the gradient. For differences above the
 * threshold, the regularization effect is comparable to TV minimization
 * [1.0 means the Huber edges are +-100 HU].
 * - \a weightZ and \a weightXY specify the relative strength of the regularization in Z- and XY-
 * plane, respectively (in most settings, this refers to between-plane and in-plane regularization)
 * [\a weightZ = \a weightXY means all dimensions are treated equally].
 * - \a directZweight is a separate multiplicative weight factor for the two voxels that are
 * direct Z-neighbors of the voxel of interest
 * [1.0 means the direct Z-Neighbors are treated the same as all other Z-Neighbors].
 */
HuberRegularizer::HuberRegularizer(float strength, float huberEdgeIn100HU, float weightZ,
                                   float weightXY, float directZweight)
    : _betaZ(weightZ)
    , _betaXY(weightXY)
    , _directZ(directZweight)
{
    setRegularizationStrength(strength);
    setHuberEdge(huberEdgeIn100HU);
}

// use base class documentation
QVariant HuberRegularizer::parameter() const
{
    QVariantMap ret = AbstractVolumeFilter::parameter().toMap();

    ret.insert(QStringLiteral("strength"), _strength);
    ret.insert(QStringLiteral("Huber edge"), _huberEdge);
    ret.insert(QStringLiteral("betaZ"), _betaZ);
    ret.insert(QStringLiteral("betaXY"), _betaXY);
    ret.insert(QStringLiteral("directZ"), _directZ);

    return ret;
}

/*!
 * \brief Sets the parameters contained in \a parameter (a QVariantMap).
 *
 * Note: it is not recommended to set parameters of the filter this way. Consider using the regular
 * setter methods instead.
 *
 * The value passed to \a parameter must be a QVariantMap containing one or multiple of the
 * following (key, value)-pairs:
 *
 * - ("strength", [\c float]),
 * - ("Huber edge", [\c float]),
 * - ("betaZ", [\c float]),
 * - ("betaXY", [\c float]),
 * - ("directZ", [\c float])
 *
 * Example: setting filter strength to 0.75 and direct Z-neighbor weighting to 2.0.
 * \code
 * HuberRegularizer filt;
 * filt.setParameter(QVariantMap{ { "strength", 0.75f },
 *                                { "directZ", 2.0f } } );
 * \endcode
 */
void HuberRegularizer::setParameter(const QVariant& parameter)
{
    AbstractVolumeFilter::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("strength")))
        _strength = parMap.value(QStringLiteral("strength")).toFloat();
    if(parMap.contains(QStringLiteral("Huber edge")))
        _huberEdge = parMap.value(QStringLiteral("Huber edge")).toFloat();
    if(parMap.contains(QStringLiteral("betaZ")))
        _betaZ = parMap.value(QStringLiteral("betaZ")).toFloat();
    if(parMap.contains(QStringLiteral("betaXY")))
        _betaXY = parMap.value(QStringLiteral("betaXY")).toFloat();
    if(parMap.contains(QStringLiteral("directZ")))
        _directZ = parMap.value(QStringLiteral("directZ")).toFloat();
}

/*!
 * \brief Sets the regularization parameter to \a strength.
 *
 * The parameter \a strength linearly scales the portion of the gradient that is applied to change
 * a single voxel's value in one execution of filter()
 * [\a strength=1.0 means 1% of the gradient is applied].
 */
void HuberRegularizer::setRegularizationStrength(float strength)
{
    _strength = strength * 0.01f;
}

/*!
 * \brief Sets the Huber edge to \a edgeIn100HU times 100 HU.
 *
 * The parameter \a edgeIn100HU defines the (absolute) difference threshold in multiples of 100 HU
 * (at 50 keV) to which differences will be clamped for computation of the gradient. For differences
 * larger than the threshold, the regularization effect is comparable to TV minimization
 */
void HuberRegularizer::setHuberEdge(float edgeIn100HU)
{
    _huberEdge = edgeIn100HU * 100.0f * 0.02269f/1000.0f;
}

/*!
 * \brief Sets the relative strength of the regularization in Z- and XY- plane to \a weightZ and
 * \a weightXY, respectively.
 *
 * Note that in most settings, \a weightZ refers to between-plane and \a weightXY to in-plane
 * regularization [\a weightZ = \a weightXY means all dimensions are treated equally].
 */
void HuberRegularizer::setRelativeWeighting(float weightZ, float weightXY)
{
    _betaZ = weightZ;
    _betaXY = weightXY;
}

/*!
 * \brief Sets the weighting factor for direct neighbors in Z-direction to \a weight.
 *
 * \a directZNeighborWeight is a separate multiplicative weight factor for the two voxels that are
 * direct Z-neighbors (i.e. with same x, and y coordinate) of the voxel of interest
 * [1.0 means the direct Z-Neighbors are treated the same as all other Z-Neighbors].
 */
void HuberRegularizer::setDirectZNeighborWeight(float weight)
{
    _directZ = weight;
}

} // namespace CTL
