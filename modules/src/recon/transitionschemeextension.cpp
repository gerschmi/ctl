#include "transitionschemeextension.h"

#include <cmath>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(TransitionSchemeExtension)

/*!
 * \brief Implementation of the wrapper for the subset generation routine.
 *
 * This wraps the call to generateSubsetsImpl() of the nested subset generator. Before passing the
 * call to the nested generator, its number of subsets is set according to the \a iteration.
 *
 * For more details on how the subset sizes for a particular iteration are constructed, please refer
 * to the detailed class description or the related setters.
 *
 * \sa setTransitionPeriod(), setMaximumNbSubsets(), setMinimumNbSubsets(), setSubsetBounds().
 */
std::vector<ProjectionDataView> TransitionSchemeExtension::generateSubsetsImpl(uint iteration) const
{
    _nestedGen->setNbSubsets(numberOfSubsets(iteration));
    return _nestedGen->generateSubsets(iteration);
}

/*!
 * \brief Creates a TransitionSchemeExtension using \a nestedGenerator as actual subset generator.
 *
 * This instance takes ownership of \a nestedGenerator.
 *
 * Make sure to set projection data (see setProjections()) and - if required by the nested generator
 * - also the acquisition setup (see setSetup()) before using this instance for subset generation.
 * It is not required to call setData() (or corresponding individual setters) separately on the
 * nested generator. The required calls will be passed to the nested generator when calling
 * the corresponding methods of this instance.
 *
 * The default subset size parameters are as follows:
 * - transition period: 1
 * - maximum nb. subsets: 0 (i.e. unlimited; uses full nb. of available projections)
 * - minimum nb. subsets: 1
 */
TransitionSchemeExtension::TransitionSchemeExtension(AbstractFixedSizeSubsetGenerator* nestedGenerator)
    : _nestedGen(nestedGenerator)
{
    if(!nestedGenerator)
        throw std::runtime_error("TransitionSchemeSubsetGenerator: Nested generator must not be "
                                 "nullptr.");
}

/*!
 * \brief Creates a TransitionSchemeExtension using \a nestedGenerator as actual subset generator.
 *
 * Make sure to set projection data (see setProjections()) and - if required by the nested generator
 * - also the acquisition setup (see setSetup()) before using this instance for subset generation.
 * It is not required to call setData() (or corresponding individual setters) separately on the
 * nested generator. The required calls will be passed to the nested generator when calling
 * the corresponding methods of this instance.
 *
 * The default subset size parameters are as follows:
 * - transition period: 1
 * - maximum nb. subsets: 0 (i.e. unlimited; uses full nb. of available projections)
 * - minimum nb. subsets: 1
 */
TransitionSchemeExtension::TransitionSchemeExtension(std::unique_ptr<AbstractFixedSizeSubsetGenerator> nestedGenerator)
    : TransitionSchemeExtension(nestedGenerator.release())
{
}

/*!
 * \brief \copybrief AbstractSubsetGenerator::setSetup
 *
 * Whether or not setting the AcquisitionSetup is required, depends on the nested subset generator
 * that is used. Please refer to the documentation of the nested subset generator for details.
 *
 * This directly passes the call to the nested generator.
 */
void TransitionSchemeExtension::setSetup(const AcquisitionSetup& setup)
{
    _nestedGen->setSetup(setup);
}

/*!
 * \copydoc AbstractSubsetGenerator::setProjections
 *
 * This directly passes the call to the nested generator.
 */
void TransitionSchemeExtension::setProjections(ProjectionDataView projections)
{
    _nestedGen->setProjections(std::move(projections));
}

// use base class documentation
void TransitionSchemeExtension::fromVariant(const QVariant& variant)
{
    AbstractSubsetGenerator::fromVariant(variant);

    QVariantMap varMap = variant.toMap();

    setTransitionPeriod(varMap.value(QStringLiteral("doubling period"), 1u).toUInt());
    setMaximumNbSubsets(varMap.value(QStringLiteral("maximum nb subsets"), 0u).toUInt());
    setMinimumNbSubsets(varMap.value(QStringLiteral("minimum nb subsets"), 1u).toUInt());


    auto parsedGenObj = SerializationHelper::parseMiscObject(varMap.value(QStringLiteral("nested generator")));
    if(!dynamic_cast<AbstractFixedSizeSubsetGenerator*>(parsedGenObj))
    {
        delete parsedGenObj;
        throw std::runtime_error("TransitionSchemeSubsetGenerator::fromVariant(): "
                                 "Could not successfully parse nested generator object.");
    }

    setSubsetGenerator(static_cast<AbstractFixedSizeSubsetGenerator*>(parsedGenObj));

}

// use base class documentation
QVariant TransitionSchemeExtension::toVariant() const
{
    QVariantMap ret = AbstractSubsetGenerator::toVariant().toMap();

    ret.insert(QStringLiteral("doubling period"), _transitionPeriod);
    ret.insert(QStringLiteral("maximum nb subsets"), _maxNbSubsets);
    ret.insert(QStringLiteral("minimum nb subsets"), _minNbSubsets);
    ret.insert(QStringLiteral("nested generator"), _nestedGen->toVariant());

    return ret;
}

/*!
 * \brief Sets the transition period (i.e. the number of iterations after which the subset count is
 * cut in half) to \a transitionEveryNIterations.
 *
 * The transition period is the number of iterations after which the number of generated subsets is
 * cut in half (rounded down). If the result of cutting the number in half deceeds the minimum
 * number of subsets specified for this instance, the minimum value will be used instead (and thus,
 * remains constant for all following iterations).
 *
 * \copydetails TransitionSchemeExtension::numberOfSubsets
 *
 * Example:
 * \code
 *  // create dummy projection data with 20 views
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(20);
 *
 *  // create a TransitionSchemeExtension 'on top' of a SimpleSubsetGenerator
 *  TransitionSchemeExtension subsetGen(new SimpleSubsetGenerator);
 *  // here, we specify the maximum number of subsets to be 5
 *  subsetGen.setMaximumNbSubsets(5);
 *  // we now set the desired transition period to 2, i.e. the number of subsets halves every 2 iterations
 *  subsetGen.setTransitionPeriod(2);
 *
 *  // pass the dummy projection data
 *  subsetGen.setProjections(dummy);
 *
 *  // generate (and inspect) subsets for the first 6 iterations
 *  for(uint it = 0; it < 6; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 5 subset(s)
 *  // std::vector(11, 3, 18, 12)
 *  // std::vector(0, 15, 9, 5)
 *  // std::vector(19, 1, 6, 8)
 *  // std::vector(16, 7, 14, 4)
 *  // std::vector(13, 10, 2, 17)
 *
 *  // Iteration 1 with 5 subset(s)
 *  // std::vector(4, 9, 17, 18)
 *  // std::vector(15, 6, 10, 7)
 *  // std::vector(1, 19, 16, 5)
 *  // std::vector(13, 3, 11, 0)
 *  // std::vector(12, 2, 8, 14)
 *
 *  // Iteration 2 with 2 subset(s)
 *  // std::vector(4, 0, 10, 11, 19, 15, 1, 12, 3, 5)
 *  // std::vector(9, 13, 2, 18, 16, 7, 6, 17, 8, 14)
 *
 *  // Iteration 3 with 2 subset(s)
 *  // std::vector(11, 10, 3, 9, 16, 13, 14, 7, 8, 1)
 *  // std::vector(12, 5, 17, 19, 6, 15, 0, 18, 4, 2)
 *
 *  // Iteration 4 with 1 subset(s)
 *  // std::vector(18, 15, 4, 16, 12, 19, 6, 17, 8, 3, 14, 10, 13, 2, 5, 0, 1, 7, 11, 9)
 *
 *  // Iteration 5 with 1 subset(s)
 *  // std::vector(1, 10, 12, 15, 6, 16, 0, 19, 11, 2, 4, 8, 14, 17, 5, 9, 13, 7, 18, 3)
 * \endcode
 */
void TransitionSchemeExtension::setTransitionPeriod(uint transitionEveryNIterations)
{
    _transitionPeriod = transitionEveryNIterations;
}

/*!
 * \brief Sets the maximum number of subsets generated to \a maxNbSubsets.
 *
 * The maximum number of subsets refers to the amount of subsets generated in the first iterations
 * (i.e. before the first transition takes place). If \a maxNbSubsets = 0, the maximum number of
 * subsets that is generated will be set to the total number of projections available in the data
 * that has been set; thus, resulting in subsets containing a single view each.
 * The default value is 0.
 *
 * A formal description of the procedure can be found in the detailed class description and in
 * numberOfSubsets().
 *
 * Example:
 * \code
 *  // create dummy projection data with 10 views
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(10);
 *
 *  // create a TransitionSchemeExtension 'on top' of a SimpleSubsetGenerator
 *  TransitionSchemeExtension subsetGen(new SimpleSubsetGenerator);
 *  // here, we specify the maximum number of subsets to be 6
 *  subsetGen.setMaximumNbSubsets(6);
 *
 *  // pass the dummy projection data
 *  subsetGen.setProjections(dummy);
 *
 *  // generate (and inspect) subsets for the first 4 iterations
 *  for(uint it = 0; it < 4; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 6 subset(s)
 *  // std::vector(2, 1)
 *  // std::vector(8)
 *  // std::vector(7)
 *  // std::vector(9, 4)
 *  // std::vector(0, 3)
 *  // std::vector(5, 6)
 *
 *  // Iteration 1 with 3 subset(s)
 *  // std::vector(7, 4, 8, 3)
 *  // std::vector(6, 1, 0)
 *  // std::vector(9, 2, 5)
 *
 *  // Iteration 2 with 1 subset(s)
 *  // std::vector(2, 5, 1, 9, 4, 6, 8, 0, 3, 7)
 *
 *  // Iteration 3 with 1 subset(s)
 *  // std::vector(1, 0, 4, 6, 7, 8, 2, 5, 3, 9)
 * \endcode
 */
void TransitionSchemeExtension::setMaximumNbSubsets(uint maxNbSubsets)
{
    _maxNbSubsets = maxNbSubsets;
}

/*!
 * \brief Sets the minimum number of subsets generated to \a minNbSubsets.
 *
 * The minimum number of subsets refers to the lowest number of subsets generated in 'late'
 * iterations (i.e. after enough transitions have taken place to lower the number of subsets to this
 * level). If \a minNbSubsets = 1, all available projections will eventually end up in a single
 * subset.
 *
 * \a minNbSubsets must be larger than zero.
 *
 * A formal description of the procedure can be found in the detailed class description and in
 * numberOfSubsets().
 *
 * Example:
 * \code
 *  // create dummy projection data with 8 views
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(8);
 *
 *  // create a TransitionSchemeExtension 'on top' of a SimpleSubsetGenerator
 *  TransitionSchemeExtension subsetGen(new SimpleSubsetGenerator);
 *  // here, we specify the minimum number of subsets to be 3
 *  subsetGen.setMinimumNbSubsets(3);
 *
 *  // pass the dummy projection data
 *  subsetGen.setProjections(dummy);
 *
 *  // generate (and inspect) subsets for the first 4 iterations
 *  for(uint it = 0; it < 4; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 8 subset(s)
 *  // std::vector(5)
 *  // std::vector(0)
 *  // std::vector(2)
 *  // std::vector(4)
 *  // std::vector(6)
 *  // std::vector(7)
 *  // std::vector(1)
 *  // std::vector(3)
 *
 *  // Iteration 1 with 4 subset(s)
 *  // std::vector(7, 2)
 *  // std::vector(3, 4)
 *  // std::vector(1, 0)
 *  // std::vector(5, 6)
 *
 *  // Iteration 2 with 3 subset(s)
 *  // std::vector(6, 4)
 *  // std::vector(7, 0, 2)
 *  // std::vector(3, 5, 1)
 *
 *  // Iteration 3 with 3 subset(s)
 *  // std::vector(0, 7, 3)
 *  // std::vector(6, 5)
 *  // std::vector(4, 1, 2)
 * \endcode
 */
void TransitionSchemeExtension::setMinimumNbSubsets(uint minNbSubsets)
{
    if(minNbSubsets == 0)
        throw std::domain_error("TransitionSchemeSubsetGenerator: "
                                "Minimum number of subsets must be larger than zero.");
    _minNbSubsets = minNbSubsets;
}

/*!
 * \brief Sets the number of subsets in the beginning (early iterations) and at the end of
 * transitions (late iterations) to \a initialNbSubsets and \a finalNbSubsets, respectively.
 *
 * This is a convenience method that is a combined call to
 * setMaximumNbSubsets(\a initialNbSubsets) and setMinimumNbSubsets(\a finalNbSubsets);
 *
 * A formal description of the procedure can be found in the detailed class description and in
 * numberOfSubsets().
 *
 * Example:
 * \code
 *  // create dummy projection data with 8 views
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(10);
 *
 *  // create a TransitionSchemeExtension 'on top' of a SimpleSubsetGenerator
 *  TransitionSchemeExtension subsetGen(new SimpleSubsetGenerator);
 *  // here, we specify the subset bound to transition from 6 (start) to 2 subsets (end)
 *  subsetGen.setSubsetBounds(6, 2);
 *
 *  // pass the dummy projection data
 *  subsetGen.setProjections(dummy);
 *
 *  // generate (and inspect) subsets for the first 4 iterations
 *  for(uint it = 0; it < 4; ++it)
 *  {
 *      const auto subsets = subsetGen.generateSubsets(it);
 *
 *      qInfo() << "Iteration" << it << "with" << subsets.size() << "subset(s)";
 *
 *      // print the ids of the views included in the two subsets
 *      for(const auto& subset : subsets)
 *          qInfo() << subset.viewIds();
 *  }
 *
 *  // output:
 *  // Iteration 0 with 6 subset(s)
 *  // std::vector(2, 1)
 *  // std::vector(8)
 *  // std::vector(7)
 *  // std::vector(9, 4)
 *  // std::vector(0, 3)
 *  // std::vector(5, 6)
 *
 *  // Iteration 1 with 3 subset(s)
 *  // std::vector(7, 4, 8, 3)
 *  // std::vector(6, 1, 0)
 *  // std::vector(9, 2, 5)
 *
 *  // Iteration 2 with 2 subset(s)
 *  // std::vector(2, 5, 1, 9, 4)
 *  // std::vector(6, 8, 0, 3, 7)
 *
 *  // Iteration 3 with 2 subset(s)
 *  // std::vector(0, 5, 7, 4, 6)
 *  // std::vector(3, 8, 2, 9, 1)
 * \endcode
 */
void TransitionSchemeExtension::setSubsetBounds(uint initialNbSubsets, uint finalNbSubsets)
{
    setMaximumNbSubsets(initialNbSubsets);
    setMinimumNbSubsets(finalNbSubsets);
}

/*!
 * \brief Sets the nested subset generator to \a nestedGenerator; deleted previous one.
 *
 * This replaces the previous nested subset generator by \a nestedGenerator. This instance takes
 * ownership of \a nestedGenerator and deletes the previous generator.
 *
 * \a nestedGenerator must not be \c nullptr.
 *
 * Note that any previous call to setData() (or setProjections() / setSetup()) needs to be repeated
 * after replacing the subset generator by means of this method. Please also pay attention of any
 * potential differences in the requirements of the newly installed nested generator (such as
 * requirement of the AcquisitionSetup etc.).
 */
void TransitionSchemeExtension::setSubsetGenerator(AbstractFixedSizeSubsetGenerator* nestedGenerator)
{
    if(!nestedGenerator)
        throw std::runtime_error("TransitionSchemeSubsetGenerator::setSubsetGenerator(): "
                                 "Nested generator must not be nullptr.");

    _nestedGen.reset(nestedGenerator);
}

/*!
 * \brief Sets the nested subset generator to \a nestedGenerator; deleted previous one.
 *
 * This replaces the previous nested subset generator by \a nestedGenerator.
 *
 * \a nestedGenerator must not be \c nullptr.
 *
 * Note that any previous call to setData() (or setProjections() / setSetup()) needs to be repeated
 * after replacing the subset generator by means of this method. Please also pay attention of any
 * potential differences in the requirements of the newly installed nested generator (such as
 * requirement of the AcquisitionSetup etc.).
 */
void TransitionSchemeExtension::setSubsetGenerator(std::unique_ptr<AbstractFixedSizeSubsetGenerator> nestedGenerator)
{
    setSubsetGenerator(nestedGenerator.release());
}

/*!
 * \brief Computes the number of subsets that shall be generated for iteration nb. \iteration.
 *
 * The number of subsets \f$ N \f$ generated in a particular iteration \f$ i \f$, can be expressed
 * as follows (given the maximum and minimum subset count as \f$ N_{max}\f$ and \f$ N_{min}\f$, as
 * well as the transition period \f$ T \f$):
 *
 * \f[
 * \def\lf{\left\lfloor}
 * \def\rf{\right\rfloor}
 * N(i) = \max \left(\lf N_{max} / 2^{\lf i / T \rf} \rf, N_{min} \right)
 * \f]
 *
 * The following special cases exist:
 *
 * \f$
 * \begin{align*}
 * T &== 0 \to T = N_{proj} \\
 * N_{max} &== 0 \to N_{max} = N_{proj}
 * \end{align*}
 * \f$
 *
 * where \f$ N_{proj} \f$ denotes the total number of projections in the data set to this instance.
 */
uint TransitionSchemeExtension::numberOfSubsets(uint iteration) const
{
    if(_transitionPeriod == 0)
        return _maxNbSubsets;

    const auto maxNbSubsets = (_maxNbSubsets == 0) ? _nestedGen->projections().nbViews()
                                                   : _maxNbSubsets;

    const auto transitions = iteration / _transitionPeriod;
    const auto nbSubsets   = maxNbSubsets / uint(std::pow(2, std::min(transitions, 31u)));

    return std::max(nbSubsets, _minNbSubsets);
}

/*!
 * \copydoc AbstractSubsetGenerator::shuffleSubsets
 *
 * This method override ensures that shuffling is only performed once. That means if both, the
 * nested generator and this instance itself, have shuffling enabled (see
 * isSubsetPermutationEnabled()), only the shuffling of the nested generator is performed.
 */
void TransitionSchemeExtension::shuffleSubsets(std::vector<ProjectionDataView>& subsets) const
{
    // only shuffle if nested projector does not shuffle already
    if(!_nestedGen->isSubsetPermutationEnabled())
        AbstractSubsetGenerator::shuffleSubsets(subsets);
}

} // namespace CTL
