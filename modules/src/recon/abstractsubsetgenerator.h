#ifndef CTL_ABSTRACTSUBSETGENERATOR_H
#define CTL_ABSTRACTSUBSETGENERATOR_H

#include "img/projectiondataview.h"
#include "io/serializationinterface.h"

#include <random>

namespace CTL {

class AcquisitionSetup;

/*!
 * \brief The AbstractSubsetGenerator class is the abstract base class for subset generators.
 *
 * This class provides the interface for subset generator implementations. The public interface
 * provides the options to generate the subsets for a particular iteration through generateSubsets()
 * and generation of the entirety of all subsets for a given number of iterations (see
 * generateAllSubsets()). Before subsets can be generated, projections must be set by passing a view
 * to the data (see ProjectionDataView) through setProjections(). Depending on the specific
 * algorithm, sub-classes may also require the acquisition setup that corresponds to the projection
 * data. This can be passed to setSetup(). For convenience, setData() can be used as a simultaneous
 * setter for both projection data and setup.
 *
 * By default, the list of generated subsets will be shuffled - i.e. the order of subsets will be
 * permuted randomly before they are returned; uses the std::mt19937 RNG engine for that purpose.
 * The permutation procedure can be configured through:
 * - setSubsetPermutationEnabled() - enable/disable subset permutation
 * - setRandomGeneratorSeed()      - set a seed for the RNG used to perform the permutation
 * - isSubsetPermutationEnabled()  - query if subset permutation is enabled/disabled
 * - randomGeneratorSeed()         - returns the current RNG seed (default is 42)
 *
 * If necessary, the current ProjectionDataView can be queried through projections().
 *
 * __Sub-classing__:
 * Sub-classes need to implement generateSubsetsImpl(), which is the routine to generate the subsets
 * for a particular iteration. Depending on the specific subset generation algorithm, it might be
 * required to have access to information about the acquisition. If required, re-implement
 * setSetup() and extract all relevant data (or copy the passed setup). This class already provides
 * a random number generator (std::mt19937); i.e., if random number generation becomes necessary in
 * sub-classes, it is strongly advised to use this class' RNG to allow end-users to provide a global
 * seed for all RNG processes through the base-class interface (see setRandomGeneratorSeed()). Here,
 * the RNG is used to permute the order of subsets after generation. It might be necessary to change
 * this behavior in sub-classes; for example to avoid multiple shuffle steps. Re-implement
 * shuffleSubsets() to change the shuffling procedure in sub-classes.
 *
 * To enable de-/serialization of objects of the new sub-class, reimplement the toVariant() and
 * fromVariant() methods. These should take care of all newly introduced information of the
 * sub-class. Additionally, call the macro #DECLARE_SERIALIZABLE_TYPE(YourNewClassName) within the
 * .cpp file of your new class (substitute "YourNewClassName" with the actual class name). Objects
 * of the new class can then be de-/serialized with any of the serializer classes (see also
 * AbstractSerializer). Remember to also register the new component in the enumeration using the
 * #CTL_TYPE_ID(newIndex) macro. Subset generators use the miscellaneous object category in the
 * index range from 1000 to 1999. It is required to specify a value for \a newIndex that is not
 * already in use.
 */
class AbstractSubsetGenerator : public SerializationInterface
{
    CTL_TYPE_ID(1000)

    protected:virtual std::vector<ProjectionDataView> generateSubsetsImpl(uint iteration) const = 0;

public:
    virtual ~AbstractSubsetGenerator() = default;

    // SerializationInterface interface
    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

    virtual void setSetup(const AcquisitionSetup&) {}
    virtual void setProjections(ProjectionDataView projections);

    // convenience combined setter
    void setData(ProjectionDataView projections, const AcquisitionSetup& setup);

    std::vector<ProjectionDataView> generateSubsets(uint iteration) const;
    virtual std::vector<std::vector<ProjectionDataView>> generateAllSubsets(uint nbIterations);

    bool isSubsetPermutationEnabled() const;
    const ProjectionDataView& projections() const;
    uint randomGeneratorSeed() const;
    void setRandomGeneratorSeed(uint seed = std::random_device{}());
    void setSubsetPermutationEnabled(bool enabled);

protected:
    AbstractSubsetGenerator();
    AbstractSubsetGenerator(const AbstractSubsetGenerator&) = default;
    AbstractSubsetGenerator(AbstractSubsetGenerator&&) = default;
    AbstractSubsetGenerator& operator=(const AbstractSubsetGenerator&) = default;
    AbstractSubsetGenerator& operator=(AbstractSubsetGenerator&&) = default;

    virtual void shuffleSubsets(std::vector<ProjectionDataView>& subsets) const;

    ProjectionDataView _fullProjections = ProjectionDataView::invalidView(); //!< The projection data view this instance operates on
    mutable std::mt19937 _rng;      //!< random number generator (RNG) instance
    uint _rngSeed = 42;             //!< value used to seed the RNG
    bool _permuteSubsets = true;    //!< state variable if subset permutation is enabled
};

/*!
 * \brief The AbstractFixedSizeSubsetGenerator class specializes the AbstractSubsetGenerator for
 * generators that generate a pre-defined number of subsets.
 *
 * This class is a specialization of AbstractSubsetGenerator and extends its interface by a setter
 * for the number of subsets that shall be generated. In AbstractSubsetGenerator, a call to
 * generateSubsets() does return a vector of subsets whose size is not knowable in advance.
 * Prefer this class over AbstractSubsetGenerator as base class for a subset generator
 * implementation whenever the algorithm allows for generation of a pre-defined number of subsets.
 *
 * Generators of base type AbstractFixedSizeSubsetGenerator can be used within the
 * TransitionSchemeExtension.
 *
 * The function assist::subsetSizes() can be used to conveniently calculate sizes for subsets given
 * the total number of projections, the requested number of subsets and an arrangement type
 * (specifying how projections shall be distributed if numbers are not divisable).
 */
class AbstractFixedSizeSubsetGenerator : public AbstractSubsetGenerator
{
    CTL_TYPE_ID(1001)

public:
    uint nbSubsets() const;
    void setNbSubsets(uint nbSubsets);

    // SerializationInterface interface
    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

protected:
    AbstractFixedSizeSubsetGenerator(uint nbSubsets = 1u);

    uint _nbSubsets = 1u;
};


namespace assist {

enum class SubsetArrangement { Evenly, LastIsLargest, LastIsSmallest };

std::vector<uint> subsetSizes(uint nbViews, uint nbSubsets, SubsetArrangement arrangement = SubsetArrangement::Evenly);

} // namespace assist


} // namespace CTL

#endif // CTL_ABSTRACTSUBSETGENERATOR_H
