#include "simplebackprojectorcpu.h"
#include "img/voxelvolume.h"
#include "mat/projectionmatrix.h"
#include "acquisition/geometryencoder.h"
#include "components/abstractdetector.h"
#include <QMetaEnum>
#include <array>

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(SimpleBackprojectorCPU)

/*!
 * \copybrief AbstractReconstructor::parameter
 *
 * Returns a QVariantMap including the following (key, value)-pairs:
 *
 * - ("interpolate", [bool] whether or not to use interpolation in projection domain lookups),
 * - ("weighting type", [string] name of the weighting type).
 */
QVariant SimpleBackprojectorCPU::parameter() const
{
    QVariantMap ret;

    ret.insert(QStringLiteral("interpolate"), _interpolate);
    ret.insert(QStringLiteral("weighting type"), metaEnum().valueToKey(_weighting));

    return ret;
}

/*!
 * \copybrief AbstractReconstructor::setParameter
 *
 * The passed \a parameter must be a QVariantMap containing one or more of the following
 * (key, value)-pairs:
 *
 * - ("interpolate", [bool] whether or not to use interpolation in projection domain lookups),
 * - ("weighting type", [string] name of the weighting type).
 *
 * Note that it is strongly discouraged to use this method to set individual parameters of the
 * instance. Please consider using individual setter methods for that purpose.
 */
void SimpleBackprojectorCPU::setParameter(const QVariant& parameter)
{
    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("interpolate")))
        _interpolate = parMap.value(QStringLiteral("interpolate")).toBool();
    if(parMap.contains(QStringLiteral("weighting type")))
    {
        const auto typeStr = parMap.value(QStringLiteral("weighting type")).toString();
        int type = metaEnum().keyToValue(qPrintable(typeStr));
        if(type == -1)
        {
            qWarning() << "SimpleBackprojectorCPU::setParameter: Incorrect weighting type specified. "
                          " (valid options: [DistanceWeightsOnly | GeometryFactors | CustomWeights]). "
                          "Using default value (GeometryFactors).";
            type = 0;
        }

        _weighting = static_cast<WeightingType>(type);
    }
}

/*!
 * \copydoc AbstractReconstructor::toVariant
 *
 * In addition to any base class information, this introduces a (key,value)-pair:
 * ("#", [string] this class' name).
 */
QVariant SimpleBackprojectorCPU::toVariant() const
{
    QVariantMap ret = AbstractReconstructor::toVariant().toMap();

    ret.insert(QStringLiteral("#"), SimpleBackprojectorCPU::staticMetaObject.className());

    return ret;
}

/*!
 * \copybrief AbstractReconstructor::reconstructToPlain
 *
 * This implements a voxel-driven backprojection approach that uses a projection operation to
 * identify which projection data needs to be backprojected to a certain voxel. The workflow is
 * described in the detailed class description.
 *
 * The implementation treats \a targetVolume as an initialization. That means, reconstructed data
 * is added to the input volume (instead of overwriting its values).
 *
 * Note that \a projections is a ProjectionDataView, meaning it may contain a subset of data (or
 * data in shuffled order). In that case, pay attention that the setup passed to configure() must
 * still correspond to the full projection dataset the ProjectionDataView refers to.
 */
bool SimpleBackprojectorCPU::reconstructToPlain(const ProjectionDataView& projections,
                                                VoxelVolume<float>& targetVolume)
{
    // fill volume with zero if unitialized
    if(!targetVolume.hasData())
        targetVolume.fill(0.0f);

    const auto nbViews = projections.nbViews();
    const auto nbViewsInSetup = _setup.nbViews();
    const auto nbModules = projections.viewDimensions().nbModules;
    const auto& viewIds = projections.viewIds();

    // consistency checks
    if(nbViewsInSetup == 0)
    {
        qCritical() << "SimpleBackprojectorCPU::reconstructToPlain(): Reconstruction aborted. "
                       "Reason: no projection matrices available. Did you forget to call "
                       "configure()?";
        return false;
    }

    if(nbModules != _setup.system()->detector()->nbDetectorModules())
    {
        qCritical() << "SimpleBackprojectorCPU::reconstructToPlain(): Reconstruction aborted. "
                       "Reason: projection data view contains data with a different number of "
                       "modules than in the configured setup.";
        return false;
    }

    if(std::any_of(viewIds.cbegin(), viewIds.cend(),
                   [nbViewsInSetup] (uint viewID) { return viewID >= nbViewsInSetup; } ))
    {
        qCritical() << "SimpleBackprojectorCPU::reconstructToPlain(): Reconstruction aborted. "
                       "Reason: projection data view contains a view ID that exceeds the number "
                       "of views in the configured setup.";
        return false;
    }

    // note that projection matrices are properly normalized for distance weighting
    const auto pMats = GeometryEncoder::encodeSubsetGeometry(_setup, projections.viewIds());

    // set correct weighting factors for all modules
    if(!selectModuleWeights(pMats, targetVolume))
        return false;

    // loop over modules
    for(uint view = 0; view < nbViews; ++view)
    {
        Q_EMIT notifier()->progress(qreal(view) / nbViews);
        for(uint mod = 0; mod < nbModules; ++mod)
            processModule(projections.view(view).module(mod),
                          pMats.view(viewIds[view]).module(mod),
                          targetVolume,
                          _weights[view * nbModules + mod]);
    }
    return true;
}

/*!
 * \brief Configures the reconstructor.
 *
 * Takes a copy of \a setup to make it available for information extraction when it becomes
 * necessary during reconstruction.
 */
void SimpleBackprojectorCPU::configure(const AcquisitionSetup& setup)
{
    _setup = setup;
}

/*!
 * \brief Constructs a SimpleBackprojectorCPU instance with weighting type \a weightType.
 *
 * For more details on weighting types, please refer to the documentation of BackprojectorBase.
 */
SimpleBackprojectorCPU::SimpleBackprojectorCPU(SimpleBackprojectorCPU::WeightingType weightType)
    : _weighting(weightType)
{
}

/*!
 * \brief Sets interpolation usage to \a interpolate.
 *
 * If \a interpolate = \c true, projection values will be linearly interpolated during
 * backprojection. Otherwise, a nearest neighbor strategy is used.
 */
void SimpleBackprojectorCPU::setInterpolate(bool interpolate) { _interpolate = interpolate; }

/*!
 * \brief Sets the view- and module-dependent custom weights to \a weights.
 *
 * \a weights must include one weight for each view and module in the projection data that shall be
 * reconstructed later. The weight factors must appear in \a weights in that order, i.e. first the
 * weights for all modules of the first view, then followed by all modules of the second view and so
 * forth. You must enable use of the custom weights by passing WeightingType::CustomWeights to
 * setWeightingType(). Since the backprojection routine already implements distance weights, the
 * custom weights are an additional multiplicative factor to these. They cannot be used in
 * combination with WeightingType::GeometryFactors, because the replace these weights when enabled.
 */
void SimpleBackprojectorCPU::setCustomWeights(std::vector<float> weights)
{
    _customWeights = std::move(weights);
}

/*!
 * \brief Sets the weighting type to \a weightType.
 *
 * \a weightType may be either DistanceWeightsOnly, GeometryFactors, or CustomWeights. Note that
 * distance weighting is always performed. When using GeometryFactors or CustomWeights, the
 * corresponding weights are multiplicative to the already performed distance weighting.
 *
 * If using CustomWeights, remember to also set the actual weights through setCustomWeights().
 *
 * For more detail on distance and geometry factor weighting, please refer to the documentation of
 * BackprojectorBase.
 */
void SimpleBackprojectorCPU::setWeightingType(SimpleBackprojectorCPU::WeightingType weightType)
{
    _weighting = weightType;
}

/*!
 * \brief Backprojects the data from \a moduleData into \a targetVolume, assuming the geometry
 * defined by \a P1 and the result weighted by \a moduleWeight.
 */
void SimpleBackprojectorCPU::processModule(const Chunk2D<float>& moduleData,
                                           const mat::ProjectionMatrix& P1,
                                           VoxelVolume<float>& targetVolume,
                                           float moduleWeight)
{
     // abbreviations
    const uint X = targetVolume.dimensions().x;
    const uint Y = targetVolume.dimensions().y;
    const uint Z = targetVolume.dimensions().z;

    // center of the voxel that is at the corner of the volume
    const auto corner = targetVolume.cornerVoxel();
    const Vector3x1 volCorner{ corner.x(), corner.y(), corner.z() };
    const Vector3x1 voxelSize{ targetVolume.voxelSize().x,
                               targetVolume.voxelSize().y,
                               targetVolume.voxelSize().z };

    // lower bound for projection pixel index (-1, this bound is because
    // integer index is considered to be the center of a pixel)
    const auto lowerBoundX = _interpolate ? -1.0 : -0.5;
    const auto lowerBoundY = _interpolate ? -1.0 : -0.5;

    // upper bound for projection pixel index (nbPixels - 1 + 0.5, this bound is because
    // integer index is considered to be the center of a pixel)
    const auto upperBoundX = _interpolate ? double(moduleData.width())  : double(moduleData.width()) - 0.5;
    const auto upperBoundY = _interpolate ? double(moduleData.height()) : double(moduleData.width()) - 0.5;

    // temp variables within the following loop
    Vector3x1 p1_homo, p1_homoX, p1_homoY;
    mat::Matrix<2, 1> p1;
    // 3d world coord of voxel center (homog. form)
    mat::Matrix<4, 1> r({ 0.0, 0.0, 0.0, 1.0 });
    // the projection matrices splitted into columns
    const Vector3x1 P1Column0 = P1.column<0>();
    const Vector3x1 P1Column1 = P1.column<1>();
    const Vector3x1 P1Column2 = P1.column<2>();
    const Vector3x1 P1Column3 = P1.column<3>();

    // __Iteration over voxels__
    // use separation of matrix product:
    // (P*r)(i) = P(i,0)*r(0) + P(i,1)*r(1) + P(i,2)*r(2) + P(i,3)*r(3)
    for(uint x = 0; x < X; ++x)
    {
        r.get<0>() = std::fma(double(x), voxelSize.get<0>(), volCorner.get<0>());
        // P*r = P(.,0)*r(0) + P(.,3)*r(3) + ...
        p1_homoX = P1Column0 * r.get<0>() + P1Column3; // * r(3)=1.0

        for(uint y = 0; y < Y; ++y)
        {
            r.get<1>() = std::fma(double(y), voxelSize.get<1>(), volCorner.get<1>());
            // ... + P(.,1)*r(1)
            p1_homoY = p1_homoX + P1Column1 * r.get<1>();

            for(uint z = 0; z < Z; ++z)
            {
                r.get<2>() = std::fma(double(z), voxelSize.get<2>(), volCorner.get<2>());
                // ... + P(.,2)*r(2)
                p1_homo = p1_homoY + P1Column2 * r.get<2>();

                // convert to cartesian coord (divide by w, where p_homo = [x y w])
                p1 = { p1_homo.get<0>(), p1_homo.get<1>() };
                p1 /= p1_homo.get<2>();

                // check if p1 is outside the detector
                if(p1.get<0>() < lowerBoundX || p1.get<0>() >= upperBoundX ||
                   p1.get<1>() < lowerBoundY || p1.get<1>() >= upperBoundY)
                    continue;

                const float backsmearValue = _interpolate ? assist::interpolate2D(moduleData, p1)
                                                          : moduleData(uint(p1.get<0>() + 0.499),
                                                                       uint(p1.get<1>() + 0.499));

                // assume proper normalization of projection matrices for distance weighting
                const float distanceWeight = p1_homo.get<2>() * p1_homo.get<2>();
                const float weight = moduleWeight / distanceWeight;

                targetVolume(x, y, z) += backsmearValue * weight;
            }
        }
    }
}

/*!
 * \brief Computes geometry factor weights for the geometry encoded in \a pMats and a target volume
 * whose dimensions are extracted from \a targetVolume.
 *
 * Note that \a targetVolume is only used to extract the dimensions (i.e. products of voxel sizes)
 * of the target volume. There is no need for \a targetVolume to actually be the target volume used
 * in reconstructTo() later on, as long as it has the same dimensions.
 */
std::vector<float> SimpleBackprojectorCPU::geoWeights(const FullGeometry& pMats,
                                                      const VoxelVolume<float>& targetVolume)
{
    std::vector<float> weights(pMats.totalNbPmats());

    const auto voxelVolume = targetVolume.voxelSize().product();

    auto wIt = weights.begin();
    for(const auto& viewPmats : pMats)
        for(const auto& modPmat: viewPmats)
        {
            const auto K = modPmat.intrinsicMatK();
            (*wIt++) = voxelVolume * K.get<0,0>() * K.get<1,1>();
        }

    return weights;
}

/*!
 * \brief Selects the weight factors to be used, based on the current selected weighting type.
 *
 * Weighting type can be specified during construction or later on through setWeightingType().
 */
bool SimpleBackprojectorCPU::selectModuleWeights(const FullGeometry& pMats,
                                                 const VoxelVolume<float>& targetVolume)
{
    switch (_weighting) {
    case CTL::SimpleBackprojectorCPU::DistanceWeightsOnly:
        _weights = std::vector<float>(pMats.totalNbPmats(), 1.0f);
        break;
    case CTL::SimpleBackprojectorCPU::GeometryFactors:
        _weights = geoWeights(pMats, targetVolume);
        break;
    case CTL::SimpleBackprojectorCPU::CustomWeights:
        if(_customWeights.size() != pMats.totalNbPmats())
        {
            qCritical() << "SimpleBackprojectorCPU: Reconstruction aborted. "
                           "Reason: custom weights have a different number of elements than there"
                           "are modules in the configured setup.";
            return false;
        }
        _weights = _customWeights;
        break;
    }

    return true;
}

/*!
 * \brief QMetaEnum object used to decode/encode subset order enum values.
 */
QMetaEnum SimpleBackprojectorCPU::metaEnum()
{
    const auto& mo = SimpleBackprojectorCPU::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("WeightingType");
    return mo.enumerator(idx);
}

/*!
 * \enum SimpleBackprojectorCPU::WeightingType
 * Enumeration for weighting type used in backprojection.
 *
 * The type decides which weighting factor will be applied to the data that is backprojected.
 */

/*!
 * \var SimpleBackprojectorCPU::DistanceWeightsOnly
 *
 * Only applies distance weighting (as part of the actual backprojection routine).
 */

/*!
 * \var SimpleBackprojectorCPU::GeometryFactors
 *
 * Applies geometry factor based weighting in addition to distance weighting.
 */

/*!
 * \var SimpleBackprojectorCPU::CustomWeights
 *
 * Applies custom weighting factors in addition to distance weighting.
 */

} // namespace CTL
