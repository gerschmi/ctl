#ifndef CTL_SIMPLEBACKPROJECTOR_H
#define CTL_SIMPLEBACKPROJECTOR_H

#include "backprojectorbase.h"
#include "acquisition/acquisitionsetup.h"
#include "mat/matrix_types.h"
#include "ocl/pinnedmem.h"
#include <QObject>
#include <memory>

namespace CTL {
namespace OCL {

/*!
 * \class SimpleBackprojector
 * \brief The SimpleBackprojector class is an OpenCL implementation of a voxel-driven backprojector
 * using the "back-targeting" approach.
 *
 * This class implements a voxel-driven backprojection approach that uses a projection operation to
 * identify which projection data needs to be backprojected to a certain voxel. In brief, the
 * routine works as follows: For each voxel V (with its x, y, z coordinates) in the target volume,
 * and given the projection matrix P of a particular view (and module), we compute the location
 * where V gets projected (using homogeneous coordinates and the corresponding projection matrix
 * product) and read-out the corresponding value from the projection data (zero if outside). This
 * value is weighted according to the chosen weighting type (see BackprojectorBase for more details)
 * and the result is added to the current value of V. This procedure is carried out for all views
 * (and modules) of the projection data.
 * All voxels within an xy-slice of the target volume are processed in parallel. Individual views
 * (and modules) are processed sequentially, accumulating the result in the current slice. The
 * routine then continues with the next slice.
 *
 * To increase accuracy, volume sub-subsamling can be used (see setVolumeSubsamplingFactor()). This
 * can be useful in particular if the voxel size in the target volume is large in comparison to the
 * pixel size in the projection data.
 *
 * For more details on usage of reconstructors in general, please also refer to the documentation
 * of AbstractReconstructor.
 *
 * Example:
 * \code
 *  // create a cube phantom
 *  const auto phantom = VoxelVolume<float>::cube(100, 1.0f, 0.02f);
 *
 *  // create an AcquisitionSetup for a flat panel short scan configuration (100 views)
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>(), 100);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  // simulate projections
 *  const auto projections = OCL::RayCasterProjector().configureAndProject(setup, phantom);
 *
 *  // create the SimpleBackprojector
 *  OCL::SimpleBackprojector rec;
 *
 *  // configure the acquisition geometry and reconstruct the data
 *  // note: we reconstruct a volume slightly larger than 'phantom'
 *  bool ok;
 *  const auto recVol = rec.configureAndReconstruct(setup, projections,
 *                                                  VoxelVolume<float>::cube(128, 1.0f, 0.0f), &ok);
 *  qInfo() << ok; // output: true
 *
 *  // visualize the result (note: requires 'gui_widgets.pri' submodule)
 *  gui::plot(recVol);
 * \endcode
 * ![Backprojection result from the above example.](simple_backprojector.png)
 */
class SimpleBackprojector : public BackprojectorBase
{
    CTL_TYPE_ID(10)

public:
    explicit SimpleBackprojector(WeightingType weightType = BackprojectorWeighting::GeometryFactors);

    // BackprojectorBase interface
    float weighting(uint view, uint module) const override;

    // AbstractReconstructor interface
    bool reconstructToPlain(const ProjectionDataView& projections,
                            VoxelVolume<float>& targetVolume) override;

    // SerializationInterface interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;
    QVariant toVariant() const override;

    // setters
    void setVolumeSubsamplingFactor(uint volumeSubsamplingFactor);

private:
    uint _volumeSubsamplingFactor = 1;

    cl::Kernel* initOpenCLKernels() const;
};

} // namespace OCL
} // namespace CTL

#endif // CTL_SIMPLEBACKPROJECTOR_H
