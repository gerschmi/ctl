#include "sfpbackprojector.h"
#include "acquisition/geometryencoder.h"
#include "components/abstractdetector.h"
#include "img/voxelvolume.h"
#include "ocl/clfileloader.h"
#include "ocl/openclconfig.h"

#include <QMetaEnum>

const std::string CL_KERNEL_NAME_BACKPROJECT = "backprojector_sfp"; //!< name of the OpenCL kernel function
const std::string CL_FILE_NAME_TR = "recon/backprojection_tr.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT = "recon/backprojection_tt.cl"; //!< path to .cl file
const std::string CL_FILE_NAME_TT_GEN = "recon/backprojection_tt_generic.cl"; //!< path to .cl file
const std::string CL_PROGRAM_NAME_TR = "SFPProjector_tr";
const std::string CL_PROGRAM_NAME_TT = "SFPProjector_tt";
const std::string CL_PROGRAM_NAME_TT_GEN = "SFPProjector_tt_gen";

namespace CTL {
namespace OCL {

DECLARE_SERIALIZABLE_TYPE(SFPBackprojector)

namespace  {

cl::Kernel* getKernel(SFPBackprojector::FootprintType footprintType);

} // unnamed namspace

/*!
 * \brief Constructs an SFPBackprojector instance with footprint type \a footprintType and weighting
 * \a weightType.
 *
 * For more details on the weighting, please refer to the documentation of BackprojectorBase.
 */
SFPBackprojector::SFPBackprojector(FootprintType footprintType, WeightingType weightType)
    : BackprojectorBase(initOpenCLKernels(footprintType), weightType)
    , _fpType(footprintType)
{
}

/*!
 * \brief Transfers angle correction data for the specified data block (i.e. views from
 * \a startView to \a startView + \a nbViews).
 */
void SFPBackprojector::transferAdditionalData(uint startView, uint nbViews)
{
    const auto detector   = setup().system()->detector();
    const auto viewDim    = detector->nbPixelPerModule();
    const auto nbModules  = detector->nbDetectorModules();
    const auto aziBufSize = nbViews * nbModules * viewDim.width()  * sizeof(float);
    const auto polBufSize = nbViews * nbModules * viewDim.height() * sizeof(float);
    const auto aziStart   = nbModules * viewDim.width()  * startView;
    const auto polStart   = nbModules * viewDim.height() * startView;

    queue().enqueueWriteBuffer(_aziBuffer, CL_FALSE, 0, aziBufSize,
                               _angleCorrAzi.data() + aziStart);
    queue().enqueueWriteBuffer(_polBuffer, CL_FALSE, 0, polBufSize,
                               _angleCorrPol.data() + polStart);
}

/*!
 * \brief Returns the weighting factor for data from \a module in \a view.
 *
 * Uses BackprojectorBase::weighting() to request a potential custom weighting factor for \a view,
 * \a module. If weighting type is GeometryFactors, the custom weight is the final return value.
 * This is due to the fact that the implemented backprojection routines already include a geometry
 * factor based weighting. If weighting type is DistanceWeightsOnly, the weighting factor will be
 * divided by the result of `precomputedGeometryFactor(view, module)`.
 *
 * \sa precomputedGeometryFactor()
 */
float SFPBackprojector::weighting(uint view, uint module) const
{
    const auto customWeight = BackprojectorBase::weighting(view, module);

    return weightingType() == WeightingType::DistanceWeightsOnly
            ? customWeight / precomputedGeometryFactor(view, module)
            : customWeight;
}

/*!
 * \brief Initializes all OpenCL kernels and returns a pointer to the kernel for \a footprintType.
 *
 * This adds kernels for all three footprint types to the OpenCLConfig; then requests the kernel
 * for \a footprintType (this includes its compilation) and returns the result.
 */
cl::Kernel* SFPBackprojector::initOpenCLKernels(FootprintType footprintType) const
{
    try // OCL exception catching
    {
        auto& oclConfig = OCL::OpenCLConfig::instance();
        // general checks
        if(!oclConfig.isValid())
            throw std::runtime_error("OpenCLConfig is not valid");

        auto addKernel = [&oclConfig] (const std::string& clFileName, const std::string& clProgramName)
        {
            if(oclConfig.kernelExists(CL_KERNEL_NAME_BACKPROJECT, clProgramName))
                return;

            // load source code from file
            ClFileLoader clFile(clFileName);
            if(!clFile.isValid())
                throw std::runtime_error(clFileName + "\nis not readable");
            const auto clSourceCode = clFile.loadSourceCode();
            // add kernel to OCLConfig
            oclConfig.addKernel(CL_KERNEL_NAME_BACKPROJECT, clSourceCode, clProgramName);
        };

        // add kernels
        addKernel(CL_FILE_NAME_TR, CL_PROGRAM_NAME_TR);
        addKernel(CL_FILE_NAME_TT, CL_PROGRAM_NAME_TT);
        addKernel(CL_FILE_NAME_TT_GEN, CL_PROGRAM_NAME_TT_GEN);

        // Create kernel
        return getKernel(footprintType);

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error: " << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }
}

/*!
 * \brief Sets the footprint shape to be used in backprojection to \a footprintType.
 *
 * Three different footprint types are available:
 * - TR: trapezoid-rectangle
 * - TT: trapezoid-trapezoid
 * - TT_Generic: more flexible, but computationally more expensive, version of TT
 *
 * See detailed class description for examples of the different footprint shapes.
 */
void SFPBackprojector::setFootprintType(SFPBackprojector::FootprintType footprintType)
{
    _fpType = footprintType;
    BackprojectorBase::setBacksmearKernel(getKernel(footprintType));
}

/*!
 * \brief Returns the type of footprint shape currently used in backprojection.
 */
SFPBackprojector::FootprintType SFPBackprojector::footprintType() const
{
    return _fpType;
}

/*!
 * \copybrief AbstractReconstructor::parameter
 *
 * Returns a QVariantMap including the following (key, value)-pair:
 *
 * - ("footprint type", [string] name of the footprint type (TR, TT, or TT_Generic).
 */
QVariant SFPBackprojector::parameter() const
{
    QVariantMap ret = BackprojectorBase::parameter().toMap();

    ret.insert(QStringLiteral("footprint type"), metaEnum().valueToKey(int(footprintType())));

    return ret;
}

/*!
 * \copybrief AbstractReconstructor::setParameter
 *
 * The passed \a parameter must be a QVariantMap containing the following (key, value)-pair:
 *
 * - ("footprint type", [string] name of the footprint type (TR, TT, or TT_Generic)).
 *
 * Note that it is strongly discouraged to use this method to set individual parameters of the
 * instance. Please consider using individual setter methods for that purpose.
 */
void SFPBackprojector::setParameter(const QVariant& parameter)
{
    BackprojectorBase::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("footprint type")))
    {
        const auto typeStr = parMap.value(QStringLiteral("footprint type")).toString();
        int type = metaEnum().keyToValue(qPrintable(typeStr));
        if(type == -1)
        {
            qWarning() << "SFPBackprojector::setParameter: Incorrect footprint type specified. "
                          "(valid options: [TR | TT | TT_Generic]). "
                          "Using default value (TR).";
            type = TR;
        }

        setFootprintType(FootprintType(type));
    }
}

/*!
 * \copydoc BackprojectorBase::toVariant
 *
 * In addition to any base class information, this introduces a (key,value)-pair:
 * ("#", [string] this class' name).
 */
QVariant SFPBackprojector::toVariant() const
{
    QVariantMap ret = BackprojectorBase::toVariant().toMap();

    ret.insert(QStringLiteral("#"), SFPBackprojector::staticMetaObject.className());

    return ret;
}

/*!
 * \brief Reconstructs projection data \a projections into \a targetVolume and returns \c true
 * on success.
 *
 * \copydetails BackprojectorBase::reconstructToPlain
 *
 * Before the actual backprojection procedure, angle corrections (also called amplitude
 * approximation) are computed based on the setup that has been configured (see configure()). In
 * case of an anisotropic resolution of \a targetVolume in the xy-plane, an additional correction
 * is applied to compensate for the effect of anisotropy. The computed correction values are then
 * transfered to the OpenCL device (this includes creation of the necessary `cl::Buffer` objects).
 * Ultimately, uses BackprojectorBase::reconstructToPlain() to perform the backprojection.
 */
bool SFPBackprojector::reconstructToPlain(const ProjectionDataView& projections,
                                          VoxelVolume<float>& targetVolume)
{
    // geometry (projection matrices) and angle correction
    const auto pMats = GeometryEncoder::encodeSubsetGeometry(setup(), projections.viewIds());
    const auto nbPixel = setup().system()->detector()->nbPixelPerModule();

    _angleCorrAzi = determineAzimutCorrection(nbPixel, pMats);
    _angleCorrPol = determineThetaCorrection(nbPixel, pMats);

    if(!qFuzzyCompare(targetVolume.voxelSize().x, targetVolume.voxelSize().y)) // x and y sizes not equal
        performAnisotropyCorrection( nbPixel, pMats , { targetVolume.voxelSize().x,
                                                        targetVolume.voxelSize().y,
                                                        targetVolume.voxelSize().z } );

    createAngleCorrBuffers();
    /*
     *   ...,
     *   global const float* restrict angleCorrAzi,    --> arg 9
     *   global const float* restrict angleCorrPol     --> arg 10
     */
    backsmearKernel()->setArg( 8, dummyImage3D()); // required for compatibility with Intel OCL driver
    backsmearKernel()->setArg( 9, _aziBuffer);
    backsmearKernel()->setArg(10, _polBuffer);

    return BackprojectorBase::reconstructToPlain(projections, targetVolume);
}

/*!
 * \brief Computes and returns the angle correction in azimuthal direction.
 *
 * Computation is based on the geometry encoded in the projection matrices \a pMats and a detector
 * with \a nbPixels pixels. The correction term corresponds to formula (37) in the paper by
 * Long et al. (full reference in detailed class description), except for the voxel size
 * \f$ \Delta_1 \f$, which is considered later in the backprojection kernel.
 *
 * Returns a vector containing one correction value per detector channel (i.e. s-direction) for
 * each module in each view encoded in \a pMats; concatenated in that particular order.
 */
QVector<float> SFPBackprojector::determineAzimutCorrection(const QSize& nbPixels, const FullGeometry& pMats)
{
    const auto detWidth  = static_cast<uint>(nbPixels.width());
    const auto nbViews   = pMats.size();
    const auto nbModules = (nbViews > 0) ? pMats.first().size() : 0u;

    QVector<float> angleCorrAzi(detWidth * nbViews * nbModules);

    uint proj = 0;
    for(uint view = 0; view < nbViews; ++view)
        for(uint mod = 0; mod < nbModules; ++mod)
        {
            for(uint s = 0; s < detWidth; ++s)   // s-dependent fan-correction of phi (by means of gamma)
            {
                const float ph = phi(s, pMats.at(view).at(mod), nbPixels.height()); //beta-gamma(s);
                angleCorrAzi[s + detWidth * proj] = 1.0f / std::max(std::abs(std::cos(ph)), std::abs(std::sin(ph)));
            }
            ++proj;
        }

    return angleCorrAzi;
}

/*!
 * \brief Computes and returns the angle correction in polar direction.
 *
 * Computation is based on the geometry encoded in the projection matrices \a pMats and a detector
 * with \a nbPixels pixels. The correction term corresponds to formula (35) in the paper by
 * Long et al. (full reference in detailed class description), except that we also use the maximum
 * (of the norm) of sine and cosine values of the angle similar to the formula for the azimuthal
 * correction (Eq. (37)) here.
 *
 * Returns a vector containing one correction value per detector row (i.e. t-direction) for each
 * module in each view encoded in \a pMats; concatenated in that particular order.
 */
QVector<float> SFPBackprojector::determineThetaCorrection(const QSize& nbPixels, const FullGeometry& pMats)
{
    const auto detHeight = static_cast<uint>(nbPixels.height());
    const auto nbViews   = pMats.size();
    const auto nbModules = (nbViews > 0) ? pMats.first().size() : 0u;

    QVector<float> angleCorrPol(detHeight * nbViews * nbModules);

    uint proj = 0;
    for(uint view = 0; view < nbViews; ++view)
        for(uint mod = 0; mod < nbModules; ++mod)
        {
            for(uint t = 0; t < detHeight; ++t)
            {
                const float th = theta(t, pMats.at(view).at(mod), nbPixels.width());
                angleCorrPol[t + detHeight * proj] = 1.0f / std::max(std::abs(std::cos(th)), std::abs(std::sin(th)));
            }
            ++proj;
        }

    return angleCorrPol;
}

/*!
 * \brief Returns the azimuthal angle of the ray incident on detector column \a s in the geometry
 * encoded by \a pMat.
 *
 * Considers the central row of the detector (w.r.t. a total number of rows \a nbRows).
 */
float SFPBackprojector::phi(uint s, const mat::ProjectionMatrix& pMat, uint nbRows)
{
    const auto direction = pMat.directionSourceToPixel(double(s), std::round((nbRows - 1u) / 2.0));
    return static_cast<float>(std::atan2(direction.get<1>(), direction.get<0>()));
}

/*!
 * \brief Returns the polar angle of the ray incident on detector row \a t in the geometry encoded
 * by \a pMat.
 *
 * Considers the central channel of the detector (w.r.t. a total number of channels \a nbChannels).
 */
float SFPBackprojector::theta(uint t, const mat::ProjectionMatrix& pMat, uint nbChannels)
{
    const auto direction = pMat.directionSourceToPixel(std::round((nbChannels - 1u) / 2.0), double(t));
    return static_cast<float>(std::atan(direction.get<2>() /
                                        std::sqrt(std::pow(direction.get<1>(), 2) +
                                                  std::pow(direction.get<0>(), 2))));
}

/*!
 * \brief QMetaEnum object used to decode/encode subset order enum values.
 */
QMetaEnum SFPBackprojector::metaEnum()
{
    const auto& mo = SFPBackprojector::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("FootprintType");
    return mo.enumerator(idx);
}

/*!
 * \brief Creates the `cl::Buffer` objects required for angle correction data.
 *
 * Note: buffers are created in the current OpenCL context in the moment this method is called.
 */
void SFPBackprojector::createAngleCorrBuffers()
{
    auto& context = OCL::OpenCLConfig::instance().context();

    const auto aziBufSize  = _angleCorrAzi.size() * sizeof(float);
    const auto polBufSize  = _angleCorrPol.size() * sizeof(float);

    _aziBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, aziBufSize);
    _polBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, polBufSize);
}

/*!
 * \brief Applies a correction to the azimuthal angle correction values to compensate for
 * anisotropic voxel dimensions (in xy-plane).
 */
void SFPBackprojector::performAnisotropyCorrection(const QSize& nbPixels,
                                                   const FullGeometry& pMats,
                                                   mat::Matrix<3,1> voxelSize)
{
    const auto detWidth  = static_cast<uint>(nbPixels.width());
    const auto nbViews   = pMats.size();
    const auto nbModules = (nbViews > 0) ? pMats.first().size() : 0u;

    uint proj = 0;
    for(uint view = 0; view < nbViews; ++view)
        for(uint mod = 0; mod < nbModules; ++mod)
        {
            for(uint s = 0; s < detWidth; ++s)
            {
                const float ph = phi(s, pMats.at(view).at(mod), nbPixels.height()); //beta-gamma(s);
                const auto inv_sin_abs = std::abs(std::sin(ph)) / float(voxelSize.get<1>());
                const auto inv_cos_abs = std::abs(std::cos(ph)) / float(voxelSize.get<0>());
                const auto anisotropyScaling = std::max(inv_sin_abs, inv_cos_abs);

                _angleCorrAzi[s + detWidth * proj] = 1.0f / (anisotropyScaling * float(voxelSize.get<0>()));
            }

            ++proj;
        }

}

namespace  {

cl::Kernel* getKernel(SFPBackprojector::FootprintType footprintType)
{
    auto selectKernelsFor = [](const std::string& programName)
    {
        if(auto kernel = OCL::OpenCLConfig::instance().kernel(CL_KERNEL_NAME_BACKPROJECT, programName))
            return kernel;
        else
            throw std::runtime_error("SFPBackprojector: kernel pointer not valid");
    };

    switch(footprintType)
    {
    case CTL::OCL::SFPBackprojector::TR:
        return selectKernelsFor(CL_PROGRAM_NAME_TR);

    case CTL::OCL::SFPBackprojector::TT:
        return selectKernelsFor(CL_PROGRAM_NAME_TT);

    case CTL::OCL::SFPBackprojector::TT_Generic:
        return selectKernelsFor(CL_PROGRAM_NAME_TT_GEN);

    default:
        throw std::runtime_error("SFPBackprojector: invalid footprint type.");
    }
}

} // unnamed namspace

} // namespace OCL
} // namespace CTL
