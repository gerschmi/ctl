#include "reconstructiontest.h"

#include "recon/artreconstructor.h"
#include "recon/fdkreconstructor.h"
#include "recon/simplebackprojector.h"
#include "recon/simplebackprojectorcpu.h"
#include "recon/sfpbackprojector.h"

#include "acquisition/trajectories.h"
#include "components/flatpaneldetector.h"
#include "components/carmgantry.h"
#include "components/cylindricaldetector.h"
#include "components/xraylaser.h"
#include "io/basetypeio.h"
#include "io/nrrd/nrrdfileio.h"
#include "processing/errormetrics.h"
#include "processing/oclprojectionfilters.h"
#include "recon/oclregularizers.h"
#include "recon/orthogonalsubsetgenerator.h"

using namespace CTL;

void ReconstructionTest::initTestCase()
{
    const uint nbViews = 10;
    const QSize nbPixels(50,50);
    const QSizeF pixelSize(10.0f, 10.0f);
    SimpleCTSystem system(FlatPanelDetector(nbPixels, pixelSize),
                          CarmGantry(),
                          XrayLaser());

    _testSetup = AcquisitionSetup(system, nbViews);
    _testSetup.applyPreparationProtocol(protocols::ShortScanTrajectory(500.0f));

    _dummyProjections = ProjectionData(nbPixels.width(), nbPixels.height(), 1);

    SingleViewData vDat(nbPixels.width(), nbPixels.height());
    vDat.allocateMemory(1);
    vDat.fill(1.0f);

    for(uint v = 0; v < nbViews; ++v)
        _dummyProjections.append(vDat);

    _customWeights.resize(_dummyProjections.nbViews());
    for(auto& el : _customWeights)
        el = std::vector<float>(_dummyProjections.viewDimensions().nbModules, 1.337f);
}

void ReconstructionTest::testConsistencyChecks()
{
    OCL::SimpleBackprojector bp_gpu;
    SimpleBackprojectorCPU bp_cpu;
    ARTReconstructor iter_rec;

    iter_rec.setMaxNbIterations(1);

    auto recVol = VoxelVolume<float>::cube(20, 20.0f, 0.0f);

    std::vector<uint> sub(5);
    std::iota(sub.begin(), sub.end(), 2u);
    auto improperSetup = _testSetup.subset(sub);

    bool ok;
    // check missed call to configure
    ok = bp_cpu.reconstructTo(_dummyProjections, recVol);
    QVERIFY2(!ok, "SimpleBackprojectorCPU: missed detection of invalid setup (no configure())");

    ok = bp_gpu.reconstructTo(_dummyProjections, recVol);
    QVERIFY2(!ok, "OCL::SimpleBackprojector: missed detection of invalid setup (no configure())");

    ok = iter_rec.reconstructTo(_dummyProjections, recVol);
    QVERIFY2(!ok, "IterativeReconstructor: missed detection of invalid setup (no configure())");


    // check incompatible projection IDs (exceeding setup)
    bp_cpu.configure(improperSetup);
    ok = bp_cpu.reconstructTo(_dummyProjections, recVol);

    QVERIFY2(!ok, "SimpleBackprojectorCPU: missed detection of improper setup");

    bp_gpu.configure(improperSetup);
    ok = bp_gpu.reconstructTo(_dummyProjections, recVol);

    QVERIFY2(!ok, "OCL::SimpleBackprojector: missed detection of improper setup");

    iter_rec.configure(improperSetup);
    ok = iter_rec.reconstructTo(_dummyProjections, recVol);

    QVERIFY2(!ok, "IterativeReconstructor: missed detection of improper setup");


    // check incompatible module count
    improperSetup = _testSetup;
    improperSetup.system()->replaceDetector(new CylindricalDetector({10,10}, {1.0, 1.0}, 2, 0.1, 1.0));
    bp_cpu.configure(improperSetup);
    ok = bp_cpu.reconstructTo(_dummyProjections, recVol);

    QVERIFY2(!ok, "SimpleBackprojectorCPU: missed detection of improper module count");

    bp_gpu.configure(improperSetup);
    ok = bp_gpu.reconstructTo(_dummyProjections, recVol);

    QVERIFY2(!ok, "OCL::SimpleBackprojector: missed detection of improper module count");

    iter_rec.configure(improperSetup);
    ok = iter_rec.reconstructTo(_dummyProjections, recVol);

    QVERIFY2(!ok, "IterativeReconstructor: missed detection of improper module count");
}

void ReconstructionTest::testBackprojectionResults()
{
    OCL::SimpleBackprojector bp_gpu;
    SimpleBackprojectorCPU bp_cpu;

    bp_cpu.setInterpolate(true);

    // !!! CPU not yet updated to correct geometry factor weighting !!!
    //auto groundTruthCPU = io::BaseTypeIO<io::NrrdFileIO>().readVolume<float>("testData/backprojectedVol_old.nrrd");
    auto groundTruthGPU = io::BaseTypeIO<io::NrrdFileIO>().readVolume<float>("testData/backprojectedVol.nrrd");

    // CPU version
    bool ok;
    auto reco = bp_cpu.configureAndReconstruct(_testSetup, _dummyProjections, VoxelVolume<float>::cube(20, 20.0f, 0.0f), &ok);
    auto diff = metric::rL2(reco, groundTruthGPU);

    QVERIFY2(ok, "SimpleBackprojectorCPU: backprojection failed");
    QVERIFY2(diff < 0.001f, "SimpleBackprojectorCPU: incorrect result");

    // GPU version
    reco = bp_gpu.configureAndReconstruct(_testSetup, _dummyProjections, VoxelVolume<float>::cube(20, 20.0f, 0.0f), &ok);
    diff = metric::rL2(reco, groundTruthGPU);

    QVERIFY2(ok, "OCL::SimpleBackprojector: backprojection failed");
    QVERIFY2(diff < 0.001f, "OCL::SimpleBackprojector: incorrect result");
}

void ReconstructionTest::testSerialization()
{
    static const double eps = 1e-3;

    {
        qInfo() << "Test OCL::SimpleBackprojector (all default)";
        OCL::SimpleBackprojector recon;
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test OCL::SimpleBackprojector (custom weights + upsampling 2x)";
        OCL::SimpleBackprojector recon;
        recon.setCustomWeights(_customWeights);
        recon.setVolumeSubsamplingFactor(2u);
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test OCL::SFPBackprojector (all default)";
        OCL::SFPBackprojector recon;
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test OCL::SFPBackprojector (TT_Generic + distance weights)";
        OCL::SFPBackprojector recon;
        recon.setFootprintType(OCL::SFPBackprojector::FootprintType::TT_Generic);
        recon.setWeightingType(BackprojectorWeighting::DistanceWeightsOnly);
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test OCL::FDKReconstructor (all default)";
        OCL::FDKReconstructor recon;
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test OCL::FDKReconstructor (SFP + Hann)";
        OCL::FDKReconstructor recon;
        recon.useSFPBackprojector();
        recon.setRevisedParkerWeightQ(0.5f);
        recon.setProjectionFilter(new OCL::ApodizationFilter(OCL::ApodizationFilter::Hann));
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test OCL::FDKReconstructor (SFP + TT)";
        OCL::FDKReconstructor recon;
        recon.useSFPBackprojector(OCL::SFPBackprojector::TT);
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test ARTReconstructor (all default)";
        ARTReconstructor recon;
        QVERIFY(runSerializationCheck(recon) < eps);
    }
    {
        qInfo() << "Test ARTReconstructor (customized)";
        ARTReconstructor recon;
        recon.setRelaxation(0.01f);
        recon.setMinChangeInProjectionError(0.1f);
        recon.setMaxNbIterations(10);
        recon.setRegularizer(new OCL::TVRegularizer(1.0f));
        recon.setSubsetGenerator(new OrthogonalSubsetGenerator(3));
        QVERIFY(runSerializationCheck(recon) < eps);
    }
}
