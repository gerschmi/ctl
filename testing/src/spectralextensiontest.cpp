#include "spectralextensiontest.h"

#include "acquisition/trajectories.h"
#include "acquisition/preparesteps.h"
#include "components/allcomponents.h"
#include "io/den/den.h"
#include "processing/errormetrics.h"
#include "projectors/poissonnoiseextension.h"
#include "projectors/raycasterprojector.h"
#include "projectors/spectraleffectsextension.h"
#include "projectors/projectionpipeline.h"

using namespace CTL;

const bool ENABLE_INTERPOLATION_IN_RAYCASTER = true;
const bool ENABLE_IMAGESUPPORT_IN_RAYCASTER = true;

SpectralExtensionTest::SpectralExtensionTest()
    : _volume(SpectralVolumeData(VoxelVolume<float>(0,0,0)))
{
}

void SpectralExtensionTest::initTestCase()
{
    // assemble CT system
    SimpleCTSystem system(FlatPanelDetector(QSize(50, 50), QSizeF(1.0, 1.0)),
                          CarmGantry(1200.0),
                          XrayTube(QSizeF(1.0, 1.0), 80.0, 100000.0));

    auto photonsPerPixel = 100000.0;
    auto fluxAdjustFactor = photonsPerPixel / system.photonsPerPixelMean();

    _setup = AcquisitionSetup(system, 5);
    _setup.applyPreparationProtocol(protocols::ShortScanTrajectory(750.0));

    for(uint v = 0; v < _setup.nbViews(); ++v)
    {
        auto srcPrep = std::make_shared<prepare::XrayTubeParam>();
        srcPrep->setTubeVoltage(80.0 + 20.0/_setup.nbViews() * v);
        srcPrep->setMilliampereSeconds(fluxAdjustFactor*(10000.0 + 10000.0/_setup.nbViews() * v));
        _setup.view(v).addPrepareStep(srcPrep);
    }

    _volume = SpectralVolumeData::ball(40.0f, 0.5f, 1.0f,
                                       attenuationModel(database::Composite::Water));

    _compVol = CompositeVolume(_volume);
    _compVol.addSubVolume(SpectralVolumeData::ball(30.0f, 0.5f, 0.3f,
                                                   attenuationModel(database::Composite::Bone_Cortical)));

}

void SpectralExtensionTest::testSpectralExtension()
{
    // configure a projector and project volume
    OCL::RayCasterProjector* myProjector = new OCL::RayCasterProjector;
    myProjector->settings().interpolate = ENABLE_INTERPOLATION_IN_RAYCASTER;
    myProjector->settings().useImageTypes = ENABLE_IMAGESUPPORT_IN_RAYCASTER;

    ProjectionPipeline pipeline(myProjector);

    PoissonNoiseExtension* noiseExt = new PoissonNoiseExtension;
    noiseExt->setFixedSeed(1337u);

    SpectralEffectsExtension* spectralExt = new SpectralEffectsExtension;
    spectralExt->setSpectralSamplingResolution(15.0f);

    pipeline.appendExtension(noiseExt);
    pipeline.appendExtension(spectralExt);

    pipeline.configure(_setup);

    io::BaseTypeIO<io::DenFileIO> io;
    // Non-linear composite
    auto proj = pipeline.projectComposite(_compVol);
    auto groundTruth = io.readProjections("testData/spectralExtension/spectral_nonlin_composite.den");
    evaluateData(proj-groundTruth, "Non-linear composite");

    // Non-linear simple
    proj = pipeline.project(_volume);
    groundTruth = io.readProjections("testData/spectralExtension/spectral_nonlin_simple.den");
    evaluateData(proj-groundTruth, "Non-linear simple");

    // remove Poisson extension -> make nested projector linear
    pipeline.removeExtension(0);

    // Linear composite
    proj = pipeline.projectComposite(_compVol);
    groundTruth = io.readProjections("testData/spectralExtension/spectral_lin_composite.den");
    evaluateData(proj-groundTruth, "Linear composite");

    // Linear simple
    proj = pipeline.project(_volume);
    groundTruth = io.readProjections("testData/spectralExtension/spectral_lin_simple.den");
    evaluateData(proj-groundTruth, "Linear simple");
}

void SpectralExtensionTest::testNegativeVolume()
{
    // assemble CT system
    SimpleCTSystem system(FlatPanelDetector(QSize(50, 50), QSizeF(1.0, 1.0)),
                          CarmGantry(1200.0),
                          XrayTube(QSizeF(1.0, 1.0), 80.0, 2.0));
    system.addBeamModifier(new AttenuationFilter(database::Element::Al, 1.0f));

    AcquisitionSetup setup(system, 5);
    setup.applyPreparationProtocol(protocols::ShortScanTrajectory(750.0));

    // configure a projector and project volume
    OCL::RayCasterProjector* myProjector = new OCL::RayCasterProjector;
    myProjector->settings().interpolate = ENABLE_INTERPOLATION_IN_RAYCASTER;
    myProjector->settings().useImageTypes = ENABLE_IMAGESUPPORT_IN_RAYCASTER;

    ProjectionPipeline pipeline(myProjector);
    pipeline.appendExtension(new SpectralEffectsExtension);

    // negative volume
    const auto ball_neg = SpectralVolumeData::ball(40.0f, 0.5f, -1.0f,
                                                   attenuationModel(database::Composite::Water));
    auto projectionsNegative = pipeline.configureAndProject(setup, ball_neg);

    // projection result is expected to be negative (but finite -> no NaN/Inf)
    QVERIFY(projectionsNegative.max() < 0.0f);
    QVERIFY(std::all_of(projectionsNegative.cbegin(), projectionsNegative.cend(),
                        [] (const float& pix) { return std::isfinite(pix); } ));


    // composite case
    const auto ball = SpectralVolumeData::ball(40.0f, 0.5f, 1.0f,
                                               attenuationModel(database::Composite::Water));
    CompositeVolume compVol(ball, ball_neg);
    auto projectionsComp = pipeline.configureAndProject(setup, compVol);

    // projection result is expected to be zero
    QVERIFY(qFuzzyIsNull(projectionsComp.min()));
    QVERIFY(qFuzzyIsNull(projectionsComp.max()));
    QVERIFY(std::abs(projectionMean(projectionsComp)) < 1.0e-6f);
    QVERIFY(projectionVariance(projectionsComp) < 1.0e-6f);

    // append Poisson noise and reproject
    auto* poissonNoiseExtension = new PoissonNoiseExtension;
    poissonNoiseExtension->setFixedSeed(42);
    pipeline.appendExtension(poissonNoiseExtension);
    projectionsComp = pipeline.configureAndProject(setup, compVol);

    // mean should remain around zero
    QVERIFY(std::abs(projectionMean(projectionsComp)) < 1.0e-3f);
    QVERIFY(projectionVariance(projectionsComp) < 1.0e-3f);

    // swap Poisson and spectral extension and repeat testing
    pipeline.appendExtension(pipeline.takeExtension(0));
    projectionsComp = pipeline.configureAndProject(setup, compVol);

    QVERIFY(std::abs(projectionMean(projectionsComp)) < 1.0e-3f);
    QVERIFY(projectionVariance(projectionsComp) < 1.0e-3f);
}

void SpectralExtensionTest::makeTestDataGroundTruth() const
{
    // configure a projector and project volume
    OCL::RayCasterProjector* myProjector = new OCL::RayCasterProjector;
    myProjector->settings().interpolate = ENABLE_INTERPOLATION_IN_RAYCASTER;
    myProjector->settings().useImageTypes = ENABLE_IMAGESUPPORT_IN_RAYCASTER;

    ProjectionPipeline pipeline(myProjector);

    PoissonNoiseExtension* noiseExt = new PoissonNoiseExtension;
    noiseExt->setFixedSeed(1337u);

    SpectralEffectsExtension* spectralExt = new SpectralEffectsExtension;
    spectralExt->setSpectralSamplingResolution(15.0f);

    pipeline.appendExtension(noiseExt);
    pipeline.appendExtension(spectralExt);

    pipeline.configure(_setup);

    io::BaseTypeIO<io::DenFileIO> io;
    // Non-linear composite
    auto proj = pipeline.projectComposite(_compVol);
    io.write(proj, "testData/spectral_nonlin_composite.den");

    // Non-linear simple
    proj = pipeline.project(_volume);
    io.write(proj, "testData/spectral_nonlin_simple.den");

    // remove Poisson extension -> make nested projector linear
    pipeline.removeExtension(0);

    // Linear composite
    proj = pipeline.projectComposite(_compVol);
    io.write(proj, "testData/spectral_lin_composite.den");

    // Linear simple
    proj = pipeline.project(_volume);
    io.write(proj, "testData/spectral_lin_simple.den");
}

double SpectralExtensionTest::projectionMean(const ProjectionData &projections) const
{
    const double total = std::accumulate(projections.cbegin(), projections.cend(), 0.0f);

    return total / static_cast<double>(projections.dimensions().totalNbElements());
}

double SpectralExtensionTest::projectionVariance(const ProjectionData &projections) const
{
    const auto localMean = projectionMean(projections);

    auto squaredDiff = [localMean] (const float& sum, const float& pix)
    {
        return sum + (pix - localMean) * (pix - localMean);
    };

    const double totalVariance = std::accumulate(projections.cbegin(), projections.cend(), 0.0f,
                                                 squaredDiff);

    return totalVariance / static_cast<double>(projections.dimensions().totalNbElements());
}

void SpectralExtensionTest::evaluateData(const ProjectionData& difference,
                                         const QString& caseName) const
{
    const auto mean = projectionMean(difference);
    const auto var  = projectionVariance(difference);
    qInfo() << mean << var;
    QVERIFY2(std::abs(mean) < 0.01, qPrintable(caseName + "failed"));
    QVERIFY2(var < 0.01, qPrintable(caseName + "failed"));
}
