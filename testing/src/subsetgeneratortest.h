#ifndef SUBSETGENERATORTEST_H
#define SUBSETGENERATORTEST_H

#include "acquisition/acquisitionsetup.h"
#include "img/projectiondataview.h"
#include "io/jsonserializer.h"

#include <QtTest>

class SubsetGeneratorTest : public QObject
{
    Q_OBJECT

public:
    SubsetGeneratorTest();

private Q_SLOTS:
    void initTestCase();
    void testSerialization();


private:
    CTL::AcquisitionSetup _setup;
    CTL::ProjectionData _dummyData;

    template<class GeneratorType>
    bool runSerializationCheck(GeneratorType& gen);
};

template<class GeneratorType>
bool SubsetGeneratorTest::runSerializationCheck(GeneratorType& gen)
{
    gen.setData(_dummyData, _setup);
    gen.setRandomGeneratorSeed(1234);

    // generate some data to change RNG state
    gen.generateSubsets(2);

    CTL::JsonSerializer ser;
    ser.serialize(gen, "generator.json");

    auto obj = ser.deserialize<GeneratorType>("generator.json");
    obj->setData(_dummyData, _setup);
    const auto subsetsDeserializedGenerator = obj->generateSubsets(2);
    const auto subsetsOriginalGenerator     = gen.generateSubsets(2);

    return subsetsOriginalGenerator == subsetsDeserializedGenerator;
}

#endif // SUBSETGENERATORTEST_H
