#ifndef DATAMODELTEST_H
#define DATAMODELTEST_H

#include "models/abstractdatamodel.h"
#include "models/abstractdatamodel2d.h"

#include <QtTest>

class DataModelTest : public QObject
{
    Q_OBJECT

public:
    DataModelTest();

private Q_SLOTS:
    void initTestCase();
    void testSerialization1DModels();
    void testSerialization2DModels();
    void testSerializationModelOperations1D();
    void testSerializationModelOperations2D();
    void testNumericalIntegrationDecorator();

private:
    QVector<float> _samples1D;
    QVector<float> _samples1DPositive;
    QVector<QPair<float, float>> _samples2D;


    void runSerializationTest(std::shared_ptr<const CTL::AbstractDataModel> model,
                              const QVector<float>& samplingPts) const;
    void runSerializationTest(std::shared_ptr<const CTL::AbstractDataModel2D> model,
                              const QVector<QPair<float, float>>& samplingPts) const;
};

#endif // DATAMODELTEST_H
