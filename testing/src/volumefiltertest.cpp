#include "volumefiltertest.h"

#include <random>

// volume filters
#include "recon/regularizers.h"
#include "recon/oclregularizers.h"

using namespace CTL;

VolumeFilterTest::VolumeFilterTest()
    : _dummyVol(10, 10, 10)
{
}

void VolumeFilterTest::initTestCase()
{
    _dummyVol.allocateMemory();
    std::mt19937 rng;
    rng.seed(1234);
    std::uniform_real_distribution<float> dist(0.0f, 2.0f);
    std::generate(_dummyVol.begin(), _dummyVol.end(), [&dist, &rng](){ return dist(rng); });
}

void VolumeFilterTest::testSerialization()
{
    static const double eps = 1e-3;

    qInfo() << "Test OCL::HuberRegularizer";
    OCL::HuberRegularizer huber_ocl(3.2f, 0.5f, 2.3f, 1.7f, 0.3f);
    QVERIFY(runSerializationCheck(huber_ocl) < eps);

    qInfo() << "Test OCL::TVRegularizer";
    OCL::TVRegularizer tv_ocl(3.2f);
    QVERIFY(runSerializationCheck(tv_ocl) < eps);

    qInfo() << "Test HuberRegularizer";
    HuberRegularizer huber_cpu(3.2f, 0.5f, 2.3f, 1.7f, 0.3f);
    QVERIFY(runSerializationCheck(huber_cpu) < eps);

    qInfo() << "Test TVRegularizer";
    TVRegularizer tv_cpu(3.2f);
    QVERIFY(runSerializationCheck(tv_cpu) < eps);

    qInfo() << "Test MedianFilterRegularizer";
    MedianFilterRegularizer median_cpu(1.3f, MedianFilterRegularizer::Box3x3x3);
    QVERIFY(runSerializationCheck(median_cpu) < eps);

    qInfo() << "Test IdentityRegularizer";
    IdentityRegularizer identity_cpu;
    QVERIFY(runSerializationCheck(identity_cpu) < eps);
}
