#ifndef RECONSTRUCTIONTEST_H
#define RECONSTRUCTIONTEST_H

#include <QtTest>

#include "acquisition/acquisitionsetup.h"
#include "img/projectiondata.h"
#include "img/voxelvolume.h"
#include "io/jsonserializer.h"
#include "processing/errormetrics.h"
#include "processing/genericoclprojectionfilter.h"

class ReconstructionTest : public QObject
{
    Q_OBJECT

public:
    ReconstructionTest() = default;

private Q_SLOTS:
    void initTestCase();
    void testConsistencyChecks();
    void testBackprojectionResults();
    void testSerialization();

private:
    CTL::AcquisitionSetup _testSetup;
    CTL::ProjectionData _dummyProjections{0,0,0};
    std::vector<std::vector<float>> _customWeights;

    template <class Reco>
    double runSerializationCheck(Reco& reco);
};

template <class Reco>
double ReconstructionTest::runSerializationCheck(Reco& reconstructor)
{
    auto recVol = CTL::VoxelVolume<float>::cube(20, 20.0f, 0.0f);

    // do dummy reconstruction to change potential RNG states
    auto dummyReco = reconstructor.configureAndReconstruct(_testSetup, _dummyProjections, recVol);

    // serialize
    CTL::JsonSerializer ser;
    ser.serialize(reconstructor, "reconstructor.json");

    // deserialize
    auto obj = ser.deserialize<Reco>("reconstructor.json");

    // reconstruct with the original reconstructor and the deserialized object
    auto reco1 = reconstructor.configureAndReconstruct(_testSetup, _dummyProjections, recVol);
    auto reco2 = obj->configureAndReconstruct(_testSetup, _dummyProjections, recVol);

    return CTL::metric::L2(reco1, reco2);
}

#endif // RECONSTRUCTIONTEST_H
