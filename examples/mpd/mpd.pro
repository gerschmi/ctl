QT -= gui

CONFIG += console
CONFIG -= app_bundle

SOURCES += main.cpp

# CTL modules
include(../../modules/ctl.pri)
