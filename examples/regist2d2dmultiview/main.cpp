#include "ctl.h"
#include "ctl_ocl.h"
#include "ctl_nlopt.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QElapsedTimer>

namespace CTL { class AbstractNloptRegistration2D2D; namespace NLOPT { class GrangeatRegistration2D2D; } }

namespace {

struct InputData
{
    std::vector<CTL::Chunk2D<float>> imgs;
    std::vector<CTL::mat::ProjectionMatrix> mats;
};

struct Output
{
    bool R;
    bool H;
    bool P;
};

struct OptimizerSettings
{
    // parameters to optimize
    double lowerBounds = -30.0;
    double upperBounds = 30.0;
    // double translationBounds    = 30.0; // [mm]
    // double rotationBounds       = 30.0; // [deg]
    // double focalLengthBounds    =  0.0; // [x or y detector pixels] (source to detector distance)
    // double principalPointBounds =  0.0; // [x or y detector pixels]

    // algorithm details
    CTL::imgproc::NormalizedGemanMcClure metric = 50.0;
    double subSampling = 1.0;
    bool global = false;

    // truncation-related parameters
    float maxLineDistance = 1.0f;
    float truncationThreshold = std::numeric_limits<float>::min();
};

std::unique_ptr<CTL::io::AbstractProjectionMatrixIO> projMatIO(const QString& fn);
std::unique_ptr<CTL::io::AbstractProjectionDataIO> projDataIO(const QString& fn);
OptimizerSettings parseOptimizerSettings(const QCommandLineParser& parser);
CTL::NLOPT::GrangeatRegistration2D2D getOptimizer(const OptimizerSettings& settings);
QString getFilenameSuffix(const OptimizerSettings& settings);
Output determineOutput(const QCommandLineParser& parser);
uint getNbViews(CTL::io::AbstractProjectionDataIO& io, const QString& fn);
uint checkDimensionality(CTL::io::AbstractProjectionDataIO& io, const QString& fn, const CTL::FullGeometry& pMats);
InputData readInputData(const QString& fnImgs, const QString& fnMats, uint cropPixels);

} // unnamed namespace

int main(int argc, char *argv[])
try
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationVersion(QStringLiteral("1.0"));

    QCommandLineParser parser;
    parser.setApplicationDescription("Grangeat-based 2D/2D registration.\n"
                                     "It performs a registration of one pair of projection "
                                     "matrices/images (P/I) to another target pair of projection "
                                     "matrices/images (Q/J). The two pairs neither require to have "
                                     "the same geometry nor the same number of matrices/images. "
                                     "It is only assumed that each pair of matrices/images is "
                                     "self-consistent (data consistency conditions are satisfied "
                                     "within each pair of matrices/images).");
    parser.addVersionOption();
    parser.addHelpOption();

    parser.addOptions({
        { { "p", "source-proj-mats" }, "File with initial projection matrices corresponding to the "
                                     "projection images passed to the 'I' option at location "
                                     "<path> [NRRD/DEN].", "path" },
        { { "i", "source-proj-imgs" }, "File with projection images (to be registered) at location "
                                     "<path> [NRRD/DEN].", "path" },
        { { "q", "target-proj-mats" },   "File with target projection matrices corresponding to the "
                                     "projection images passed to the 'J' option at location "
                                     "<path> [NRRD/DEN].", "path" },
        { { "j", "target-proj-imgs" },   "File with target projection images at location "
                                     "<path> [NRRD/DEN].", "path" },

        { { "g", "global" },         "Enable global optimization." },
        { { "s", "angular-sub-sampling" }, "Fraction of the sampled subset of available values. "
                                     "Default: 1.0", "fraction" },

        { { "l", "lower-bound" }, "Lower bound of opimization [mm/deg]. Default: -30.0.", "value" },
        { { "u", "upper-bound" }, "Upper bound of opimization [mm/deg]. Default: 30.0.", "value" },
    //  { { "a", "trans" },        "Lower/upper bound of translation opimization [mm]. "
    //                             "Default: 30.0", "value" },
    //  { { "b", "rot" },          "Lower/upper bound of rotation opimization [deg]. "
    //                             "Default: 30.0", "value" },
    //  { { "c", "focal-length" }, "Lower/upper bound of focal length opimization [pixel]. "
    //                             "Default: 0.0", "value" },
    //  { { "d", "princ-point" },  "Lower/upper bound of principal point opimization [pixel]. "
    //                             "Default: 0.0", "value" },

        { { "m", "gmc-metric" }, "Parameter of the GMC metric. Default: 50.0", "value" },

        { { "t", "max-line-distance" }, "Crop maximum used line distance (0,1], e.g. in presence "
                                        "of truncation. Only active if set a <value> less than 1. "
                                        "Default: 1.0", "value" },
        { { "T", "truncation-threshold" }, "Threshold for boundary extinctions for ignoring "
                                           "truncated lines on the detector. Only active if "
                                           "set a <value> greater than 0 (e.g. '-T 4.0'). "
                                           "Default: 0.0", "value" },

        { { "c", "crop" }, "Symmetrically crop input projection images [number of pixels]. Default: 0", "number" },

        { { "J", "device-number" }, "Use only a specific OpenCL device with index <number>.", "number" },

        { { "R", "output-rigid" }, "Outputs estimated rigid parameters, i.e. rotational axis r and translation vector t. "
                                   "This is the default if no output flag is specified [DEN]." },
        { { "H", "output-homo" }, "Outputs estimated homography H ~ [R|t], with rotation matrix R (from rotationals axis r) "
                                  "and translation vector t [DEN]." },
        { { "P", "output-proj-mat" }, "Outputs estimated projection matrix P ~ P0 * H [DEN]." },
        { { "A", "output-all" }, "Outputs all three of the above results, i.e. same as -RHP." },
        { { "o", "output" }, "Output directory. Default: '.'", "path" }
    });

    // Process the actual command line arguments given by the user
    parser.process(app);

    // Check integrity
    if(!(   parser.isSet(QStringLiteral("i")) && parser.isSet(QStringLiteral("p"))
         && parser.isSet(QStringLiteral("j")) && parser.isSet(QStringLiteral("q")) ))
    {
        qCritical("Parameters p, q, i and j are manadatory. Pass '--help' for further information.");
        return -1;
    }
    // Check parameter constrains (not completed TBD!)
    if(parser.isSet(QStringLiteral("t")))
    {
        if(   parser.value(QStringLiteral("t")).toFloat() <= 0.0f
           || parser.value(QStringLiteral("t")).toFloat()  > 1.0f)
        {
            qCritical("Value for '-t' must be a floating point number within the interval (0,1].");
            return -1;
        }
    }

    // Use only specific device
    if(parser.isSet(QStringLiteral("J")))
        CTL::OCL::OpenCLConfig::instance().setDevices( {
            CTL::OCL::OpenCLConfig::instance().devices().at(parser.value("J").toUInt()) } );

    // Input paths
    const auto fnSourceMats = parser.value(QStringLiteral("p"));
    const auto fnSourceImgs = parser.value(QStringLiteral("i"));
    const auto fnTargetMats = parser.value(QStringLiteral("q"));
    const auto fnTargetImgs = parser.value(QStringLiteral("j"));

    // Read input data
    qInfo("Read data...");
    const auto nbPixels2Crop = parser.value("c").toUInt();
    const auto sourceData = readInputData(fnSourceImgs, fnSourceMats, nbPixels2Crop);
    if(sourceData.imgs.empty())
        return -1;
    const auto targetData = readInputData(fnTargetImgs, fnTargetMats, nbPixels2Crop);
    if(targetData.imgs.empty())
        return -1;

    // Init optimizer
    const auto settings = parseOptimizerSettings(parser);
    auto reg = getOptimizer(settings);

    // start optimization
    qInfo("Start optimization...");
    QElapsedTimer time; time.start();
    const auto optHomo = reg.optimize(sourceData.imgs, sourceData.mats,
                                      targetData.imgs, targetData.mats);
    const auto rotAxis = CTL::mat::rotationAxis(optHomo.subMat<0,2, 0,2>());
    const auto translVec = optHomo.subMat<0,2, 3,3>();

    qInfo().noquote() << "\nRegistration result:";
    qInfo().noquote() << QString::fromStdString((rotAxis*180./CTL::PI).info(  "rotational axis [deg] "));
    qInfo().noquote() << QString::fromStdString(translVec.info("translation vec. [mm] "));
    qInfo().noquote() << "----------------\n";
    qInfo() << "Elapsed time [ms]:" << time.elapsed();

    // Save result                
    auto outPath = QStringLiteral(".");
    if(parser.isSet(QStringLiteral("o")))
    {
        outPath = parser.value(QStringLiteral("o"));
        if(outPath.right(1) == QLatin1String("/") || outPath.right(1) == QLatin1String("\\"))
            outPath.chop(1);
    }

    // determine output format
    const auto output = determineOutput(parser);
    const auto fnSuffix = getFilenameSuffix(settings);
    // save estimated rigid parameters (rotational axes and translation vectors)
    if(output.R)
    {
        CTL::io::den::save(CTL::mat::toQVector(rotAxis), outPath + "/reg_rot" + fnSuffix, 1, 3);
        CTL::io::den::save(CTL::mat::toQVector(translVec), outPath + "/reg_trans" + fnSuffix, 1, 3);
    }
    // save estimated homographies
    if(output.H)
    {
        CTL::io::den::save(CTL::mat::toQVector(optHomo), outPath + "/homo" + fnSuffix, 4, 4, 1);
    }
    // save estimated projection matrices
    if(output.P)
    {
        CTL::FullGeometry estPmats;
        for(const auto& pMat : sourceData.mats)
            estPmats.append(CTL::SingleViewGeometry{
                                { CTL::ProjectionMatrix(pMat * optHomo).normalized() } });
        CTL::io::BaseTypeIO<CTL::io::DenFileIO>{}.write(estPmats, outPath + "/est_pmat" + fnSuffix);
    }

    return 0;
} catch (const std::exception& e)
{
    qCritical() << "exception thrown:" << e.what();
    return -1;
}

namespace {

using namespace CTL::io;
using NrrdIO = BaseTypeIO<NrrdFileIO>;
using DenIO = BaseTypeIO<DenFileIO>;

bool isNrrd(const QString& fn)
{
    return NrrdIO{}.metaInfo(fn).contains(QStringLiteral("nrrd version"));
}

std::unique_ptr<AbstractProjectionMatrixIO> projMatIO(const QString& fn)
{
    std::unique_ptr<AbstractProjectionMatrixIO> ret;

    if(isNrrd(fn))
        ret = NrrdIO::makeProjectionMatrixIO();
    else
        ret = DenIO::makeProjectionMatrixIO();

    return ret;
}

std::unique_ptr<AbstractProjectionDataIO> projDataIO(const QString& fn)
{
    std::unique_ptr<AbstractProjectionDataIO> ret;

    if(isNrrd(fn))
        ret = NrrdIO::makeProjectionDataIO();
    else
        ret = DenIO::makeProjectionDataIO();

    return ret;
}

OptimizerSettings parseOptimizerSettings(const QCommandLineParser& parser)
{
    OptimizerSettings ret;

    if(parser.isSet(QStringLiteral("m")))
        ret.metric = parser.value(QStringLiteral("m")).toDouble();

    if(parser.isSet(QStringLiteral("l")))
        ret.lowerBounds = parser.value(QStringLiteral("l")).toDouble();

    if(parser.isSet(QStringLiteral("u")))
        ret.upperBounds = parser.value(QStringLiteral("u")).toDouble();

//    if(parser.isSet(QStringLiteral("a")))
//        ret.translationBounds = parser.value(QStringLiteral("a")).toDouble();

//    if(parser.isSet(QStringLiteral("b")))
//        ret.rotationBounds = parser.value(QStringLiteral("b")).toDouble();

//    if(parser.isSet(QStringLiteral("c")))
//        ret.focalLengthBounds = parser.value(QStringLiteral("c")).toDouble();

//    if(parser.isSet(QStringLiteral("d")))
//        ret.principalPointBounds = parser.value(QStringLiteral("d")).toDouble();

    if(parser.isSet(QStringLiteral("t")))
        ret.maxLineDistance = parser.value(QStringLiteral("t")).toFloat();

    if(parser.isSet(QStringLiteral("T")))
        ret.truncationThreshold = parser.value(QStringLiteral("T")).toFloat();

    if(parser.isSet(QStringLiteral("s")))
        ret.subSampling = parser.value(QStringLiteral("s")).toDouble();

    ret.global = parser.isSet(QStringLiteral("g"));

    return ret;
}

CTL::NLOPT::GrangeatRegistration2D2D getOptimizer(const OptimizerSettings& settings)
{
    CTL::NLOPT::GrangeatRegistration2D2D ret;

    // truncation

    // metric
    ret.setMetric(&settings.metric);

    // sub-sampling
    ret.setAngluarSamplingLevel(settings.subSampling);

    // truncation
    ret.setMaxLineDistance(settings.maxLineDistance);
    if(!qFuzzyCompare(settings.truncationThreshold, std::numeric_limits<float>::min()))
        ret.setTruncationSieve(settings.truncationThreshold);

    // initial algorithm
    auto& opt = ret.optObject();

    if(settings.global)
    {
        opt = nlopt::opt{ nlopt::algorithm::GN_CRS2_LM, 6u };
        opt.set_maxtime(1000.0);
        opt.set_population(500u);
    }
    else
    {
        opt.set_xtol_rel(-1.0);
        opt.set_initial_step({ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 });
    }
    opt.set_xtol_abs(0.001);

    // set bounds
    opt.set_lower_bounds(settings.lowerBounds);
    opt.set_upper_bounds(settings.upperBounds);

    return ret;
}

QString getFilenameSuffix(const OptimizerSettings& settings)
{
    return (settings.global
                ? QStringLiteral("_global")
                : QStringLiteral("_local")) +
            "_sub" + QString::number(settings.subSampling) +
            (qFuzzyIsNull(settings.truncationThreshold)
                ? QStringLiteral("")
                : QStringLiteral("_trunc") + QString::number(settings.truncationThreshold)) +
            "_gmc" + QString::number(settings.metric.parameter()) +
            (!qFuzzyIsNull(settings.maxLineDistance - 1.0f)
                ? QStringLiteral("_maxDist") + QString::number(settings.maxLineDistance)
                : QStringLiteral("")) +
            ".den";
}

Output determineOutput(const QCommandLineParser& parser)
{
    // output
    bool R{ false }, H{ false }, P{ false };

    if(!parser.isSet(QStringLiteral("R")) && !parser.isSet(QStringLiteral("H"))
       && !parser.isSet(QStringLiteral("P")) && !parser.isSet(QStringLiteral("A")))
    {
        R = true;
    } else
    {
        if(parser.isSet(QStringLiteral("R")))
            R = true;
        if(parser.isSet(QStringLiteral("H")))
            H = true;
        if(parser.isSet(QStringLiteral("P")))
            P = true;
        if(parser.isSet(QStringLiteral("A")))
            R = H = P = true;
    }

    return { R, H, P };
}

uint getNbViews(AbstractProjectionDataIO& io, const QString& fn)
{
    const auto dims = io.metaInfo(fn).value(meta_info::dimensions)
                                     .value<meta_info::Dimensions>();
    return dims.nbDim < 4 ? dims.dim3 : dims.dim4;
}

uint checkDimensionality(CTL::io::AbstractProjectionDataIO& io, const QString& fn, const CTL::FullGeometry& pMats)
{
    if(pMats.nbViews() < 1)
    {
        qCritical("No projection matrices.");
        return 0;
    }

    // check dimensions of projections
    const auto nbImgs = getNbViews(io, fn);
    if(pMats.nbViews() != nbImgs)
    {
        qCritical("Number of projection matrices and number of projections do not match.");
        return 0;
    }

    return nbImgs;
}

CTL::Chunk2D<float> crop(const CTL::Chunk2D<float>& img, uint cropPixels)
{
    if(2 * cropPixels >= img.width() || 2 * cropPixels >= img.height())
        throw std::runtime_error{"The number of cropped pixels is larger than the projection image size."};

    CTL::Chunk2D<float> ret{img.width() - 2 * cropPixels, img.height() - 2 * cropPixels};
    ret.allocateMemory();

    for(auto x = 0u, X = ret.width(); x < X; ++x)
        for(auto y = 0u, Y = ret.height(); y < Y; ++y)
            ret(x, y) = img(x + cropPixels, y + cropPixels);

    return ret;
}

CTL::ProjectionMatrix crop(const CTL::ProjectionMatrix& mat, uint cropPixels)
{
    auto ret = mat;
    ret.shiftDetectorOrigin(cropPixels, cropPixels);
    return ret;
}

InputData readInputData(const QString& fnImgs, const QString& fnMats, uint cropPixels)
{
    // projection matrices
    const auto mats = projMatIO(fnMats)->readFullGeometry(fnMats);

    // Projection data IO
    const auto ioImgs = projDataIO(fnImgs);

    // Perform some checks
    const auto nbViews = checkDimensionality(*ioImgs, fnImgs, mats);
    if(nbViews == 0)
    {
        qCritical() << "Files are not valid.";
        qCritical() << "images file path:" << fnImgs;
        qCritical() << "matrices file path:" << fnMats;
        return { };
    }

    std::vector<CTL::Chunk2D<float>> imgVec;
    std::vector<CTL::mat::ProjectionMatrix> matVec;
    imgVec.reserve(nbViews);
    matVec.reserve(nbViews);

    for(auto v = 0u; v < nbViews; ++v)
    {
        imgVec.push_back(crop(ioImgs->readSingleView(fnImgs, v, 1).module(0), cropPixels));
        matVec.push_back(crop(mats.view(v).module(0), cropPixels));
    }

    return { std::move(imgVec), std::move(matVec) };
}

} // unnamed namespace
