// simple_projection_filter.cl

// OpenCL kernel - with one additional argument
kernel void filter( read_only image3d_t inputProj,  /* the input projection */
                    global float* filteredProj,     /* the output buffer */
                    uint view,                      /* the index of the view currently being processed */
                    float param)                    /* our additional parameter (constant we want to add) */
{
    // get IDs
    const int u = get_global_id(0);
    const int v = get_global_id(1);

    // single module case (using auto combine in host code)...
    const int4 pixIdx = (int4)(u, v, 0, 0); // ...that's why we can always use '0' as third index

    // read out the value of the pixel in 'inputProj'
    const float pixVal = read_imagef(inputProj, pixIdx).x; // can choose any component, since input is CL_INTENSITY

    // add 'param' to the input pixel and store result in 'filteredProj'
    // NOTE: we use the convenience method 'write_bufferf' here, see documentation of OpenCLFunctions::write_bufferf()
    write_bufferf(filteredProj, pixIdx, pixVal + param, inputProj); // passing 'inputProj' here provides the dimensions of the volume 
}
